package ua.com.integrity.bpm.camunda.study.service.impl;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import ua.com.integrity.bpm.camunda.study.dto.EmployeeDto;
import ua.com.integrity.bpm.camunda.study.dto.OrgUnitDto;
import ua.com.integrity.bpm.camunda.study.dto.PositionDto;
import ua.com.integrity.bpm.camunda.study.dto.vacation.VacationDto;

import java.util.List;

@FeignClient(name = "orgStructureFeign", url = "${org-structure.url}")
public interface OrgStructureRestApi {

    @GetMapping("employee/login/{login}")
    EmployeeDto getEmployeeByLogin(@PathVariable("login")String initiatorLogin);

    @GetMapping("/position/{id}")
    PositionDto getPosition(@PathVariable("id") Long position);

    @GetMapping("/position")
    List<PositionDto> getAllPositions();

    @GetMapping("/employee/{id}")
    EmployeeDto getEmployeeById(@PathVariable("id") Long employee);

    @GetMapping("/unit/{id}")
    OrgUnitDto getUnit(@PathVariable("id") Long orgUnit);

    @PostMapping("/vacation")
    VacationDto createVacation(@RequestBody VacationDto vacation);

    @DeleteMapping("/vacation/{id}")
    void deleteVacation(@PathVariable("id") Long vacationId);
}
