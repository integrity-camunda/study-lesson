package ua.com.integrity.bpm.camunda.study;

public class ProcessConstants {

    public static final String PROCESS_DEFINITION_KEY = "raif-example-vacation"; // BPMN Process ID

    public static final String vacationType = "vacationType";
    public static final String startDate = "startDate";
    public static final String endDate = "endDate";
    public static final String initiator = "initiator";
    public static final String fullName = "fullName";
    public static final String employeePosition = "employeePosition";
    public static final String managersList = "managersList";

    public static final String reminderDateTime = "reminderDateTime";
    public static final String vacationId = "vacationId";
    public static final String supervisorApproved = "supervisorApproved";
    public static final String vacationApproved = "vacationApproved";
}
