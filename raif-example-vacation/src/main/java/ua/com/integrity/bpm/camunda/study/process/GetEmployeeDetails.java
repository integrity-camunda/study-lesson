package ua.com.integrity.bpm.camunda.study.process;

import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;
import ua.com.integrity.bpm.camunda.study.ProcessConstants;
import ua.com.integrity.bpm.camunda.study.dto.EmployeeDto;
import ua.com.integrity.bpm.camunda.study.dto.PositionDto;
import ua.com.integrity.bpm.camunda.study.service.OrgStructureService;

@Component
@RequiredArgsConstructor
public class GetEmployeeDetails implements JavaDelegate {

    private final OrgStructureService orgStructureService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        String initiatorLogin = (String) execution.getVariable(ProcessConstants.initiator);

        EmployeeDto employee = orgStructureService.getEmployeeByLogin(initiatorLogin);
        String fullName = String.format("%s %s", employee.getLastName(), employee.getFirstName());
        execution.setVariable(ProcessConstants.fullName, fullName);

        PositionDto employeePosition = orgStructureService.getPosition(employee.getPosition());
        execution.setVariable(ProcessConstants.employeePosition, employeePosition);
    }
}
