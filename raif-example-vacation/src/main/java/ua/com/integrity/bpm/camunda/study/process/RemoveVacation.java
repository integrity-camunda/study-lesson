package ua.com.integrity.bpm.camunda.study.process;

import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;
import ua.com.integrity.bpm.camunda.study.ProcessConstants;
import ua.com.integrity.bpm.camunda.study.service.OrgStructureService;

@Component
@RequiredArgsConstructor
public class RemoveVacation implements JavaDelegate {

    private final OrgStructureService orgStructureService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        Long vacationId = (Long) execution.getVariable(ProcessConstants.vacationId);
        orgStructureService.deleteVacation(vacationId);
    }

}
