package ua.com.integrity.bpm.camunda.study.service.listener;

import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.RuntimeService;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

//@Component
@RequiredArgsConstructor
public class ExampleEventListener {

    private final RuntimeService runtimeService;

    @JmsListener(destination = "RINFO.MQ.REQEUST")
    public void consumeEvent(@Payload EmployeeApprovalEvent event) {
        runtimeService.createMessageCorrelation("message_empoyee_moved")
                .processInstanceVariableEquals("initiator", event.getLogin())
                .setVariable("employeeMessage", event.getText())
                .correlate();
    }
}
