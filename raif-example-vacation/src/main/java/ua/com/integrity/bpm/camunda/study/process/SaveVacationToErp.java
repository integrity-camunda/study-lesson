package ua.com.integrity.bpm.camunda.study.process;

import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;
import ua.com.integrity.bpm.camunda.study.ProcessConstants;
import ua.com.integrity.bpm.camunda.study.dto.EmployeeDto;
import ua.com.integrity.bpm.camunda.study.dto.PositionDto;
import ua.com.integrity.bpm.camunda.study.dto.vacation.VacationDto;
import ua.com.integrity.bpm.camunda.study.dto.vacation.VacationType;
import ua.com.integrity.bpm.camunda.study.service.OrgStructureService;

import java.time.*;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalUnit;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class SaveVacationToErp implements JavaDelegate {

    private final OrgStructureService orgStructureService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        String vacationType = (String) execution.getVariable(ProcessConstants.vacationType);
        Date startDate = (Date) execution.getVariable(ProcessConstants.startDate);
        Date endDate = (Date) execution.getVariable(ProcessConstants.endDate);
        String initiatorLogin = (String) execution.getVariable(ProcessConstants.initiator);

        EmployeeDto employee = orgStructureService.getEmployeeByLogin(initiatorLogin);

        VacationDto vacation = buildVacation(vacationType, startDate, endDate, employee.getId());

        VacationDto savedVacation = orgStructureService.createVacation(vacation);
        execution.setVariable(ProcessConstants.vacationId, savedVacation.getId());
    }

    private static VacationDto buildVacation(String vacationType, Date startDate, Date endDate, Long employeeId) {
        return VacationDto.builder()
                .employee(employeeId)
                .type(VacationType.valueOf(vacationType))
                .startDate(convert(startDate))
                .duration(computeDuration(startDate, endDate))
                .build();
    }

    private static Integer computeDuration(Date startDate, Date endDate) {
        return Period.between(convert(startDate), convert(endDate))
                .getDays();
    }

    private static LocalDate convert(Date date) {
        return date.toInstant().atZone(ZoneId.of("Europe/Kiev")).toLocalDate();
    }
}
