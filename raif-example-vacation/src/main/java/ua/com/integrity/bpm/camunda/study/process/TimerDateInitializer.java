package ua.com.integrity.bpm.camunda.study.process;

import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.springframework.stereotype.Component;
import ua.com.integrity.bpm.camunda.study.ProcessConstants;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class TimerDateInitializer implements ExecutionListener {

    @Override
    public void notify(DelegateExecution execution) throws Exception {
        Date startDate = (Date) execution.getVariable(ProcessConstants.startDate);
        LocalDateTime reminderDateTime = convert(startDate).minusDays(1).atTime(10, 0);
        execution.setVariable(ProcessConstants.reminderDateTime, convert(reminderDateTime));
    }


    private static LocalDate convert(Date date) {
        return date.toInstant().atZone(ZoneId.of("Europe/Kiev")).toLocalDate();
    }

    private static Date convert(LocalDateTime dateTime) {
        return Date.from(dateTime.atZone(ZoneId.of("Europe/Kiev")).toInstant());
    }
}
