package ua.com.integrity.bpm.camunda.study.process;

import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;
import ua.com.integrity.bpm.camunda.study.ProcessConstants;
import ua.com.integrity.bpm.camunda.study.dto.EmployeeDto;
import ua.com.integrity.bpm.camunda.study.dto.OrgUnitDto;
import ua.com.integrity.bpm.camunda.study.dto.PositionDto;
import ua.com.integrity.bpm.camunda.study.service.OrgStructureService;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class FindEmployeeManagers implements JavaDelegate {

    private final OrgStructureService orgStructureService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        PositionDto employeePosition = (PositionDto) execution.getVariable(ProcessConstants.employeePosition);
        Long employeeId = employeePosition.getEmployee();
        List<PositionDto> allPositions = orgStructureService.getAllPositions();
        OrgUnitDto unit = orgStructureService.getUnit(employeePosition.getOrgUnit());

        List<String> managerLoginsList = new LinkedList<>();
        fillManagerLogins(managerLoginsList, allPositions, unit, employeeId);

        execution.setVariable(ProcessConstants.managersList, managerLoginsList);
    }

    private void fillManagerLogins(List<String> managerLoginsList, List<PositionDto> allPositions, OrgUnitDto unit, Long employeeId) {
        Optional<PositionDto> managerPosition = allPositions.stream()
                .filter(position -> unit.getId().equals(position.getOrgUnit()) && position.getEmployee() != null)
                .min(Comparator.comparingInt(PositionDto::getIndex));
        if (managerPosition.isPresent()) {
            if (!employeeId.equals(managerPosition.get().getEmployee())) {
                EmployeeDto manager = orgStructureService.getEmployeeById(managerPosition.get().getEmployee());
                managerLoginsList.add(manager.getLogin());
            }
            Long ascendantUnitId = unit.getAscendant();
            if (ascendantUnitId != null) {
                OrgUnitDto acsendantUnit = orgStructureService.getUnit(ascendantUnitId);
                fillManagerLogins(managerLoginsList, allPositions, acsendantUnit, employeeId);
            }
        }
    }
}
