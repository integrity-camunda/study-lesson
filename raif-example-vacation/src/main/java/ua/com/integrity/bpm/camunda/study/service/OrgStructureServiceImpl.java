package ua.com.integrity.bpm.camunda.study.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ua.com.integrity.bpm.camunda.study.dto.EmployeeDto;
import ua.com.integrity.bpm.camunda.study.dto.OrgUnitDto;
import ua.com.integrity.bpm.camunda.study.dto.PositionDto;
import ua.com.integrity.bpm.camunda.study.dto.vacation.VacationDto;
import ua.com.integrity.bpm.camunda.study.service.impl.OrgStructureRestApi;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrgStructureServiceImpl implements OrgStructureService {

    private final OrgStructureRestApi orgStructureRestApi;

    @Override
    public EmployeeDto getEmployeeByLogin(String initiatorLogin) {
        return orgStructureRestApi.getEmployeeByLogin(initiatorLogin);
    }

    @Override
    public PositionDto getPosition(Long position) {
        return orgStructureRestApi.getPosition(position);
    }

    @Override
    public List<PositionDto> getAllPositions() {
        return orgStructureRestApi.getAllPositions();
    }

    @Override
    public EmployeeDto getEmployeeById(Long employee) {
        return orgStructureRestApi.getEmployeeById(employee);
    }

    @Override
    public OrgUnitDto getUnit(Long orgUnit) {
        return orgStructureRestApi.getUnit(orgUnit);
    }

    @Override
    public VacationDto createVacation(VacationDto vacation) {
        return orgStructureRestApi.createVacation(vacation);
    }

    @Override
    public void deleteVacation(Long vacationId) {
        orgStructureRestApi.deleteVacation(vacationId);
    }
}
