package ua.com.integrity.bpm.camunda.study.service;

import ua.com.integrity.bpm.camunda.study.dto.EmployeeDto;
import ua.com.integrity.bpm.camunda.study.dto.OrgUnitDto;
import ua.com.integrity.bpm.camunda.study.dto.PositionDto;
import ua.com.integrity.bpm.camunda.study.dto.vacation.VacationDto;

import java.util.List;

public interface OrgStructureService {
    EmployeeDto getEmployeeByLogin(String initiatorLogin);

    PositionDto getPosition(Long position);

    List<PositionDto> getAllPositions();

    EmployeeDto getEmployeeById(Long employee);

    OrgUnitDto getUnit(Long orgUnit);

    VacationDto createVacation(VacationDto vacation);

    void deleteVacation(Long vacationId);
}
