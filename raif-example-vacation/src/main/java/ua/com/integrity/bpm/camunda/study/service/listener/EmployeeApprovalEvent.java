package ua.com.integrity.bpm.camunda.study.service.listener;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor @AllArgsConstructor
public class EmployeeApprovalEvent {
    private String login;
    private String text;
}
