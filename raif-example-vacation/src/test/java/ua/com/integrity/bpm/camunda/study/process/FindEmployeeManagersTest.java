package ua.com.integrity.bpm.camunda.study.process;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import ua.com.integrity.bpm.camunda.study.ProcessConstants;
import ua.com.integrity.bpm.camunda.study.dto.EmployeeDto;
import ua.com.integrity.bpm.camunda.study.dto.OrgUnitDto;
import ua.com.integrity.bpm.camunda.study.dto.PositionDto;
import ua.com.integrity.bpm.camunda.study.service.OrgStructureService;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@MockitoSettings(strictness = Strictness.LENIENT)
class FindEmployeeManagersTest {

    @Mock
    DelegateExecution execution;
    @Mock
    OrgStructureService orgStructureService;

    @InjectMocks
    FindEmployeeManagers findEmployeeManagers;

    @Captor
    ArgumentCaptor<List<String>> managersCaptor;

    @Test
    void execute_main_boss() throws Exception {
        when(execution.getVariable(ProcessConstants.employeePosition)).thenReturn(buildPosition(1L,11L, 111L,1));

        findEmployeeManagers.execute(execution);

        verify(execution).setVariable(eq(ProcessConstants.managersList), managersCaptor.capture());

        assertThat(managersCaptor.getValue())
                .isEmpty();
    }

    @Test
    void execute_421_user() throws Exception {
        when(execution.getVariable(ProcessConstants.employeePosition)).thenReturn(buildPosition(4L,42L, 421L,4));

        findEmployeeManagers.execute(execution);

        verify(execution).setVariable(eq(ProcessConstants.managersList), managersCaptor.capture());

        assertThat(managersCaptor.getValue())
                .containsExactly("user411","user221","user111");
    }

    @Test
    void execute_311_user() throws Exception {
        when(execution.getVariable(ProcessConstants.employeePosition)).thenReturn(buildPosition(3L,31L, 311L,4));

        findEmployeeManagers.execute(execution);

        verify(execution).setVariable(eq(ProcessConstants.managersList), managersCaptor.capture());

        assertThat(managersCaptor.getValue())
                .containsOnly("user111");
    }

    @BeforeEach
    void setUp() {
        when(orgStructureService.getAllPositions()).thenReturn(buildMockPositions());
        when(orgStructureService.getUnit(1L)).thenReturn(OrgUnitDto.builder().id(1L).descendants(Arrays.asList(2L,3L)).build());
        when(orgStructureService.getUnit(2L)).thenReturn(OrgUnitDto.builder().id(2L).descendants(Arrays.asList(4L)).ascendant(1L).build());
        when(orgStructureService.getUnit(3L)).thenReturn(OrgUnitDto.builder().id(3L).ascendant(1L).build());
        when(orgStructureService.getUnit(4L)).thenReturn(OrgUnitDto.builder().id(4L).ascendant(2L).build());

        when(orgStructureService.getEmployeeById(anyLong())).then(invocation -> {
            long requestedEmplyeeId = invocation.getArgument(0);
            switch ((int) requestedEmplyeeId) {
                case 111:
                    return EmployeeDto.builder().id(111L).login("user111").build();
                case 121:
                    return EmployeeDto.builder().id(121L).login("user121").build();
                case 131:
                    return EmployeeDto.builder().id(131L).login("user131").build();
                case 221:
                    return EmployeeDto.builder().id(221L).login("user221").build();
                case 311:
                    return EmployeeDto.builder().id(311L).login("user311").build();
                case 331:
                    return EmployeeDto.builder().id(331L).login("user331").build();
                case 411:
                    return EmployeeDto.builder().id(411L).login("user411").build();
                case 421:
                    return EmployeeDto.builder().id(421L).login("user421").build();
                default:
                    throw new IllegalArgumentException("Unknown employee id for test");
            }
        });
    }

    private List<PositionDto> buildMockPositions() {
        return Arrays.asList(
                buildPosition(1L, 11L, 111L, 1),
                buildPosition(1L, 12L, 121L, 2),
                buildPosition(1L, 13L, 131L, 3),
                buildPosition(1L, 14L, null, 4),
                buildPosition(2L, 21L, null, 1),
                buildPosition(2L, 22L, 221L, 2),
                buildPosition(3L, 31L, 311L, 1),
                buildPosition(3L, 32L, null, 2),
                buildPosition(3L, 33L, 331L, 3),
                buildPosition(4L, 41L, 411L, 1),
                buildPosition(4L, 42L, 421L, 3)
        );
    }



    private PositionDto buildPosition(Long unitId, Long positionId, Long employeeId, int index) {
        return PositionDto.builder()
                .orgUnit(unitId)
                .id(positionId)
                .employee(employeeId)
                .index(index)
                .build();
    }
}