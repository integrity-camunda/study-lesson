package ua.com.integrity.bpm.camunda.study;

import org.apache.ibatis.logging.LogFactory;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.DateUtil;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.test.mock.Mocks;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import ua.com.integrity.bpm.camunda.study.dto.EmployeeDto;
import ua.com.integrity.bpm.camunda.study.dto.OrgUnitDto;
import ua.com.integrity.bpm.camunda.study.dto.PositionDto;
import ua.com.integrity.bpm.camunda.study.dto.vacation.VacationDto;
import ua.com.integrity.bpm.camunda.study.process.*;
import ua.com.integrity.bpm.camunda.study.service.OrgStructureService;
import ua.com.integrity.bpm.camunda.study.service.listener.EmployeeApprovalEvent;
import ua.com.integrity.bpm.camunda.study.service.listener.ExampleEventListener;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.*;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.*;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

/**
 * Test case starting an in-memory database-backed Process Engine.
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class InMemoryH2Test {

    static {
        LogFactory.useSlf4jLogging(); // MyBatis
    }

    @ClassRule
    @Rule
    public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();

    @Mock
    OrgStructureService orgStructureService;


    @Before
    public void setup() {
        init(rule.getProcessEngine());

        initMockitoMocks();

        GetEmployeeDetails getEmployeeDetails = new GetEmployeeDetails(orgStructureService);
        Mocks.register("getEmployeeDetails", getEmployeeDetails);

        FindEmployeeManagers findEmployeeManagers = new FindEmployeeManagers(orgStructureService);
        Mocks.register("findEmployeeManagers", findEmployeeManagers);

        TimerDateInitializer timerDateInitializer = new TimerDateInitializer();
        Mocks.register("timerDateInitializer", timerDateInitializer);

        SaveVacationToErp saveVacationToErp = new SaveVacationToErp(orgStructureService);
        Mocks.register("saveVacationToErp", saveVacationToErp);

        RemoveVacation removeVacation = new RemoveVacation(orgStructureService);
        Mocks.register("removeVacation", removeVacation);

        exampleEventListener = new ExampleEventListener(runtimeService());

    }

    ExampleEventListener exampleEventListener;

    @Test
    @Deployment(resources = "process.bpmn")
    public void testParsingAndDeployment() {

    }

    @Test
    @Deployment(resources = "process.bpmn")
    public void testHappyPath() {

        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey(ProcessConstants.PROCESS_DEFINITION_KEY,
                withVariables(
                        ProcessConstants.initiator, "user421",
                        ProcessConstants.vacationType, "ANNUALLY",
                        ProcessConstants.startDate, DateUtil.parse("2022-02-02"),
                        ProcessConstants.endDate, DateUtil.parse("2022-02-05")
                )
        );

        assertThat(processInstance)
                .isStarted();

        execute(job());

        assertThat(processInstance)
                .hasPassedInOrder(
                        "get_employee_details",
                        "find_employee_manager"
                ).isWaitingAt("approve_vacation_by_manager")
                .hasVariables(ProcessConstants.fullName)
                .task().isAssignedTo("user411");

        String fullName = (String) runtimeService().createVariableInstanceQuery()
                .processInstanceIdIn(processInstance.getProcessInstanceId())
                .variableName(ProcessConstants.fullName)
                .singleResult().getValue();

        assertThat(fullName).isEqualTo("Razin Stepan");

        complete(task(), withVariables(ProcessConstants.supervisorApproved, true));

        assertThat(processInstance)
                .hasPassedInOrder(
                        "approve_vacation_by_manager",
                        "manager_approve_gateway",
                        "manager_approved"
                ).hasNotPassed("manager_not_approved")
                .isWaitingAt("approve_vacation_by_manager")
                .task().isAssignedTo("user221");

        complete(task(), withVariables(ProcessConstants.supervisorApproved, true));

        assertThat(processInstance)
                .hasPassedInOrder(
                        "approve_vacation_by_manager",
                        "manager_approve_gateway",
                        "manager_approved"
                ).hasNotPassed("manager_not_approved")
                .isWaitingAt("approve_vacation_by_manager")
                .task().isAssignedTo("user111");

        complete(task(), withVariables(ProcessConstants.supervisorApproved, true));

        assertThat(processInstance)
                .isWaitingAt("hr_approval")
                .task().isAssignedTo("vkard");

        complete(task(), withVariables(ProcessConstants.supervisorApproved, true));

        assertThat(processInstance)
                .hasPassed("save_to_erp")
                .isWaitingAt("time_one_day_before")
                .job().hasDueDate(DateUtil.parseDatetime("2022-02-01T10:00:00"));

        execute(job());

        assertThat(processInstance)
                .hasPassed("time_one_day_before")
                .isWaitingAt("acknpwledge_vacation")
                .task().isAssignedTo("user421");

        complete(task(), withVariables(ProcessConstants.vacationApproved, true));

        assertThat(processInstance)
                .isWaitingFor("message_empoyee_moved");

        exampleEventListener.consumeEvent(new EmployeeApprovalEvent("user421", "simple text"));

        assertThat(processInstance)
                .isEnded();

        String employeeMessage = (String) historyService().createHistoricVariableInstanceQuery()
                .processInstanceId(processInstance.getProcessInstanceId())
                .variableName("employeeMessage")
                .singleResult().getValue();

        assertThat(employeeMessage).isEqualTo("simple text");


    }


    @Test
    @Deployment(resources = "process.bpmn")
    public void testprocessExecutionForCancelingCase() {
        Map<String, Object> processVariables = Variables
                .putValue(ProcessConstants.initiator, "user111")
                .putValue(ProcessConstants.startDate, DateUtil.parse("2022-02-02"))
                .putValue(ProcessConstants.endDate, DateUtil.parse("2022-02-08"))
                .putValue(ProcessConstants.vacationType, "ANNUALLY");

        ProcessInstance processInstance = runtimeService().createProcessInstanceByKey(ProcessConstants.PROCESS_DEFINITION_KEY)
                .startBeforeActivity("save_to_erp")
                .setVariables(processVariables)
                .execute();

        assertThat(processInstance)
                .isStarted()
                .hasPassed("save_to_erp")
                .isWaitingAt("time_one_day_before");

        execute(job());

        assertThat(processInstance)
                .isWaitingAt("acknpwledge_vacation")
                .task().isAssignedTo("user111");

        complete(task(), withVariables(ProcessConstants.vacationApproved, false));

        assertThat(processInstance)
                .hasPassedInOrder(
                        "acknpwledge_vacation",
                        "gateway_check_vacation_acknowledgement",
                        "end_event_vacation_canceled",
                        "delete_vacation_from_erp"

                ).isEnded();

            verify(orgStructureService).deleteVacation(1000L);

    }

    private void initMockitoMocks() {
        when(orgStructureService.getAllPositions()).thenReturn(buildMockPositions());
        when(orgStructureService.getUnit(1L)).thenReturn(OrgUnitDto.builder().id(1L).descendants(Arrays.asList(2L, 3L)).build());
        when(orgStructureService.getUnit(2L)).thenReturn(OrgUnitDto.builder().id(2L).descendants(Arrays.asList(4L)).ascendant(1L).build());
        when(orgStructureService.getUnit(3L)).thenReturn(OrgUnitDto.builder().id(3L).ascendant(1L).build());
        when(orgStructureService.getUnit(4L)).thenReturn(OrgUnitDto.builder().id(4L).ascendant(2L).build());

        when(orgStructureService.getEmployeeById(anyLong())).then(invocation -> {
            long requestedEmplyeeId = invocation.getArgument(0);
            switch ((int) requestedEmplyeeId) {
                case 111:
                    return EmployeeDto.builder().id(111L).login("user111").position(11L).firstName("Ivan").lastName("Ivanov").build();
                case 121:
                    return EmployeeDto.builder().id(121L).login("user121").position(12L).firstName("Borys").lastName("Borysov").build();
                case 131:
                    return EmployeeDto.builder().id(131L).login("user131").position(13L).firstName("Gleb").lastName("Glebov").build();
                case 221:
                    return EmployeeDto.builder().id(221L).login("user221").position(21L).firstName("Sidor").lastName("Sidorov").build();
                case 311:
                    return EmployeeDto.builder().id(311L).login("user311").position(31L).firstName("Viktor").lastName("Ivanchenko").build();
                case 331:
                    return EmployeeDto.builder().id(331L).login("user331").position(33L).firstName("Vitaliy").lastName("Maliy").build();
                case 411:
                    return EmployeeDto.builder().id(411L).login("user411").position(41L).firstName("Oleg").lastName("Glavniy").build();
                case 421:
                    return EmployeeDto.builder().id(421L).login("user421").position(42L).firstName("Stepan").lastName("Razin").build();
                default:
                    throw new IllegalArgumentException("Unknown employee id for test");
            }
        });

        when(orgStructureService.getEmployeeByLogin(anyString())).then(invocation -> {
            String login = invocation.getArgument(0);
            switch (login) {
                case "user111":
                    return EmployeeDto.builder().id(111L).login("user111").position(11L).firstName("Ivan").lastName("Ivanov").build();
                case "user121":
                    return EmployeeDto.builder().id(121L).login("user121").position(12L).firstName("Borys").lastName("Borysov").build();
                case "user131":
                    return EmployeeDto.builder().id(131L).login("user131").position(13L).firstName("Gleb").lastName("Glebov").build();
                case "user221":
                    return EmployeeDto.builder().id(221L).login("user221").position(14L).firstName("Sidor").lastName("Sidorov").build();
                case "user311":
                    return EmployeeDto.builder().id(311L).login("user311").position(31L).firstName("Viktor").lastName("Ivanchenko").build();
                case "user331":
                    return EmployeeDto.builder().id(331L).login("user331").position(33L).firstName("Vitaliy").lastName("Maliy").build();
                case "user411":
                    return EmployeeDto.builder().id(411L).login("user411").position(41L).firstName("Oleg").lastName("Glavniy").build();
                case "user421":
                    return EmployeeDto.builder().id(421L).login("user421").position(42L).firstName("Stepan").lastName("Razin").build();
                default:
                    throw new IllegalArgumentException("Unknown employee id for test");
            }
        });

        when(orgStructureService.getPosition(anyLong())).then(invocation -> {
            long positionId = invocation.getArgument(0);
            return buildMockPositions().stream().filter(positionDto -> positionId == positionDto.getId())
                    .findAny().get();
        });

        when(orgStructureService.createVacation(any())).thenReturn(VacationDto.builder().id(1000L).build());
    }

    private List<PositionDto> buildMockPositions() {
        return Arrays.asList(
                buildPosition(1L, 11L, 111L, 1),
                buildPosition(1L, 12L, 121L, 2),
                buildPosition(1L, 13L, 131L, 3),
                buildPosition(1L, 14L, null, 4),
                buildPosition(2L, 21L, null, 1),
                buildPosition(2L, 22L, 221L, 2),
                buildPosition(3L, 31L, 311L, 1),
                buildPosition(3L, 32L, null, 2),
                buildPosition(3L, 33L, 331L, 3),
                buildPosition(4L, 41L, 411L, 1),
                buildPosition(4L, 42L, 421L, 3)
        );
    }

    private PositionDto buildPosition(Long unitId, Long positionId, Long employeeId, int index) {
        return PositionDto.builder()
                .orgUnit(unitId)
                .id(positionId)
                .employee(employeeId)
                .index(index)
                .build();
    }

}


