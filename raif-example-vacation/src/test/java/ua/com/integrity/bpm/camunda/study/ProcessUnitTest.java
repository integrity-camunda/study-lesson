package ua.com.integrity.bpm.camunda.study;

import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import org.apache.ibatis.logging.LogFactory;
import org.assertj.core.util.DateUtil;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import ua.com.integrity.bpm.camunda.study.dto.EmployeeDto;
import ua.com.integrity.bpm.camunda.study.dto.OrgUnitDto;
import ua.com.integrity.bpm.camunda.study.dto.PositionDto;
import ua.com.integrity.bpm.camunda.study.dto.vacation.VacationDto;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.assertThat;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.complete;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.execute;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.init;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.job;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.runtimeService;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.task;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.withVariables;


/**
 * Test case starting an in-memory database-backed Process Engine.
 */
@SpringBootTest(
        webEnvironment = WebEnvironment.RANDOM_PORT,
        properties = {
                "org-structure.url=http://localhost:52020/"
        }
)
@WireMockTest(httpPort = 52020)
public class ProcessUnitTest {

  @Autowired
  private ProcessEngine processEngine;

  static {
    LogFactory.useSlf4jLogging(); // MyBatis
  }


  @BeforeEach
  public void setup() {
    init(processEngine);
    setupWireMocks();
  }

  @Test
  @Deployment(resources = "process.bpmn") // only required for process test coverage
  public void testHappyPath() throws InterruptedException {
    ProcessInstance processInstance = runtimeService().startProcessInstanceByKey(ProcessConstants.PROCESS_DEFINITION_KEY,
            withVariables(
                    ProcessConstants.initiator, "user421",
                    ProcessConstants.vacationType, "ANNUALLY",
                    ProcessConstants.startDate, DateUtil.parse("2022-02-02"),
                    ProcessConstants.endDate, DateUtil.parse("2022-02-05")
            )
    );

    assertThat(processInstance)
            .isStarted()
            .hasPassedInOrder(
                    "get_employee_details",
                    "find_employee_manager"
            ).isWaitingAt("approve_vacation_by_manager")
            .hasVariables(ProcessConstants.fullName)
            .task().isAssignedTo("user411");

    String fullName = (String) runtimeService().createVariableInstanceQuery()
            .processInstanceIdIn(processInstance.getProcessInstanceId())
            .variableName(ProcessConstants.fullName)
            .singleResult().getValue();

    assertThat(fullName).isEqualTo("Razin Stepan");

    complete(task(), withVariables(ProcessConstants.supervisorApproved, true));

    assertThat(processInstance)
            .hasPassedInOrder(
                    "approve_vacation_by_manager",
                    "manager_approve_gateway",
                    "manager_approved"
            ).hasNotPassed("manager_not_approved")
            .isWaitingAt("approve_vacation_by_manager")
            .task().isAssignedTo("user221");

    complete(task(), withVariables(ProcessConstants.supervisorApproved, true));

    assertThat(processInstance)
            .hasPassedInOrder(
                    "approve_vacation_by_manager",
                    "manager_approve_gateway",
                    "manager_approved"
            ).hasNotPassed("manager_not_approved")
            .isWaitingAt("approve_vacation_by_manager")
            .task().isAssignedTo("user111");

    complete(task(), withVariables(ProcessConstants.supervisorApproved, true));

    assertThat(processInstance)
            .isWaitingAt("hr_approval")
            .task().isAssignedTo("vkard");

    complete(task(), withVariables(ProcessConstants.supervisorApproved, true));

    assertThat(processInstance)
            .hasPassed("save_to_erp")
            .isWaitingAt("time_one_day_before")
            .job().hasDueDate(DateUtil.parseDatetime("2022-02-01T10:00:00"));

    execute(job());

    assertThat(processInstance)
            .hasPassed("time_one_day_before")
            .isWaitingAt("acknpwledge_vacation")
            .task().isAssignedTo("user421");

    complete(task(), withVariables(ProcessConstants.vacationApproved, true));

    assertThat(processInstance)
            .isWaitingAt("end_event_vacation_approved");


  }

  private void setupWireMocks() {
    stubFor(get("/position").willReturn(jsonResponse(buildMockPositions(), 200)));
    stubFor(get("/unit/1").willReturn(jsonResponse(OrgUnitDto.builder().id(1L).descendants(Arrays.asList(2L, 3L)).build(), 200)));
    stubFor(get("/unit/2").willReturn(jsonResponse(OrgUnitDto.builder().id(2L).descendants(Arrays.asList(4L)).ascendant(1L).build(), 200)));
    stubFor(get("/unit/3").willReturn(jsonResponse(OrgUnitDto.builder().id(3L).ascendant(1L).build(), 200)));
    stubFor(get("/unit/4").willReturn(jsonResponse(OrgUnitDto.builder().id(4L).ascendant(2L).build(), 200)));

    stubFor(get("/position/11").willReturn(jsonResponse(buildPosition(1L, 11L, 111L, 1), 200)));
    stubFor(get("/position/12").willReturn(jsonResponse(buildPosition(1L, 12l, 121L, 2), 200)));
    stubFor(get("/position/13").willReturn(jsonResponse(buildPosition(1L, 13L, 131L, 3), 200)));
    stubFor(get("/position/14").willReturn(jsonResponse(buildPosition(1L, 14L, null, 4), 200)));
    stubFor(get("/position/21").willReturn(jsonResponse(buildPosition(2L, 21L, null, 1), 200)));
    stubFor(get("/position/22").willReturn(jsonResponse(buildPosition(2L, 22L, 221L, 2), 200)));
    stubFor(get("/position/31").willReturn(jsonResponse(buildPosition(3L, 31L, 311L, 1), 200)));
    stubFor(get("/position/32").willReturn(jsonResponse(buildPosition(3L, 32L, null, 2), 200)));
    stubFor(get("/position/33").willReturn(jsonResponse(buildPosition(3L, 33L, 331L, 3), 200)));
    stubFor(get("/position/41").willReturn(jsonResponse(buildPosition(4L, 41L, 411L, 1), 200)));
    stubFor(get("/position/42").willReturn(jsonResponse(buildPosition(4L, 42L, 421L, 2), 200)));


    stubFor(get("/employee/111").willReturn(jsonResponse(EmployeeDto.builder().id(111L).login("user111").position(11L).firstName("Ivan").lastName("Ivanov").build(), 200)));
    stubFor(get("/employee/121").willReturn(jsonResponse(EmployeeDto.builder().id(121L).login("user121").position(12L).firstName("Borys").lastName("Borysov").build(), 200)));
    stubFor(get("/employee/131").willReturn(jsonResponse(EmployeeDto.builder().id(131L).login("user131").position(13L).firstName("Gleb").lastName("Glebov").build(), 200)));
    stubFor(get("/employee/221").willReturn(jsonResponse(EmployeeDto.builder().id(221L).login("user221").position(21L).firstName("Sidor").lastName("Sidorov").build(), 200)));
    stubFor(get("/employee/311").willReturn(jsonResponse(EmployeeDto.builder().id(311L).login("user311").position(31L).firstName("Viktor").lastName("Ivanchenko").build(), 200)));
    stubFor(get("/employee/331").willReturn(jsonResponse(EmployeeDto.builder().id(331L).login("user331").position(33L).firstName("Vitaliy").lastName("Maliy").build(), 200)));
    stubFor(get("/employee/411").willReturn(jsonResponse(EmployeeDto.builder().id(411L).login("user411").position(41L).firstName("Oleg").lastName("Glavniy").build(), 200)));
    stubFor(get("/employee/421").willReturn(jsonResponse(EmployeeDto.builder().id(421L).login("user421").position(42L).firstName("Stepan").lastName("Razin").build(), 200)));

    stubFor(get("/employee/login/user111").willReturn(jsonResponse(EmployeeDto.builder().id(111L).login("user111").position(11L).firstName("Ivan").lastName("Ivanov").build(), 200)));
    stubFor(get("/employee/login/user121").willReturn(jsonResponse(EmployeeDto.builder().id(121L).login("user121").position(12L).firstName("Borys").lastName("Borysov").build(), 200)));
    stubFor(get("/employee/login/user131").willReturn(jsonResponse(EmployeeDto.builder().id(131L).login("user131").position(13L).firstName("Gleb").lastName("Glebov").build(), 200)));
    stubFor(get("/employee/login/user221").willReturn(jsonResponse(EmployeeDto.builder().id(221L).login("user221").position(21L).firstName("Sidor").lastName("Sidorov").build(), 200)));
    stubFor(get("/employee/login/user311").willReturn(jsonResponse(EmployeeDto.builder().id(311L).login("user311").position(31L).firstName("Viktor").lastName("Ivanchenko").build(), 200)));
    stubFor(get("/employee/login/user331").willReturn(jsonResponse(EmployeeDto.builder().id(331L).login("user331").position(33L).firstName("Vitaliy").lastName("Maliy").build(), 200)));
    stubFor(get("/employee/login/user411").willReturn(jsonResponse(EmployeeDto.builder().id(411L).login("user411").position(41L).firstName("Oleg").lastName("Glavniy").build(), 200)));
    stubFor(get("/employee/login/user421").willReturn(jsonResponse(EmployeeDto.builder().id(421L).login("user421").position(42L).firstName("Stepan").lastName("Razin").build(), 200)));

    stubFor(post("/vacation").willReturn(jsonResponse(VacationDto.builder().id(1000L).build(), 200)));
    stubFor(delete("/vacation/1000").willReturn(status(204)));
  }

  private List<PositionDto> buildMockPositions() {
    return Arrays.asList(
            buildPosition(1L, 11L, 111L, 1),
            buildPosition(1L, 12L, 121L, 2),
            buildPosition(1L, 13L, 131L, 3),
            buildPosition(1L, 14L, null, 4),
            buildPosition(2L, 21L, null, 1),
            buildPosition(2L, 22L, 221L, 2),
            buildPosition(3L, 31L, 311L, 1),
            buildPosition(3L, 32L, null, 2),
            buildPosition(3L, 33L, 331L, 3),
            buildPosition(4L, 41L, 411L, 1),
            buildPosition(4L, 42L, 421L, 3)
    );
  }

  private PositionDto buildPosition(Long unitId, Long positionId, Long employeeId, int index) {
    return PositionDto.builder()
            .orgUnit(unitId)
            .id(positionId)
            .employee(employeeId)
            .index(index)
            .build();
  }

}
