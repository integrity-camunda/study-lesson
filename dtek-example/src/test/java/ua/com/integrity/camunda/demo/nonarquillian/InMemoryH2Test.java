package ua.com.integrity.camunda.demo.nonarquillian;

import org.apache.ibatis.logging.LogFactory;
import org.assertj.core.api.Assertions;
import org.camunda.bpm.engine.delegate.BpmnError;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.test.assertions.ProcessEngineTests;
import org.camunda.bpm.engine.test.mock.Mocks;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import ua.com.integrity.camunda.demo.dao.IndicationDao;
import ua.com.integrity.camunda.demo.model.Indication;
import ua.com.integrity.camunda.demo.process.*;
import ua.com.integrity.camunda.demo.rest.endpoints.StartProcessEndPoints;
import ua.com.integrity.camunda.demo.rest.model.StartProcessByChatBotRequest;
import ua.com.integrity.camunda.demo.service.ChatBotService;
import ua.com.integrity.camunda.demo.service.MailService;

import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.Optional;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.*;
import static org.mockito.Mockito.*;

/**
 * Test case starting an in-memory database-backed Process Engine.
 */
public class InMemoryH2Test {

  private static final String TEST_ACCOUNT_NUMBER = "TEST_ACCOUNT_NUMBER";
  private static final String TEST_BOT_ID = "TEST_BOT_ID";
  private static final String TEST_CHAT_ID = "TEST_CHAT_ID";
  private static final String TEST_USER_ID = "TEST_USER_ID";
  private static final String TEST_CUSTOMER_EMAIL = "test@example.com";
  private static final String TEST_MESSAGE_CHANNEL = "TELEGRAMM";
  private static final String TEST_SERVICE_OFFICE = "25";
  private static final int TEST_ZONES_NUMBER = 3;
  private static final String ZONE_1_VALUE = "150";
  private static final String ZONE_2_VALUE = "100";
  private static final String ZONE_3_VALUE = "50";
  private static final String EXPECTED_TOTAL_ACCRUED = "277.50";


  private static final String ZONE_1_PREV_VALUE = "100";
  private static final String ZONE_2_PREV_VALUE = "50";
  private static final String ZONE_3_PREV_VALUE = "40";
  private static final String EXPECTED_TOTAL_ACCRUED_WITH_PREV_VALUE = "76.50";


  @ClassRule
  @Rule
  public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();

  private static final String PROCESS_DEFINITION_KEY = "dtek-demo-omnichannel";

  static {
    LogFactory.useSlf4jLogging(); // MyBatis
  }

  @Mock
  MailService mailService;

  @Mock
  ChatBotService chatBotService;

  @Mock
  IndicationDao indicationDao;

  StartProcessEndPoints startProcessEndPoints;

  @Before
  public void setup() {
    init(rule.getProcessEngine());
    MockitoAnnotations.openMocks(this);

    startProcessEndPoints = new StartProcessEndPoints();
    startProcessEndPoints.setProcessEngine(processEngine());

    FindPreviousIndications findPreviousIndications = new FindPreviousIndications();
    findPreviousIndications.setIndicationDao(indicationDao);
    Mocks.register("findPreviousIndications", findPreviousIndications);

    CountDifference countDifference = new CountDifference();
    Mocks.register("countDifference", countDifference);

    ProcessTariffsResult processTariffsResult = new ProcessTariffsResult();
    Mocks.register("processTariffsResult", processTariffsResult);

    CountSummToPay countSummToPay = new CountSummToPay();
    Mocks.register("countSummToPay", countSummToPay);

    SaveIndicationsToDb saveIndicationsToDb = new SaveIndicationsToDb();
    saveIndicationsToDb.setIndicationDao(indicationDao);
    Mocks.register("saveIndicationsToDb", saveIndicationsToDb);

    CreateAndSendBill createAndSendBill = new CreateAndSendBill();
    createAndSendBill.setMailService(mailService);
    Mocks.register("createAndSendBill", createAndSendBill);

    SendSuccessResultToChatBot sendSuccessResultToChatBot = new SendSuccessResultToChatBot();
    sendSuccessResultToChatBot.setChatBotService(chatBotService);
    Mocks.register("sendSuccessResultToChatBot", sendSuccessResultToChatBot);

    SendErrorNotification sendErrorNotification = new SendErrorNotification();
    sendErrorNotification.setChatBotService(chatBotService);
    Mocks.register("sendErrorNotification", sendErrorNotification);

  }

  /**
   * Just tests if the process definition is deployable.
   */
  @Test
  @Deployment(resources = "process.bpmn")
  public void testParsingAndDeployment() {
    // nothing is done here, as we just want to check for exceptions during deployment
  }

  @Test
  @Deployment(resources = {"process.bpmn", "diagram_tariffs.dmn"})
  public void testHappyPath() {
      when(indicationDao.findLatestIndication(anyString(), anyInt())).thenReturn(Optional.empty());

	  ProcessInstance processInstance = startProcessInstance();

	  assertThat(processInstance)
              .hasPassed("StartEventProcessStarted")
              .isWaitingAt("StartEventProcessStarted");

	  execute(job());

    assertThat(processInstance)
              .hasPassedInOrder(
                      "task_get_previous_indications",
                      "process_difference_between_indications",
                      "task_find_appropriate_tariff",
                      "count_summ",
                      "save_indications_to_db"
              )
              .isWaitingAt("save_indications_to_db");

    Indication indication = (Indication) runtimeService().createVariableInstanceQuery()
            .processInstanceIdIn(processInstance.getProcessInstanceId())
            .variableName(ProcessVariables.CURRENT_INDICATIONS)
            .singleResult().getValue();

    Assertions.assertThat(indication.getAccruedInTotal().toPlainString()).isEqualTo(EXPECTED_TOTAL_ACCRUED);

    execute(job());

    assertThat(processInstance)
            .hasPassed("create_and_send_bill_to_email")
            .isWaitingAt("create_and_send_bill_to_email");

    execute(job());

    assertThat(processInstance)
            .hasPassed("send_success_result_to_chat_bot")
            .isEnded();
  }

  @Test
  @Deployment(resources = {"process.bpmn", "diagram_tariffs.dmn"})
  public void testHappyPathWithTwoZones() {
    when(indicationDao.findLatestIndication(anyString(), anyInt())).thenReturn(Optional.empty());

    ProcessInstance processInstance = startProcessInstanceWithZonesNumber(2);
    assertThat(processInstance)
            .hasPassed("StartEventProcessStarted")
            .isWaitingAt("StartEventProcessStarted");
    execute(job());

    assertThat(processInstance)
            .hasPassedInOrder(
                    "task_get_previous_indications",
                    "process_difference_between_indications",
                    "task_find_appropriate_tariff",
                    "count_summ",
                    "save_indications_to_db"
            )
            .isWaitingAt("save_indications_to_db");

    Indication indication = (Indication) runtimeService().createVariableInstanceQuery()
            .processInstanceIdIn(processInstance.getProcessInstanceId())
            .variableName(ProcessVariables.CURRENT_INDICATIONS)
            .singleResult().getValue();

    Assertions.assertThat(indication.getAccruedInTotal().toPlainString()).isEqualTo("219.00");

    execute(job());

    assertThat(processInstance)
            .hasPassed("create_and_send_bill_to_email")
            .isWaitingAt("create_and_send_bill_to_email");

    execute(job());

    assertThat(processInstance)
            .hasPassed("send_success_result_to_chat_bot")
            .isEnded();
  }

  @Test
  @Deployment(resources = {"process.bpmn", "diagram_tariffs.dmn"})
  public void testHappyPathWithOneZone() {
    when(indicationDao.findLatestIndication(anyString(), anyInt())).thenReturn(Optional.empty());

    ProcessInstance processInstance = startProcessInstanceWithZonesNumber(1);
    assertThat(processInstance)
            .hasPassed("StartEventProcessStarted")
            .isWaitingAt("StartEventProcessStarted");
    execute(job());

    assertThat(processInstance)
            .hasPassedInOrder(
                    "task_get_previous_indications",
                    "process_difference_between_indications",
                    "task_find_appropriate_tariff",
                    "count_summ",
                    "save_indications_to_db"
            )
            .isWaitingAt("save_indications_to_db");

    Indication indication = (Indication) runtimeService().createVariableInstanceQuery()
            .processInstanceIdIn(processInstance.getProcessInstanceId())
            .variableName(ProcessVariables.CURRENT_INDICATIONS)
            .singleResult().getValue();

    Assertions.assertThat(indication.getAccruedInTotal().toPlainString()).isEqualTo("174.00");

    execute(job());

    assertThat(processInstance)
            .hasPassed("create_and_send_bill_to_email")
            .isWaitingAt("create_and_send_bill_to_email");

    execute(job());

    assertThat(processInstance)
            .hasPassed("send_success_result_to_chat_bot")
            .isEnded();
  }

  @Test
  @Deployment(resources = {"process.bpmn", "diagram_tariffs.dmn"})
  public void testHappyPathWithPreviousIndications() {
    Indication prevIndication = new Indication();
    prevIndication.setOverallBalance(new BigDecimal("205.90"));
    prevIndication.setZone1Indication(new BigDecimal(ZONE_1_PREV_VALUE));
    prevIndication.setZone2Indication(new BigDecimal(ZONE_2_PREV_VALUE));
    prevIndication.setZone3Indication(new BigDecimal(ZONE_3_PREV_VALUE));
    when(indicationDao.findLatestIndication(anyString(), anyInt())).thenReturn(Optional.of(prevIndication));

    ProcessInstance processInstance = startProcessInstance();
    assertThat(processInstance)
            .hasPassed("StartEventProcessStarted")
            .isWaitingAt("StartEventProcessStarted");
    execute(job());

    assertThat(processInstance)
            .hasPassedInOrder(
                    "task_get_previous_indications",
                    "process_difference_between_indications",
                    "task_find_appropriate_tariff",
                    "count_summ",
                    "save_indications_to_db"
            )
            .isWaitingAt("save_indications_to_db");

    Indication indication = (Indication) runtimeService().createVariableInstanceQuery()
            .processInstanceIdIn(processInstance.getProcessInstanceId())
            .variableName(ProcessVariables.CURRENT_INDICATIONS)
            .singleResult().getValue();

    Assertions.assertThat(indication.getAccruedInTotal().toPlainString()).isEqualTo(EXPECTED_TOTAL_ACCRUED_WITH_PREV_VALUE);
    Assertions.assertThat(indication.getOverallBalance().toPlainString()).isEqualTo("282.40");

    execute(job());

    assertThat(processInstance)
            .hasPassed("create_and_send_bill_to_email")
            .isWaitingAt("create_and_send_bill_to_email");

    execute(job());

    assertThat(processInstance)
            .hasPassed("send_success_result_to_chat_bot")
            .isEnded();
  }

  @Test
  @Deployment(resources = {"process.bpmn", "diagram_tariffs.dmn"})
  public void testPathWithInvalidIndications() {
    Indication prevIndication = new Indication();
    prevIndication.setZone1Indication(new BigDecimal("200"));
    prevIndication.setZone2Indication(new BigDecimal(ZONE_2_PREV_VALUE));
    prevIndication.setZone3Indication(new BigDecimal(ZONE_3_PREV_VALUE));
    when(indicationDao.findLatestIndication(anyString(), anyInt())).thenReturn(Optional.of(prevIndication));

    ProcessInstance processInstance = startProcessInstance();
    assertThat(processInstance)
            .hasPassed("StartEventProcessStarted")
            .isWaitingAt("StartEventProcessStarted");
    execute(job());

    assertThat(processInstance)
            .hasPassedInOrder(
                    "task_get_previous_indications",
                    "process_difference_between_indications",
                    "error_event_count_difference",
                    "send_error_notification_to_chat_bot"
            )
            .isEnded();

  }

  @Test
  @Deployment(resources = {"process.bpmn", "diagram_tariffs.dmn"})
  public void testPathWithInvalidTooBigIndications() {
    Indication prevIndication = new Indication();
    prevIndication.setZone1Indication(new BigDecimal("200"));
    prevIndication.setZone2Indication(new BigDecimal(ZONE_2_PREV_VALUE));
    prevIndication.setZone3Indication(new BigDecimal(ZONE_3_PREV_VALUE));
    when(indicationDao.findLatestIndication(anyString(), anyInt())).thenReturn(Optional.of(prevIndication));

    ProcessInstance processInstance = startProcessInstance();
    assertThat(processInstance)
            .hasPassed("StartEventProcessStarted")
            .isWaitingAt("StartEventProcessStarted");
    execute(job());

    assertThat(processInstance)
            .hasPassedInOrder(
                    "task_get_previous_indications",
                    "process_difference_between_indications",
                    "error_event_count_difference",
                    "send_error_notification_to_chat_bot"
            )
            .isEnded();

  }

  @Test
  @Deployment(resources = {"process.bpmn", "diagram_tariffs.dmn"})
  public void testPathWithInvalidZonesNumber() {
    Indication prevIndication = new Indication();
    prevIndication.setZone1Indication(new BigDecimal(ZONE_1_PREV_VALUE));
    prevIndication.setZone2Indication(new BigDecimal(ZONE_2_PREV_VALUE));
    prevIndication.setZone3Indication(new BigDecimal(ZONE_3_PREV_VALUE));
    when(indicationDao.findLatestIndication(anyString(), anyInt())).thenReturn(Optional.of(prevIndication));

    ProcessInstance processInstance = startProcessInstanceWithZonesNumber(5);
    assertThat(processInstance)
            .hasPassed("StartEventProcessStarted")
            .isWaitingAt("StartEventProcessStarted");
    execute(job());

    assertThat(processInstance)
            .hasPassedInOrder(
                    "task_get_previous_indications",
                    "process_difference_between_indications",
                    "error_event_count_difference",
                    "send_error_notification_to_chat_bot"
            )
            .isEnded();

  }


  private ProcessInstance startProcessInstance() {
    return startProcessInstanceWithZonesNumber(TEST_ZONES_NUMBER);
  }



  private ProcessInstance startProcessInstanceTooBigValue() {
    StartProcessByChatBotRequest request = new StartProcessByChatBotRequest();
    request.setAccountNumber(TEST_ACCOUNT_NUMBER);
    request.setBotId(TEST_BOT_ID);
    request.setChatId(TEST_CHAT_ID);
    request.setUserId(TEST_USER_ID);
    request.setCustomerEmail(TEST_CUSTOMER_EMAIL);
    request.setMessageChannel(TEST_MESSAGE_CHANNEL);
    request.setServiceOffice(TEST_SERVICE_OFFICE);
    request.setCounterZonesNumber(TEST_ZONES_NUMBER);
    request.setZone1Indication("1000");
    request.setZone2Indication(ZONE_2_VALUE);
    request.setZone3Indication(ZONE_3_VALUE);
    Response response = startProcessEndPoints.startProcessInstanceByChatbot(request);
    String pi = (String) response.getEntity();
    return runtimeService().createProcessInstanceQuery().processInstanceId(pi).singleResult();
  }

  private ProcessInstance startProcessInstanceWithZonesNumber(int zonesNumber) {
    StartProcessByChatBotRequest request = new StartProcessByChatBotRequest();
    request.setAccountNumber(TEST_ACCOUNT_NUMBER);
    request.setBotId(TEST_BOT_ID);
    request.setChatId(TEST_CHAT_ID);
    request.setUserId(TEST_USER_ID);
    request.setCustomerEmail(TEST_CUSTOMER_EMAIL);
    request.setMessageChannel(TEST_MESSAGE_CHANNEL);
    request.setServiceOffice(TEST_SERVICE_OFFICE);
    request.setCounterZonesNumber(zonesNumber);
    request.setZone1Indication(ZONE_1_VALUE);
    if (zonesNumber > 1) {
      request.setZone2Indication(ZONE_2_VALUE);
      if (zonesNumber > 2) {
        request.setZone3Indication(ZONE_3_VALUE);
      }
    }
    Response response = startProcessEndPoints.startProcessInstanceByChatbot(request);
    String pi = (String) response.getEntity();
    return runtimeService().createProcessInstanceQuery().processInstanceId(pi).singleResult();
  }

}
