package ua.com.integrity.camunda.demo.process;

import org.hamcrest.Matchers;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class CountSummToPayTest {

    @Test
    public void calculateSummForGt100kW() {
        BigDecimal bigDecimal = CountSummToPay.calculateSummForGt100kW("452", "1.68");
        assertThat(bigDecimal.toPlainString(), Matchers.is("591.36"));
    }

    @Test
    public void calculateSummForGt100kWAnotherExample() {
        BigDecimal bigDecimal = CountSummToPay.calculateSummForGt100kW("552.326", "1.68");
        assertThat(bigDecimal.toPlainString(), Matchers.is("759.91"));
    }

    @Test
    public void testCalculateSummForGt100kW() {
        BigDecimal bigDecimal = CountSummToPay.calculateSummForLt100kW("72.164", "0.9");
        assertThat(bigDecimal.toPlainString(), Matchers.is("64.95"));
    }
}