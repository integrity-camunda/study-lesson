package ua.com.integrity.camunda.demo.process;

import org.hamcrest.Matchers;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class CountDifferenceTest {

    @Test
    public void calculateDifference() {
        BigDecimal prev = new BigDecimal("123.7");
        String current = "261.25";
        BigDecimal bigDecimal = CountDifference.calculateDifference(prev, current);
        assertThat(bigDecimal.toPlainString(), Matchers.is("137.550"));
    }
}