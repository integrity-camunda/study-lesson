package ua.com.integrity.camunda.demo.service;

import fr.opensagres.xdocreport.core.XDocReportException;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.Variables;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;

import static org.junit.Assert.*;
import static ua.com.integrity.camunda.demo.process.ProcessVariables.*;
import static ua.com.integrity.camunda.demo.process.ProcessVariables.NEW_CHARGES;

public class DocEngineTest {

    InputStream inputData;

    ByteArrayOutputStream outputData;

    FileOutputStream fileStream;

    @Before
    public void setUp() throws Exception {
        inputData = this.getClass().getClassLoader().getResourceAsStream("template_bill.docx");
        outputData = new ByteArrayOutputStream();
        fileStream = new FileOutputStream("test_bill.docx");
    }

    @After
    public void tearDown() throws IOException {
        inputData.close();
        outputData.writeTo(fileStream);
        outputData.close();
        fileStream.close();
    }

    private static final String ZONE_1_PREV_VALUE = "zone1PrevValue";
    private static final String ZONE_2_PREV_VALUE = "zone2PrevValue";
    private static final String ZONE_3_PREV_VALUE = "zone3PrevValue";
    private static final String BILL_NUMBER = "billNumber";
    private static final String BILL_DATE = "billDate";

//    @Test
    public void insertData() throws IOException, XDocReportException {
        VariableMap variableMap = Variables
                .putValue(CUSTOMER_ACCOUNT, "1111-0000-111")
                .putValue(ZONE_1_VALUE, "150")
                .putValue(ZONE_2_VALUE, "200")
                .putValue(ZONE_3_VALUE, "250")
                .putValue(ZONE_1_PREV_VALUE, "120")
                .putValue(ZONE_2_PREV_VALUE, "150")
                .putValue(ZONE_3_PREV_VALUE, "180")
                .putValue(ZONE_1_TARIFF_LT_100, "0.9")
                .putValue(ZONE_1_TARIFF_GT_100, "1.68")
                .putValue(ZONE_2_TARIFF_LT_100, "0.36")
                .putValue(ZONE_2_TARIFF_GT_100, "0.672")
                .putValue(ZONE_3_TARIFF_LT_100, "1.35")
                .putValue(ZONE_3_TARIFF_GT_100, "2.52")
                .putValue(PREVIOUS_BALANCE, "127.00")
                .putValue(CURRENT_BALANCE, "205.00")
                .putValue(NEW_CHARGES, "68.00")
                .putValue(BILL_NUMBER, "2577171")
                .putValue(BILL_DATE, "25.08.2020");
        DocEngine.insertData(inputData, variableMap, outputData);
    }
}