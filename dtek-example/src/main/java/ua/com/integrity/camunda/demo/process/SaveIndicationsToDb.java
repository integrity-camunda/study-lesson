package ua.com.integrity.camunda.demo.process;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import ua.com.integrity.camunda.demo.dao.IndicationDao;
import ua.com.integrity.camunda.demo.model.Indication;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Named;

@Named
@Stateless
public class SaveIndicationsToDb implements JavaDelegate {

    @EJB
    private IndicationDao indicationDao;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        Indication indication = (Indication) execution.getVariable(ProcessVariables.CURRENT_INDICATIONS);
        indicationDao.addNewIndication(indication);
    }

    public void setIndicationDao(IndicationDao indicationDao) {
        this.indicationDao = indicationDao;
    }
}
