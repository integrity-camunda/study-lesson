package ua.com.integrity.camunda.demo.process;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import ua.com.integrity.camunda.demo.model.Indication;

import javax.ejb.Stateless;
import javax.inject.Named;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.Map;

@Named
@Stateless
public class CountSummToPay implements JavaDelegate {

    private static final MathContext mc = MathContext.UNLIMITED;
    private static final BigDecimal decimal100kW = BigDecimal.valueOf(100L);
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        Map<String, Object> processVars = execution.getVariables();

        String zone1Diff = (String) processVars.get(ProcessVariables.ZONE_1_DIFF);
        String zone1TariffLt100 = (String) processVars.get(ProcessVariables.ZONE_1_TARIFF_LT_100);
        String zone1TariffGt100 = (String) processVars.get(ProcessVariables.ZONE_1_TARIFF_GT_100);
        BigDecimal zone1SummLt100 = calculateSummForLt100kW(zone1Diff, zone1TariffLt100);
        processVars.put(ProcessVariables.ZONE_1_SUMM_LT_100, zone1SummLt100);
        BigDecimal zone1SummGt100 = calculateSummForGt100kW(zone1Diff, zone1TariffGt100);
        processVars.put(ProcessVariables.ZONE_1_SUMM_GT_100, zone1SummGt100);

        String zone2Diff = (String) processVars.get(ProcessVariables.ZONE_2_DIFF);
        String zone2TariffLt100 = (String) processVars.get(ProcessVariables.ZONE_2_TARIFF_LT_100);
        String zone2TariffGt100 = (String) processVars.get(ProcessVariables.ZONE_2_TARIFF_GT_100);
        BigDecimal zone2SummLt100 = calculateSummForLt100kW(zone2Diff, zone2TariffLt100);
        processVars.put(ProcessVariables.ZONE_2_SUMM_LT_100, zone2SummLt100);
        BigDecimal zone2SummGt100 = calculateSummForGt100kW(zone2Diff, zone2TariffGt100);
        processVars.put(ProcessVariables.ZONE_2_SUMM_GT_100, zone2SummGt100);

        String zone3Diff = (String) processVars.get(ProcessVariables.ZONE_3_DIFF);
        String zone3TariffLt100 = (String) processVars.get(ProcessVariables.ZONE_3_TARIFF_LT_100);
        String zone3TariffGt100 = (String) processVars.get(ProcessVariables.ZONE_3_TARIFF_GT_100);
        BigDecimal zone3SummLt100 = calculateSummForLt100kW(zone3Diff, zone3TariffLt100);
        processVars.put(ProcessVariables.ZONE_3_SUMM_LT_100, zone3SummLt100);
        BigDecimal zone3SummGt100 = calculateSummForGt100kW(zone3Diff, zone3TariffGt100);
        processVars.put(ProcessVariables.ZONE_3_SUMM_GT_100, zone3SummGt100);

        String previousBalance = (String) processVars.get(ProcessVariables.PREVIOUS_BALANCE);

        BigDecimal accruedInTotal = scale(zone1SummLt100.add(zone1SummGt100).add(zone2SummLt100).add(zone2SummGt100)
                .add(zone3SummLt100).add(zone3SummGt100));
        processVars.put(ProcessVariables.NEW_CHARGES, accruedInTotal.toPlainString());

        BigDecimal overallBalance = accruedInTotal.add(new BigDecimal(previousBalance));
        processVars.put(ProcessVariables.CURRENT_BALANCE, overallBalance.toPlainString());

        Indication currentIndication = (Indication) processVars.get(ProcessVariables.CURRENT_INDICATIONS);
        currentIndication.setAccruedInTotal(accruedInTotal);
        currentIndication.setOverallBalance(overallBalance);

        execution.setVariables(processVars);
    }

    public static BigDecimal calculateSummForGt100kW(String kW, String tariff) {
        BigDecimal kWDecimal = new BigDecimal(kW);
        BigDecimal tariffDecimal = new BigDecimal(tariff);
        if (kWDecimal.compareTo(decimal100kW) > 0) {
            return scale(kWDecimal.subtract(decimal100kW, mc).multiply(tariffDecimal, mc));
        } else {
            return BigDecimal.ZERO;
        }
    }

    public static BigDecimal calculateSummForLt100kW(String kW, String tariff) {
        BigDecimal kWDecimal = new BigDecimal(kW);
        BigDecimal tariffDecimal = new BigDecimal(tariff);
        if (kWDecimal.compareTo(decimal100kW) <= 0) {
            return scale(kWDecimal.multiply(tariffDecimal, mc));
        } else {
            return scale(decimal100kW.multiply(tariffDecimal, mc));
        }
    }

    private static BigDecimal scale(BigDecimal bigDecimal) {
        return bigDecimal.setScale(2, RoundingMode.HALF_UP);
    }
}
