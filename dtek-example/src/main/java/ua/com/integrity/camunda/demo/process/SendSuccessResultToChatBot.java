package ua.com.integrity.camunda.demo.process;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import ua.com.integrity.camunda.demo.service.ChatBotService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;

@Named
@Stateless
public class SendSuccessResultToChatBot implements JavaDelegate {

    @Inject
    private ChatBotService chatBotService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        Map<String, Object> processVars = execution.getVariables();

        String accountNumber = (String) processVars.get(ProcessVariables.CUSTOMER_ACCOUNT);
        String newCharges = (String) processVars.get(ProcessVariables.NEW_CHARGES);
        String currentBalance = (String) processVars.get(ProcessVariables.CURRENT_BALANCE);
        String message = String.format("Шановний абонент. Ваші показники по о/р %1$s прийнято. Нараховано %2$s грн. Всього до сплати з урахуванням заборгованості: %3$s грн. Квитанцію відправлено вам на email.",
                accountNumber, newCharges, currentBalance);

        String chatId = (String) processVars.get(ProcessVariables.CHAT_ID);
        String chatBotMessageChannel = (String) processVars.get(ProcessVariables.BOT_MESSAGE_CHANNEL);
        String botId = (String) processVars.get(ProcessVariables.BOT_ID);
        String botUserId = (String) processVars.get(ProcessVariables.BOT_USER_ID);

        chatBotService.sendMessage(botId, chatId, chatBotMessageChannel, botUserId, message);
    }

    public void setChatBotService(ChatBotService chatBotService) {
        this.chatBotService = chatBotService;
    }
}
