package ua.com.integrity.camunda.demo.process;

public class ProcessVariables {

    public static final String CUSTOMER_ACCOUNT = "customerAccount";
    public static final String CUSTOMER_EMAIL = "customerEmail";
    public static final String ZONES_NUMBER = "zonesNumber";
    public static final String ZONE_1_VALUE = "zone1Value";
    public static final String ZONE_2_VALUE = "zone2Value";
    public static final String ZONE_3_VALUE = "zone3Value";
    public static final String SERVICE_OFFICE_ID = "serviceOfficeId";

    public static final String CHANNEL_TYPE = "channelType";

    public static final String ZONE_1_DIFF = "zone1Diff";
    public static final String ZONE_2_DIFF = "zone2Diff";
    public static final String ZONE_3_DIFF = "zone3Diff";

    public static final String ZONE_1_TARIFF_LT_100 = "zone1TariffLt100";
    public static final String ZONE_1_TARIFF_GT_100 = "zone1TariffGt100";
    public static final String ZONE_2_TARIFF_LT_100 = "zone2TariffLt100";
    public static final String ZONE_2_TARIFF_GT_100 = "zone2TariffGt100";
    public static final String ZONE_3_TARIFF_LT_100 = "zone3TariffLt100";
    public static final String ZONE_3_TARIFF_GT_100 = "zone3TariffGt100";

    public static final String ZONE_1_SUMM_LT_100 = "zone1SummLt100";
    public static final String ZONE_1_SUMM_GT_100 = "zone1SummGt100";
    public static final String ZONE_2_SUMM_LT_100 = "zone2SummLt100";
    public static final String ZONE_2_SUMM_GT_100 = "zone2SummGt100";
    public static final String ZONE_3_SUMM_LT_100 = "zone3SummLt100";
    public static final String ZONE_3_SUMM_GT_100 = "zone3SummGt100";

    public static final String PREVIOUS_BALANCE = "previousBalance";
    public static final String NEW_CHARGES = "newCharges";
    public static final String CURRENT_BALANCE = "currentBalance";

    public static final String PREVIOUS_INDICATIONS = "previousIndications";
    public static final String CURRENT_INDICATIONS = "currentIndications";


    public static final String CHAT_ID = "chatId";
    public static final String BOT_ID = "botId";
    public static final String BOT_USER_ID = "botUserId";
    public static final String BOT_MESSAGE_CHANNEL = "botMessageChannel";


    public static final String START_PROCESS_BY_CHATBOT_MESSAGE = "msg_start_process_by_chatbot";

    public static final String CHATBOT_CHANNEL = "CHATBOT";
    public static final String WEBSITE_CHANNEL = "WEBSITE";
    public static final String CALLCENTER_CHANNEL = "CALLCENTER";
}
