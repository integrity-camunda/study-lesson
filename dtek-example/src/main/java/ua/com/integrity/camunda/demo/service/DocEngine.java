package ua.com.integrity.camunda.demo.service;

import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;


public class DocEngine {

    public static void insertData(InputStream contentStream, Map<String, Object> mapingData, ByteArrayOutputStream outStream)
            throws IOException, XDocReportException {
        IXDocReport report = XDocReportRegistry.getRegistry().loadReport(contentStream, TemplateEngineKind.Freemarker,
                false);
        IContext context = report.createContext();
        context.putMap(mapingData);
        report.process(context, outStream);
    }

}
