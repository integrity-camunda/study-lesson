package ua.com.integrity.camunda.demo.process;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import ua.com.integrity.camunda.demo.service.ChatBotService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;

@Named
@Stateless
public class SendErrorNotification implements JavaDelegate {

    @Inject
    private ChatBotService chatBotService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        Map<String, Object> processVars = execution.getVariables();

        String accountNumber = (String) processVars.get(ProcessVariables.CUSTOMER_ACCOUNT);
        String message = String.format("Шановний абонент. По о/р %1$s введено некорректні значення. Перевірте правильність введених показників.",
                accountNumber);

        String chatId = (String) processVars.get(ProcessVariables.CHAT_ID);
        String chatBotMessageChannel = (String) processVars.get(ProcessVariables.BOT_MESSAGE_CHANNEL);
        String botId = (String) processVars.get(ProcessVariables.BOT_ID);
        String botUserId = (String) processVars.get(ProcessVariables.BOT_USER_ID);

        chatBotService.sendMessage(botId, chatId, chatBotMessageChannel, botUserId, message);
    }

    public void setChatBotService(ChatBotService chatBotService) {
        this.chatBotService = chatBotService;
    }
}
