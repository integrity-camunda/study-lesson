package ua.com.integrity.camunda.demo.dao;

import ua.com.integrity.camunda.demo.model.Indication;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.Optional;

@Stateless
public class IndicationDao {

    @PersistenceContext(unitName = "dtek-demo-omnichannel-indications")
    private EntityManager entityManager;

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Optional<Indication> findLatestIndication(String customerAccount, int counterZonesNumber) {
        try {
            Indication indication = entityManager.createNamedQuery("findLatestIndications", Indication.class)
                    .setParameter("accountNumber", customerAccount)
                    .setParameter("counterZonesNumber", counterZonesNumber)
                    .getSingleResult();
            return Optional.ofNullable(indication);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @TransactionAttribute(TransactionAttributeType.MANDATORY)
    public Indication addNewIndication(Indication indication) {
        entityManager.persist(indication);
        return indication;
    }
}
