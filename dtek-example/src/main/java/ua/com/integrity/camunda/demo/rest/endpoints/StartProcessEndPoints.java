package ua.com.integrity.camunda.demo.rest.endpoints;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.cdi.annotation.ProcessEngineName;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.Variables;
import ua.com.integrity.camunda.demo.process.ProcessVariables;
import ua.com.integrity.camunda.demo.rest.model.StartProcessByChatBotRequest;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;

@Path("start")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class StartProcessEndPoints {

    @Inject
    @ProcessEngineName("default")
    private ProcessEngine processEngine;

    @POST
    @Path("chatbot")
    public Response startProcessInstanceByChatbot(StartProcessByChatBotRequest request) {

        Optional.ofNullable(request.getZone1Indication()).orElse("0").replace(',', '.');
        VariableMap variableMap = Variables
                .putValue(ProcessVariables.CUSTOMER_ACCOUNT, request.getAccountNumber())
                .putValue(ProcessVariables.CUSTOMER_EMAIL, request.getCustomerEmail())
                .putValue(ProcessVariables.ZONES_NUMBER, request.getCounterZonesNumber())
                .putValue(ProcessVariables.ZONE_1_VALUE, parseIndications(request.getZone1Indication()))
                .putValue(ProcessVariables.ZONE_2_VALUE, parseIndications(request.getZone2Indication()))
                .putValue(ProcessVariables.ZONE_3_VALUE, parseIndications(request.getZone3Indication()))
                .putValue(ProcessVariables.SERVICE_OFFICE_ID, request.getServiceOffice())
                .putValue(ProcessVariables.CHAT_ID, request.getChatId())
                .putValue(ProcessVariables.BOT_ID, request.getBotId())
                .putValue(ProcessVariables.BOT_MESSAGE_CHANNEL, request.getMessageChannel())
                .putValue(ProcessVariables.BOT_USER_ID, request.getUserId());

        ProcessInstance processInstance;
        try {
            processInstance = processEngine.getRuntimeService()
                    .startProcessInstanceByMessage(ProcessVariables.START_PROCESS_BY_CHATBOT_MESSAGE, variableMap);
        } catch (RuntimeException e) {
            return Response.serverError().entity(e.toString()).build();
        }
        return Response.ok(processInstance.getProcessInstanceId()).build();
    }

    private static String parseIndications(String indication) {
        if (indication == null || indication.isEmpty()) {
            return "0";
        }
        return indication.replace(',', '.');
    }

    public void setProcessEngine(ProcessEngine processEngine) {
        this.processEngine = processEngine;
    }
}
