package ua.com.integrity.camunda.demo.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@NamedQueries({
        @NamedQuery(
                name = "findLatestIndications",
                query = "SELECT i FROM Indication i WHERE i.accountNumber = :accountNumber " +
                        "AND i.counterZonesNumber = :counterZonesNumber AND i.insertTime = (" +
                            "SELECT MAX(indication.insertTime) FROM Indication indication " +
                            "WHERE indication.accountNumber = :accountNumber " +
                            "AND indication.counterZonesNumber = :counterZonesNumber" +
                        ")"
        )
})
@Table(name = "counters_indications")
public class Indication implements Serializable {

    @Id
    @Column(name = "indications_id")
    @SequenceGenerator(name = "indicationsId",sequenceName = "counters_indications_sequence")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "indicationsId")
    private Long id;

    @Column(name = "customer_account", nullable = false)
    private String accountNumber;

    @Column(name = "number_of_zones", nullable = false)
    private int counterZonesNumber;

    @Column(name = "zone_1_indication", precision = 256, scale = 3, nullable = false)
    private BigDecimal zone1Indication;

    @Column(name = "zone_2_indication", precision = 256, scale = 3)
    private BigDecimal zone2Indication;

    @Column(name = "zone_3_indication", precision = 256, scale = 3)
    private BigDecimal zone3Indication;

    @Column(name = "accrued_in_total", precision = 256, scale = 2, nullable = false)
    private BigDecimal accruedInTotal;

    @Column(name = "overall_balance", precision = 256, scale = 2, nullable = false)
    private BigDecimal overallBalance;

    @Column(name = "source_channel")
    private String sourceChannel;

    @Column(name = "insert_time", nullable = false)
    private Date insertTime;

    public Indication() {
        this.zone1Indication = BigDecimal.ZERO;
        this.zone2Indication = BigDecimal.ZERO;
        this.zone3Indication = BigDecimal.ZERO;
        this.accruedInTotal = BigDecimal.ZERO;
        this.overallBalance = BigDecimal.ZERO;
    }

    public Indication(String accountNumber, int counterZonesNumber) {
        this();
        this.accountNumber = accountNumber;
        this.counterZonesNumber = counterZonesNumber;
    }

    public Indication(String accountNumber, int counterZonesNumber, BigDecimal zone1Indication) {
        this();
        this.accountNumber = accountNumber;
        this.counterZonesNumber = counterZonesNumber;
        this.zone1Indication = zone1Indication;
    }

    @PrePersist
    private void setupInsertDate(){
        this.insertTime = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public int getCounterZonesNumber() {
        return counterZonesNumber;
    }

    public void setCounterZonesNumber(int counterZonesNumber) {
        this.counterZonesNumber = counterZonesNumber;
    }

    public BigDecimal getZone1Indication() {
        return zone1Indication;
    }

    public void setZone1Indication(BigDecimal zone1Indication) {
        this.zone1Indication = zone1Indication;
    }

    public BigDecimal getZone2Indication() {
        return zone2Indication;
    }

    public void setZone2Indication(BigDecimal zone2Indication) {
        this.zone2Indication = zone2Indication;
    }

    public BigDecimal getZone3Indication() {
        return zone3Indication;
    }

    public void setZone3Indication(BigDecimal zone3Indication) {
        this.zone3Indication = zone3Indication;
    }

    public BigDecimal getAccruedInTotal() {
        return accruedInTotal;
    }

    public void setAccruedInTotal(BigDecimal accruedInTotal) {
        this.accruedInTotal = accruedInTotal;
    }

    public BigDecimal getOverallBalance() {
        return overallBalance;
    }

    public void setOverallBalance(BigDecimal overallBalance) {
        this.overallBalance = overallBalance;
    }

    public String getSourceChannel() {
        return sourceChannel;
    }

    public void setSourceChannel(String sourceChannel) {
        this.sourceChannel = sourceChannel;
    }

    public Date getInsertTime() {
        return insertTime;
    }

    public void setInsertTime(Date insertTime) {
        this.insertTime = insertTime;
    }
}
