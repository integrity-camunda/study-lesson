package ua.com.integrity.camunda.demo.service;

import ua.com.integrity.camunda.demo.rest.config.DefaultWebTarget;
import ua.com.integrity.camunda.demo.rest.model.ChatBotPushRequest;

import javax.inject.Inject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class ChatBotService {

    @Inject
    @DefaultWebTarget
    private WebTarget webTarget;

    public void sendMessage(String botId, String chatId, String channel, String botUserId, String message) {
        ChatBotPushRequest request = new ChatBotPushRequest(message);
        request.getReceivers().add(chatId);

        Response response = webTarget.queryParam("botId", botId)
                .queryParam("channelType", channel)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .header("Authorization", "Basic skjdfajkfblskadjfdksjfslkfjbklh")
                .buildPost(Entity.json(request))
                .invoke();
        String responseBody = response.readEntity(String.class);
        switch (response.getStatus()) {
            case 200:
                return;
            case 400:
                throw new IllegalArgumentException("Invalid request body found.Check the input data. "+ responseBody );
            case 401:
                throw new IllegalArgumentException("Request unauthorized, check client configuration " + responseBody);
            case 404:
                throw new IllegalArgumentException("Invalid URL found, check the client configuration " + responseBody);
            case 500:
                throw new RuntimeException("Internal error occured on chatbot: " + responseBody);
        }

    }
}
