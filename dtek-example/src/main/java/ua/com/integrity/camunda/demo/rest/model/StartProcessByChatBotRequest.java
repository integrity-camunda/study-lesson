package ua.com.integrity.camunda.demo.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class StartProcessByChatBotRequest {

    private String accountNumber;
    private String serviceOffice;

    private int counterZonesNumber;
    private String zone1Indication;
    private String zone2Indication;
    private String zone3Indication;


    private String customerEmail;

    private String chatId;
    private String botId;
    private String userId;
    private String messageChannel;

    public StartProcessByChatBotRequest() {
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getServiceOffice() {
        return serviceOffice;
    }

    public void setServiceOffice(String serviceOffice) {
        this.serviceOffice = serviceOffice;
    }

    public int getCounterZonesNumber() {
        return counterZonesNumber;
    }

    public void setCounterZonesNumber(int counterZonesNumber) {
        this.counterZonesNumber = counterZonesNumber;
    }

    public String getZone1Indication() {
        return zone1Indication;
    }

    public void setZone1Indication(String zone1Indication) {
        this.zone1Indication = zone1Indication;
    }

    public String getZone2Indication() {
        return zone2Indication;
    }

    public void setZone2Indication(String zone2Indication) {
        this.zone2Indication = zone2Indication;
    }

    public String getZone3Indication() {
        return zone3Indication;
    }

    public void setZone3Indication(String zone3Indication) {
        this.zone3Indication = zone3Indication;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getBotId() {
        return botId;
    }

    public void setBotId(String botId) {
        this.botId = botId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMessageChannel() {
        return messageChannel;
    }

    public void setMessageChannel(String messageChannel) {
        this.messageChannel = messageChannel;
    }
}
