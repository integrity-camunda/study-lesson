package ua.com.integrity.camunda.demo.process;

import org.camunda.bpm.engine.delegate.BpmnError;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import ua.com.integrity.camunda.demo.model.Indication;

import javax.inject.Named;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.RoundingMode;

@Named
@Transactional(value = Transactional.TxType.MANDATORY, dontRollbackOn = BpmnError.class)
public class CountDifference implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        Indication prevIndication = (Indication) execution.getVariable(ProcessVariables.PREVIOUS_INDICATIONS);
        int zonesNumber = (Integer) execution.getVariable(ProcessVariables.ZONES_NUMBER);
        String sourceChannel = (String) execution.getVariable(ProcessVariables.CHANNEL_TYPE);

        String zone1Value = (String) execution.getVariable(ProcessVariables.ZONE_1_VALUE);
        String zone2Value = (String) execution.getVariable(ProcessVariables.ZONE_2_VALUE);
        String zone3Value = (String) execution.getVariable(ProcessVariables.ZONE_3_VALUE);

        BigDecimal zone1Diff = BigDecimal.ZERO;
        BigDecimal zone2Diff = BigDecimal.ZERO;
        BigDecimal zone3Diff = BigDecimal.ZERO;

        switch (zonesNumber) {
            case 3:
                zone3Diff = calculateDifference(prevIndication.getZone3Indication(), zone3Value);
                validateDifference(zone3Diff, sourceChannel);
            case 2:
                zone2Diff = calculateDifference(prevIndication.getZone2Indication(), zone2Value);
                validateDifference(zone2Diff, sourceChannel);
            case 1:
                zone1Diff = calculateDifference(prevIndication.getZone1Indication(), zone1Value);
                validateDifference(zone1Diff, sourceChannel);
                break;
            default:
                throwErrorEvent(sourceChannel);
        }

        execution.setVariable(ProcessVariables.ZONE_3_DIFF, zone3Diff.toPlainString());
        execution.setVariable(ProcessVariables.ZONE_2_DIFF, zone2Diff.toPlainString());
        execution.setVariable(ProcessVariables.ZONE_1_DIFF, zone1Diff.toPlainString());


        String accountNumber = (String) execution.getVariable(ProcessVariables.CUSTOMER_ACCOUNT);
        Indication currentIndication = new Indication(accountNumber, zonesNumber);
        currentIndication.setZone1Indication(new BigDecimal(zone1Value));
        currentIndication.setZone2Indication(new BigDecimal(zone2Value));
        currentIndication.setZone3Indication(new BigDecimal(zone3Value));
        currentIndication.setSourceChannel(sourceChannel);
        execution.setVariable(ProcessVariables.CURRENT_INDICATIONS, currentIndication);
    }

    private static void throwErrorEvent(String sourceChannel) {
        if (sourceChannel.equals(ProcessVariables.CHATBOT_CHANNEL)) {
            throw new BpmnError("INVALID_INDICATIONS");
        } else {
            throw new IllegalArgumentException("Перевірте корректність введення даних лічильника та показників.");
        }
    }

    private static void validateDifference(BigDecimal difference, String sourceChannel) {
        boolean differenceIsNegative = (difference.compareTo(BigDecimal.ZERO) == -1);
        boolean differenceIsTooLarge = (difference.compareTo(BigDecimal.valueOf(500L)) >= 0);
        if (differenceIsNegative || differenceIsTooLarge) {
            throwErrorEvent(sourceChannel);
        }
    }

    public static BigDecimal calculateDifference(BigDecimal previousValue, String currentValue) {
        return new BigDecimal(currentValue).subtract(previousValue).setScale(3, RoundingMode.DOWN);
    }
}
