package ua.com.integrity.camunda.demo.process;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.variable.Variables;
import ua.com.integrity.camunda.demo.model.Indication;
import ua.com.integrity.camunda.demo.service.MailService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.mail.internet.MimeBodyPart;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

import static ua.com.integrity.camunda.demo.process.ProcessVariables.*;

@Named
@Stateless
public class CreateAndSendBill implements JavaDelegate {

    @Inject
    private MailService mailService;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        Map<String, Object> processVars = execution.getVariables();
        Map<String, Object> templateVars = buildTemplateVariables(processVars);
        String subject = "Квитанция на оплату ДТЕК демо";
        String customerEmail = (String) processVars.get(CUSTOMER_EMAIL);
        MimeBodyPart attachment = mailService.generateAttachment(templateVars);
        mailService.sendMessage(subject, customerEmail, attachment);
    }



    private static final String ZONE_1_PREV_VALUE = "zone1PrevValue";
    private static final String ZONE_2_PREV_VALUE = "zone2PrevValue";
    private static final String ZONE_3_PREV_VALUE = "zone3PrevValue";
    private static final String BILL_NUMBER = "billNumber";
    private static final String BILL_DATE = "billDate";

    private static Map<String, Object> buildTemplateVariables(Map<String, Object> processVars) {
        Indication prevIndication = (Indication) processVars.get(PREVIOUS_INDICATIONS);
        Indication currIndication = (Indication) processVars.get(CURRENT_INDICATIONS);
        Date billDate = Optional.ofNullable(currIndication.getInsertTime()).orElse(new Date());
        return Variables
                .putValue(CUSTOMER_ACCOUNT, processVars.get(CUSTOMER_ACCOUNT))
                .putValue(ZONE_1_VALUE, processVars.get(ZONE_1_VALUE))
                .putValue(ZONE_2_VALUE, processVars.get(ZONE_2_VALUE))
                .putValue(ZONE_3_VALUE,processVars.get(ZONE_3_VALUE))
                .putValue(ZONE_1_PREV_VALUE,prevIndication.getZone1Indication().toPlainString())
                .putValue(ZONE_2_PREV_VALUE,prevIndication.getZone2Indication().toPlainString())
                .putValue(ZONE_3_PREV_VALUE,prevIndication.getZone3Indication().toPlainString())
                .putValue(ZONE_1_TARIFF_LT_100,processVars.get(ZONE_1_TARIFF_LT_100))
                .putValue(ZONE_1_TARIFF_GT_100,processVars.get(ZONE_1_TARIFF_GT_100))
                .putValue(ZONE_2_TARIFF_LT_100,processVars.get(ZONE_2_TARIFF_LT_100))
                .putValue(ZONE_2_TARIFF_GT_100,processVars.get(ZONE_2_TARIFF_GT_100))
                .putValue(ZONE_3_TARIFF_LT_100,processVars.get(ZONE_3_TARIFF_LT_100))
                .putValue(ZONE_3_TARIFF_GT_100,processVars.get(ZONE_3_TARIFF_GT_100))
                .putValue(PREVIOUS_BALANCE,processVars.get(PREVIOUS_BALANCE))
                .putValue(CURRENT_BALANCE,processVars.get(CURRENT_BALANCE))
                .putValue(NEW_CHARGES,processVars.get(NEW_CHARGES))
                .putValue(BILL_NUMBER, currIndication.getId())
                .putValue(BILL_DATE, new SimpleDateFormat("dd.MM.yyyy").format(billDate));

    }

    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }
}
