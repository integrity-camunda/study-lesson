package ua.com.integrity.camunda.demo.rest.model;

import java.util.ArrayList;
import java.util.List;

public class ChatBotPushRequest {

    private String text;
    private List<String> receivers = new ArrayList<>();

    public ChatBotPushRequest() {
    }

    public ChatBotPushRequest(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<String> getReceivers() {
        return receivers;
    }

    public void setReceivers(List<String> receivers) {
        this.receivers = receivers;
    }
}
