package ua.com.integrity.camunda.demo.process;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import ua.com.integrity.camunda.demo.dao.IndicationDao;
import ua.com.integrity.camunda.demo.model.Indication;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Named;
import java.util.Optional;

@Named
@Stateless
public class FindPreviousIndications implements JavaDelegate {

    @EJB
    private IndicationDao indicationDao;

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        String accountNumber = (String) execution.getVariable(ProcessVariables.CUSTOMER_ACCOUNT);
        int zonesNumber = (Integer) execution.getVariable(ProcessVariables.ZONES_NUMBER);
        Optional<Indication> latestIndication = indicationDao.findLatestIndication(accountNumber, zonesNumber);
        if (latestIndication.isPresent()) {
            Indication indication = latestIndication.get();
            execution.setVariable(ProcessVariables.PREVIOUS_INDICATIONS, indication);
            execution.setVariable(ProcessVariables.PREVIOUS_BALANCE, indication.getOverallBalance().toPlainString());
        } else {
            Indication previousNullValueIndications = new Indication(accountNumber,zonesNumber);
            execution.setVariable(ProcessVariables.PREVIOUS_INDICATIONS, previousNullValueIndications);
            execution.setVariable(ProcessVariables.PREVIOUS_BALANCE, previousNullValueIndications.getOverallBalance().toPlainString());
        }
    }

    public void setIndicationDao(IndicationDao indicationDao) {
        this.indicationDao = indicationDao;
    }
}
