package ua.com.integrity.camunda.demo.rest.config;

import javax.enterprise.inject.Produces;
import javax.net.ssl.SSLContext;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.security.NoSuchAlgorithmException;

public class ClientProducer {

    private static final String CHAT_BOT_URL = "https://example.com";

    @Produces @DefaultWebTarget
    public WebTarget produceDefaultClient() throws NoSuchAlgorithmException {
        return ClientBuilder
                .newBuilder()
                .sslContext(SSLContext.getDefault())
                .build()
                .target(CHAT_BOT_URL);
    }
}
