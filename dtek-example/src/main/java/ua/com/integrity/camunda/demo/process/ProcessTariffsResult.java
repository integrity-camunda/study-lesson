package ua.com.integrity.camunda.demo.process;

import org.camunda.bpm.dmn.engine.DmnDecisionResult;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;

import javax.ejb.Stateless;
import javax.inject.Named;
import java.util.Map;

import static ua.com.integrity.camunda.demo.process.ProcessVariables.*;

@Named
@Stateless
public class ProcessTariffsResult implements ExecutionListener {
    @Override
    public void notify(DelegateExecution execution) throws Exception {
        DmnDecisionResult decisionResult = (DmnDecisionResult) execution.getVariable("decisionResult");
        Map<String, Object> entryMap = decisionResult.getSingleResult().getEntryMap();
        Map<String, Object> processVars = execution.getVariables();
        processVars.put(ZONE_1_TARIFF_LT_100, entryMap.get(ZONE_1_TARIFF_LT_100));
        processVars.put(ZONE_1_TARIFF_GT_100, entryMap.get(ZONE_1_TARIFF_GT_100));
        processVars.put(ZONE_2_TARIFF_LT_100, entryMap.get(ZONE_2_TARIFF_LT_100));
        processVars.put(ZONE_2_TARIFF_GT_100, entryMap.get(ZONE_2_TARIFF_GT_100));
        processVars.put(ZONE_3_TARIFF_LT_100, entryMap.get(ZONE_3_TARIFF_LT_100));
        processVars.put(ZONE_3_TARIFF_GT_100, entryMap.get(ZONE_3_TARIFF_GT_100));

        execution.setVariables(processVars);
    }
}
