package ua.com.integrity.camunda.demo.service;

import fr.opensagres.xdocreport.core.XDocReportException;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.annotation.Resource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public class MailService {

    private static final String TEMPLATE_FILE = "template_bill.docx";
    private static final String ATTACHMENT_MIME_TYPE = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

    @Resource(name = "java:jboss/mail/hr.integrity.gmail")
    private Session session;

    public MimeBodyPart generateAttachment(Map<String, Object> templateVars) throws IOException, XDocReportException, MessagingException {
        try (InputStream templateFileStream = this.getClass().getClassLoader().getResourceAsStream(TEMPLATE_FILE);
             ByteArrayOutputStream outStream = new ByteArrayOutputStream()) {
            DocEngine.insertData(templateFileStream, templateVars, outStream);
            byte[] bytes = outStream.toByteArray();
            MimeBodyPart attachment = new MimeBodyPart();
            DataSource dataSource = new ByteArrayDataSource(outStream.toByteArray(), ATTACHMENT_MIME_TYPE);
            attachment.setDataHandler(new DataHandler(dataSource));
            attachment.setFileName("bill.docx");
            return attachment;
        }
    }

    public void sendMessage(String subject, String email, MimeBodyPart attachment) throws MessagingException {
        Multipart messageBody = new MimeMultipart();
        messageBody.addBodyPart(attachment);
        MimeMessage mailMessage = new MimeMessage(session);
        mailMessage.setContent(messageBody);
        mailMessage.setFrom(InternetAddress.getLocalAddress(session));
        mailMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
        mailMessage.setSubject(subject, "UTF-8");
        Transport.send(mailMessage);
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }
}
