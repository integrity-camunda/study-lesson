Данный проект содержит в себе примеры реализации бизнес процессов с применением Camunda Platform, а также задания для тестового выполнения во врем обучения.

# Тестовые задания
Тестовые задания размещены в каталоге [tasks](tasks).
Для выполнения тестовых заданий необходимо организовывать интеграцию с ERP системой.
ERP система выставляет REST и SOAP интерфейсы для работы с ней.
OpenAPI описание REST интерфейсов находится по [ссылке](https://wisconsin.integrity.com.ua/openapi/erp?format=JSON)
WSDL файлы отдельно для работы с [орг-структурой](https://wisconsin.integrity.com.ua/erp/org-structure?wsdl), [материалами](https://wisconsin.integrity.com.ua/erp/materials?wsdl), [оборудованием](https://wisconsin.integrity.com.ua/erp/equipments?wsdl).
Исходный код ERP можно найти в модуле [erp](erp).
Для работы с ERP можно использовать ДТО классы из модуля [erp-dto](erp-dto). 