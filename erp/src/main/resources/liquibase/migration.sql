-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: src/main/resources/liquibase/changelog-master.xml
-- Ran at: 02.07.21 10:02
-- Against: erp_user@jdbc:postgresql:simple_erp_db
-- Liquibase version: 4.2.0
-- *********************************************************************

-- Create Database Lock Table
CREATE TABLE databasechangeloglock (ID INTEGER NOT NULL, LOCKED BOOLEAN NOT NULL, LOCKGRANTED TIMESTAMP WITHOUT TIME ZONE, LOCKEDBY VARCHAR(255), CONSTRAINT DATABASECHANGELOGLOCK_PKEY PRIMARY KEY (ID));

-- Initialize Database Lock Table
DELETE FROM databasechangeloglock;

INSERT INTO databasechangeloglock (ID, LOCKED) VALUES (1, FALSE);

-- Lock Database
UPDATE databasechangeloglock SET LOCKED = TRUE, LOCKEDBY = 'NB-38 (192.168.99.1)', LOCKGRANTED = '2021-07-02 10:02:44.734' WHERE ID = 1 AND LOCKED = FALSE;

-- Create Database Change Log Table
CREATE TABLE databasechangelog (ID VARCHAR(255) NOT NULL, AUTHOR VARCHAR(255) NOT NULL, FILENAME VARCHAR(255) NOT NULL, DATEEXECUTED TIMESTAMP WITHOUT TIME ZONE NOT NULL, ORDEREXECUTED INTEGER NOT NULL, EXECTYPE VARCHAR(10) NOT NULL, MD5SUM VARCHAR(35), DESCRIPTION VARCHAR(255), COMMENTS VARCHAR(255), TAG VARCHAR(255), LIQUIBASE VARCHAR(20), CONTEXTS VARCHAR(255), LABELS VARCHAR(255), DEPLOYMENT_ID VARCHAR(10));

-- Changeset src/main/resources/liquibase/org-structure/create-org-structure-changelog.xml::add-org-structure-tables::vivanchenko
CREATE SEQUENCE  IF NOT EXISTS org_structure_seq START WITH 5000;

CREATE TABLE org_unit (id BIGINT NOT NULL, title TEXT, ascendant BIGINT, CONSTRAINT ORG_UNIT_PKEY PRIMARY KEY (id), CONSTRAINT ascendant_fk FOREIGN KEY (ascendant) REFERENCES org_unit(id));

CREATE TABLE org_position (id BIGINT NOT NULL, title TEXT, index INTEGER, org_unit_id BIGINT, CONSTRAINT ORG_POSITION_PKEY PRIMARY KEY (id), CONSTRAINT org_unit_fk FOREIGN KEY (org_unit_id) REFERENCES org_unit(id));

CREATE TABLE employee (id BIGINT NOT NULL, f_name TEXT, l_name TEXT, birth_date date, hire_date date, fire_date date, position_id BIGINT, CONSTRAINT EMPLOYEE_PKEY PRIMARY KEY (id), CONSTRAINT org_position_fk FOREIGN KEY (position_id) REFERENCES org_position(id));

CREATE TABLE vacations (id BIGINT NOT NULL, start_date date, duration INTEGER, type TEXT, employee_id BIGINT, CONSTRAINT VACATIONS_PKEY PRIMARY KEY (id), CONSTRAINT employee FOREIGN KEY (employee_id) REFERENCES employee(id));

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('add-org-structure-tables', 'vivanchenko', 'src/main/resources/liquibase/org-structure/create-org-structure-changelog.xml', NOW(), 1, '8:5145661b6d7974a7513efdf071055d05', 'createSequence sequenceName=org_structure_seq; createTable tableName=org_unit; createTable tableName=org_position; createTable tableName=employee; createTable tableName=vacations', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/materials/create-materials-changelog.xml::add-materials-related-tables::vivanchenko
CREATE SEQUENCE  IF NOT EXISTS materials_seq START WITH 99000;

CREATE TABLE materials (id BIGINT NOT NULL, title TEXT, measure_unit TEXT, CONSTRAINT MATERIALS_PKEY PRIMARY KEY (id));

CREATE SEQUENCE  IF NOT EXISTS material_batches_seq START WITH 166000;

CREATE TABLE material_batches (id BIGINT NOT NULL, material_id BIGINT, purchase_date date, purchase_amount DECIMAL, price DECIMAL, remaining_amount DECIMAL, CONSTRAINT MATERIAL_BATCHES_PKEY PRIMARY KEY (id), CONSTRAINT materials_fk FOREIGN KEY (material_id) REFERENCES materials(id));

CREATE TABLE consumption_rates (id BIGINT NOT NULL, title TEXT, material_id BIGINT, duration_unit TEXT, duration_amount BIGINT, amount DECIMAL, CONSTRAINT CONSUMPTION_RATES_PKEY PRIMARY KEY (id), CONSTRAINT materials_fk FOREIGN KEY (material_id) REFERENCES materials(id));

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('add-materials-related-tables', 'vivanchenko', 'src/main/resources/liquibase/materials/create-materials-changelog.xml', NOW(), 2, '8:e319ae491a6edc39b9fd8ebc6f2d1925', 'createSequence sequenceName=materials_seq; createTable tableName=materials; createSequence sequenceName=material_batches_seq; createTable tableName=material_batches; createTable tableName=consumption_rates', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/equipment/create-equipment-changelog.xml::add-equipment-related-tables::vivanchenko
CREATE SEQUENCE  IF NOT EXISTS maintenance_seq START WITH 116000;

CREATE TABLE maintenance (id BIGINT NOT NULL, title TEXT, CONSTRAINT MAINTENANCE_PKEY PRIMARY KEY (id));

CREATE TABLE maintenance_consumption_rate (maintenance_type BIGINT, consumption_rate BIGINT, CONSTRAINT consumption_rates_fk FOREIGN KEY (consumption_rate) REFERENCES consumption_rates(id), CONSTRAINT maintenance_fk FOREIGN KEY (maintenance_type) REFERENCES maintenance(id));

CREATE SEQUENCE  IF NOT EXISTS equipment_types_seq START WITH 33000;

CREATE TABLE equipment_type (id BIGINT NOT NULL, title TEXT, description TEXT, life_time INTEGER, CONSTRAINT EQUIPMENT_TYPE_PKEY PRIMARY KEY (id));

CREATE TABLE equipment_maintenances (equipment_type BIGINT, maintenance_type BIGINT, CONSTRAINT maintenance_fk FOREIGN KEY (maintenance_type) REFERENCES maintenance(id), CONSTRAINT equipment_type_fk FOREIGN KEY (equipment_type) REFERENCES equipment_type(id));

CREATE TABLE equipment_consumption_rates (equipment_type BIGINT, consumption_rate BIGINT, CONSTRAINT equipment_type_fk FOREIGN KEY (equipment_type) REFERENCES equipment_type(id), CONSTRAINT consumption_rates_fk FOREIGN KEY (consumption_rate) REFERENCES consumption_rates(id));

CREATE TABLE equipment (serial_number TEXT NOT NULL, type BIGINT, purchase_price DECIMAL, balance_price DECIMAL, commissioning_date date, decommissioning_date date, employee_id BIGINT, CONSTRAINT EQUIPMENT_PKEY PRIMARY KEY (serial_number), CONSTRAINT employee_fk FOREIGN KEY (employee_id) REFERENCES employee(id), CONSTRAINT equipment_type_fk FOREIGN KEY (type) REFERENCES equipment_type(id));

CREATE SEQUENCE  IF NOT EXISTS rates_seq START WITH 22000;

CREATE TABLE supply_rates (id BIGINT NOT NULL, title TEXT, equipment_type_id BIGINT, amount INTEGER, CONSTRAINT SUPPLY_RATES_PKEY PRIMARY KEY (id), CONSTRAINT equipment_type_fk FOREIGN KEY (equipment_type_id) REFERENCES equipment_type(id));

CREATE TABLE positions_supply_rates (position_id BIGINT, sup_rate_id BIGINT, CONSTRAINT supply_rates_fk FOREIGN KEY (sup_rate_id) REFERENCES supply_rates(id), CONSTRAINT org_position_fk FOREIGN KEY (position_id) REFERENCES org_position(id));

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('add-equipment-related-tables', 'vivanchenko', 'src/main/resources/liquibase/equipment/create-equipment-changelog.xml', NOW(), 3, '8:a270372e6ae9065a8d3aa15c64f90f95', 'createSequence sequenceName=maintenance_seq; createTable tableName=maintenance; createTable tableName=maintenance_consumption_rate; createSequence sequenceName=equipment_types_seq; createTable tableName=equipment_type; createTable tableName=equipm...', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/org-structure/init-fill-org-structure.xml::add-org-units::vivanchenko
INSERT INTO org_unit (id, title) VALUES (1, 'ООО ТЕСТОВАЯ КОМПАНИЯ');

INSERT INTO org_unit (id, title, ascendant) VALUES (2, 'Инженерный отдел', '1');

INSERT INTO org_unit (id, title, ascendant) VALUES (3, 'Отдел продаж', '1');

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('add-org-units', 'vivanchenko', 'src/main/resources/liquibase/org-structure/init-fill-org-structure.xml', NOW(), 4, '8:602823d6ba9aee4b3038394b36432d19', 'insert tableName=org_unit; insert tableName=org_unit; insert tableName=org_unit', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/org-structure/init-fill-org-structure.xml::add-positions-to-main-unit::vivanchenko
INSERT INTO org_position (id, org_unit_id, index, title) VALUES (11, 1, 1, 'Директор');

INSERT INTO org_position (id, org_unit_id, index, title) VALUES (12, 1, 2, 'Бухгалтер');

INSERT INTO org_position (id, org_unit_id, index, title) VALUES (13, 1, 3, 'Заведующий складом');

INSERT INTO org_position (id, org_unit_id, index, title) VALUES (14, 1, 4, 'HR-менеждер');

INSERT INTO org_position (id, org_unit_id, index, title) VALUES (15, 1, 5, 'Системный администратор');

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('add-positions-to-main-unit', 'vivanchenko', 'src/main/resources/liquibase/org-structure/init-fill-org-structure.xml', NOW(), 5, '8:f883b26e01281d5d4f3b076a9711b4b6', 'insert tableName=org_position; insert tableName=org_position; insert tableName=org_position; insert tableName=org_position; insert tableName=org_position', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/org-structure/init-fill-org-structure.xml::add-positions-to-sales-unit::vivanchenko
INSERT INTO org_position (id, org_unit_id, index, title) VALUES (21, 2, 1, 'Начальник отдела');

INSERT INTO org_position (id, org_unit_id, index, title) VALUES (22, 2, 2, 'Главный сцециалист по продажам');

INSERT INTO org_position (id, org_unit_id, index, title) VALUES (23, 2, 3, 'Ведущий специалист по продажам');

INSERT INTO org_position (id, org_unit_id, index, title) VALUES (24, 2, 4, 'Специалист по продажам');

INSERT INTO org_position (id, org_unit_id, index, title) VALUES (25, 2, 5, 'Специалист по продажам');

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('add-positions-to-sales-unit', 'vivanchenko', 'src/main/resources/liquibase/org-structure/init-fill-org-structure.xml', NOW(), 6, '8:d8e0f39ee2f69205ff90d1d392b7f541', 'insert tableName=org_position; insert tableName=org_position; insert tableName=org_position; insert tableName=org_position; insert tableName=org_position', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/org-structure/init-fill-org-structure.xml::add-positions-to-engineers-unit::vivanchenko
INSERT INTO org_position (id, org_unit_id, index, title) VALUES (31, 3, 1, 'Начальник отдела');

INSERT INTO org_position (id, org_unit_id, index, title) VALUES (32, 3, 2, 'Главный инженер');

INSERT INTO org_position (id, org_unit_id, index, title) VALUES (33, 3, 3, 'Ведущий инженер');

INSERT INTO org_position (id, org_unit_id, index, title) VALUES (34, 3, 4, 'Ведущий инженер');

INSERT INTO org_position (id, org_unit_id, index, title) VALUES (35, 3, 5, 'Инженер');

INSERT INTO org_position (id, org_unit_id, index, title) VALUES (36, 3, 6, 'Инженер');

INSERT INTO org_position (id, org_unit_id, index, title) VALUES (37, 3, 7, 'Инженер');

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('add-positions-to-engineers-unit', 'vivanchenko', 'src/main/resources/liquibase/org-structure/init-fill-org-structure.xml', NOW(), 7, '8:717b0669eb470a4c78418c52ca316cbe', 'insert tableName=org_position; insert tableName=org_position; insert tableName=org_position; insert tableName=org_position; insert tableName=org_position; insert tableName=org_position; insert tableName=org_position', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/org-structure/init-fill-org-structure.xml::add-employees-to-main-unit::vivanchenko
INSERT INTO employee (id, f_name, l_name, birth_date, hire_date, position_id) VALUES (111, 'Олег', 'Главный', '1974-05-25', '2006-01-23', 11);

INSERT INTO employee (id, f_name, l_name, birth_date, hire_date, position_id) VALUES (121, 'Светлана', 'Копейка', '1972-11-22', '2009-12-13', 12);

INSERT INTO employee (id, f_name, l_name, birth_date, hire_date, position_id) VALUES (131, 'Иван', 'Старшина', '1982-01-02', '2006-12-13', 13);

INSERT INTO employee (id, f_name, l_name, birth_date, hire_date, position_id) VALUES (141, 'Виктория', 'Кадр', '1995-04-01', '2019-05-06', 14);

INSERT INTO employee (id, f_name, l_name, birth_date, hire_date, position_id) VALUES (151, 'Андрей', 'Ад', '1967-11-22', '2011-07-09', 15);

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('add-employees-to-main-unit', 'vivanchenko', 'src/main/resources/liquibase/org-structure/init-fill-org-structure.xml', NOW(), 8, '8:90974413d322d9a51079491d94a169f1', 'insert tableName=employee; insert tableName=employee; insert tableName=employee; insert tableName=employee; insert tableName=employee', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/org-structure/init-fill-org-structure.xml::add-employees-to-sales-unit::vivanchenko
INSERT INTO employee (id, f_name, l_name, birth_date, hire_date, position_id) VALUES (211, 'Петр', 'Продажный', '1988-07-25', '2007-12-29', 21);

INSERT INTO employee (id, f_name, l_name, birth_date, hire_date, position_id) VALUES (221, 'Ольга', 'Рыночная', '2000-07-25', '2020-12-29', 22);

INSERT INTO employee (id, f_name, l_name, birth_date, hire_date, position_id) VALUES (241, 'Антон', 'Барыга', '1992-07-04', '2018-02-16', 24);

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('add-employees-to-sales-unit', 'vivanchenko', 'src/main/resources/liquibase/org-structure/init-fill-org-structure.xml', NOW(), 9, '8:ed99e8dfd4630473eddf2f75af7b7c4c', 'insert tableName=employee; insert tableName=employee; insert tableName=employee', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/org-structure/init-fill-org-structure.xml::add-employees-to-engineering-unit::vivanchenko
INSERT INTO employee (id, f_name, l_name, birth_date, hire_date, position_id) VALUES (311, 'Юлия', 'Иванова', '1992-07-25', '2019-07-21', 31);

INSERT INTO employee (id, f_name, l_name, birth_date, hire_date, position_id) VALUES (321, 'Виктор', 'Фахивец', '1990-09-25', '2016-03-09', 32);

INSERT INTO employee (id, f_name, l_name, birth_date, hire_date, position_id) VALUES (331, 'Степан', 'Разин', '1978-08-12', '2000-12-09', 33);

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('add-employees-to-engineering-unit', 'vivanchenko', 'src/main/resources/liquibase/org-structure/init-fill-org-structure.xml', NOW(), 10, '8:ac699f3330d4b3d1323a0e19c9e5aca7', 'insert tableName=employee; insert tableName=employee; insert tableName=employee', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/materials/init-fill-materials.xml::add-init-materials::vivanchenko
INSERT INTO materials (id, measure_unit, title) VALUES (51, 'PCS', 'Бумага офисная А4');

INSERT INTO materials (id, measure_unit, title) VALUES (52, 'PKG', 'Бумага офисная А4 в упаковке (500 шт)');

INSERT INTO materials (id, measure_unit, title) VALUES (53, 'PCS', 'Бумага офисная А3');

INSERT INTO materials (id, measure_unit, title) VALUES (54, 'PKG', 'Бумага офисная А3 в упаковке (100 шт)');

INSERT INTO materials (id, measure_unit, title) VALUES (55, 'PKG', 'Тонер IPM HP 1005/1505');

INSERT INTO materials (id, measure_unit, title) VALUES (56, 'PKG', 'Тонер PRINTALIST HP CLJ универсальный');

INSERT INTO materials (id, measure_unit, title) VALUES (57, 'PKG', 'Тонер PRINTALIST MPT5');

INSERT INTO materials (id, measure_unit, title) VALUES (58, 'PCS', 'Тонер-картридж OKI C310/C330/C510/C530 Black');

INSERT INTO materials (id, measure_unit, title) VALUES (59, 'PCS', 'Тонер-картридж OKI C3100 magenta');

INSERT INTO materials (id, measure_unit, title) VALUES (60, 'LITER', 'Спирт технический');

INSERT INTO materials (id, measure_unit, title) VALUES (61, 'METER', 'Кабель SFTP cat.4');

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('add-init-materials', 'vivanchenko', 'src/main/resources/liquibase/materials/init-fill-materials.xml', NOW(), 11, '8:16942d24311b63f87ab4bc5c1e5ef36b', 'insert tableName=materials; insert tableName=materials; insert tableName=materials; insert tableName=materials; insert tableName=materials; insert tableName=materials; insert tableName=materials; insert tableName=materials; insert tableName=materi...', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/materials/init-fill-materials.xml::add-init-batch-of-materials::vivanchenko
INSERT INTO material_batches (id, material_id, purchase_date, purchase_amount, price, remaining_amount) VALUES (511, 51, '2020-05-25', 1000, 88.50, 202);

INSERT INTO material_batches (id, material_id, purchase_date, purchase_amount, price, remaining_amount) VALUES (521, 52, '2019-05-25', 9, 698.20, 2);

INSERT INTO material_batches (id, material_id, purchase_date, purchase_amount, price, remaining_amount) VALUES (522, 52, '2021-01-25', 15, 1150, 6);

INSERT INTO material_batches (id, material_id, purchase_date, purchase_amount, price, remaining_amount) VALUES (541, 54, '2019-05-25', 3, 300, 1);

INSERT INTO material_batches (id, material_id, purchase_date, purchase_amount, price, remaining_amount) VALUES (551, 55, '2019-05-25', 5, 1567, 5);

INSERT INTO material_batches (id, material_id, purchase_date, purchase_amount, price, remaining_amount) VALUES (561, 56, '2019-05-25', 12, 2078.20, 1);

INSERT INTO material_batches (id, material_id, purchase_date, purchase_amount, price, remaining_amount) VALUES (581, 58, '2019-05-25', 3, 1500, 1);

INSERT INTO material_batches (id, material_id, purchase_date, purchase_amount, price, remaining_amount) VALUES (582, 58, '2020-05-25', 6, 1500, 3);

INSERT INTO material_batches (id, material_id, purchase_date, purchase_amount, price, remaining_amount) VALUES (591, 59, '2019-05-25', 3, 3600, 2);

INSERT INTO material_batches (id, material_id, purchase_date, purchase_amount, price, remaining_amount) VALUES (601, 60, '2017-05-25', 12.5, 428.78, 0.25);

INSERT INTO material_batches (id, material_id, purchase_date, purchase_amount, price, remaining_amount) VALUES (611, 61, '2017-05-25', 403, 968, 105.4);

INSERT INTO material_batches (id, material_id, purchase_date, purchase_amount, price, remaining_amount) VALUES (612, 61, '2020-05-25', 403, 1069, 203.4);

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('add-init-batch-of-materials', 'vivanchenko', 'src/main/resources/liquibase/materials/init-fill-materials.xml', NOW(), 12, '8:ce0edc11b3490270111140e72e4bd18b', 'insert tableName=material_batches; insert tableName=material_batches; insert tableName=material_batches; insert tableName=material_batches; insert tableName=material_batches; insert tableName=material_batches; insert tableName=material_batches; in...', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/materials/init-fill-materials.xml::add-init-consumption-rates::vivanchenko
INSERT INTO consumption_rates (id, material_id, title, duration_unit, duration_amount, amount) VALUES (1051, 51, 'Расход бумаги на принтере', 'WEEKS', 1, 55);

INSERT INTO consumption_rates (id, material_id, title, amount) VALUES (1056, 57, 'Расход тонера при малой заправке', 1);

INSERT INTO consumption_rates (id, material_id, title, amount) VALUES (1058, 58, 'Расход катриджей при полной замене', 3);

INSERT INTO consumption_rates (id, material_id, title, amount) VALUES (1059, 59, 'Расход катриджей при неполной замене', 1);

INSERT INTO consumption_rates (id, material_id, title, amount) VALUES (1060, 60, 'Норма расхода спирта №1', 0.25);

INSERT INTO consumption_rates (id, material_id, title, amount) VALUES (1061, 60, 'Норма расхода спирта №2', 0.5);

INSERT INTO consumption_rates (id, material_id, title, amount) VALUES (1062, 60, 'Норма расхода спирта №3', 0.7);

INSERT INTO consumption_rates (id, material_id, title, amount) VALUES (1063, 60, 'Норма расхода спирта №4', 1.25);

INSERT INTO consumption_rates (id, material_id, title, amount) VALUES (1064, 61, 'Расход кабеля при ЕТО', 0.01);

INSERT INTO consumption_rates (id, material_id, title, amount) VALUES (1065, 61, 'Расход кабеля при ТО-1', 1.3);

INSERT INTO consumption_rates (id, material_id, title, amount) VALUES (1066, 61, 'Расход кабеля при ТО-2', 26.1);

INSERT INTO consumption_rates (id, material_id, title, amount) VALUES (1067, 61, 'Расход кабеля при замене', 305.1);

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('add-init-consumption-rates', 'vivanchenko', 'src/main/resources/liquibase/materials/init-fill-materials.xml', NOW(), 13, '8:4230293a3ce61a325fe35d6ab6fa6f66', 'insert tableName=consumption_rates; insert tableName=consumption_rates; insert tableName=consumption_rates; insert tableName=consumption_rates; insert tableName=consumption_rates; insert tableName=consumption_rates; insert tableName=consumption_ra...', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/equipment/init-fill-equipment-type-data.xml::fill-maintenance-list::vivanchenko
INSERT INTO maintenance (id, title) VALUES (2001, 'Ежедневное ТО принтера');

INSERT INTO maintenance (id, title) VALUES (2002, 'Дозаправка картриджа');

INSERT INTO maintenance (id, title) VALUES (2003, 'Замена картриджа');

INSERT INTO maintenance (id, title) VALUES (2004, 'Средний ремонт принтера');

INSERT INTO maintenance (id, title) VALUES (2005, 'Регламентное обслуживание принтера');

INSERT INTO maintenance (id, title) VALUES (2006, 'Полугодовое обслуживание ПК');

INSERT INTO maintenance (id, title) VALUES (2007, 'Текущий ремонт ПК');

INSERT INTO maintenance (id, title) VALUES (2008, 'Полугодовое обслуживание сервера');

INSERT INTO maintenance (id, title) VALUES (2009, 'Обслуживание кабельной линии');

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('fill-maintenance-list', 'vivanchenko', 'src/main/resources/liquibase/equipment/init-fill-equipment-type-data.xml', NOW(), 14, '8:429c9610f90a66f63eede4a72d254306', 'insert tableName=maintenance; insert tableName=maintenance; insert tableName=maintenance; insert tableName=maintenance; insert tableName=maintenance; insert tableName=maintenance; insert tableName=maintenance; insert tableName=maintenance; insert ...', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/equipment/init-fill-equipment-type-data.xml::map-consumption-rates-to-maintenance::vivanchenko
INSERT INTO maintenance_consumption_rate (maintenance_type, consumption_rate) VALUES (2001, 1060);

INSERT INTO maintenance_consumption_rate (maintenance_type, consumption_rate) VALUES (2002, 1060);

INSERT INTO maintenance_consumption_rate (maintenance_type, consumption_rate) VALUES (2002, 1056);

INSERT INTO maintenance_consumption_rate (maintenance_type, consumption_rate) VALUES (2003, 1060);

INSERT INTO maintenance_consumption_rate (maintenance_type, consumption_rate) VALUES (2003, 1059);

INSERT INTO maintenance_consumption_rate (maintenance_type, consumption_rate) VALUES (2003, 1058);

INSERT INTO maintenance_consumption_rate (maintenance_type, consumption_rate) VALUES (2004, 1061);

INSERT INTO maintenance_consumption_rate (maintenance_type, consumption_rate) VALUES (2005, 1060);

INSERT INTO maintenance_consumption_rate (maintenance_type, consumption_rate) VALUES (2005, 1056);

INSERT INTO maintenance_consumption_rate (maintenance_type, consumption_rate) VALUES (2006, 1060);

INSERT INTO maintenance_consumption_rate (maintenance_type, consumption_rate) VALUES (2007, 1061);

INSERT INTO maintenance_consumption_rate (maintenance_type, consumption_rate) VALUES (2007, 1065);

INSERT INTO maintenance_consumption_rate (maintenance_type, consumption_rate) VALUES (2008, 1066);

INSERT INTO maintenance_consumption_rate (maintenance_type, consumption_rate) VALUES (2008, 1062);

INSERT INTO maintenance_consumption_rate (maintenance_type, consumption_rate) VALUES (2009, 1063);

INSERT INTO maintenance_consumption_rate (maintenance_type, consumption_rate) VALUES (2009, 1067);

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('map-consumption-rates-to-maintenance', 'vivanchenko', 'src/main/resources/liquibase/equipment/init-fill-equipment-type-data.xml', NOW(), 15, '8:c0cc5adfa1b258f49f25c052b2f7c501', 'insert tableName=maintenance_consumption_rate; insert tableName=maintenance_consumption_rate; insert tableName=maintenance_consumption_rate; insert tableName=maintenance_consumption_rate; insert tableName=maintenance_consumption_rate; insert table...', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/equipment/init-fill-equipment-type-data.xml::fill-equipment-types::vivanchenko
INSERT INTO equipment_type (id, title, life_time) VALUES (3001, 'Ноутбук НР', 96);

INSERT INTO equipment_type (id, title, life_time) VALUES (3002, 'Ноутбук Lenovo', 60);

INSERT INTO equipment_type (id, title, life_time) VALUES (3003, 'Ноутбук Dell', 72);

INSERT INTO equipment_type (id, title, life_time) VALUES (3004, 'Ноутбук MSI', 24);

INSERT INTO equipment_type (id, title, life_time) VALUES (3005, 'ПК с монитором, клавиатурой и мишью', 60);

INSERT INTO equipment_type (id, title, life_time) VALUES (3006, 'Принтер OKI цветной', 36);

INSERT INTO equipment_type (id, title, life_time) VALUES (3007, 'МФУ Canon ч/б', 36);

INSERT INTO equipment_type (id, title, life_time) VALUES (3008, 'Принтер матричный', 120);

INSERT INTO equipment_type (id, title, life_time) VALUES (3009, 'Проектор', 55);

INSERT INTO equipment_type (id, title, life_time) VALUES (3010, 'IP-телефон Grandstream', 12);

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('fill-equipment-types', 'vivanchenko', 'src/main/resources/liquibase/equipment/init-fill-equipment-type-data.xml', NOW(), 16, '8:275cbeac49ad6989c0e9ac36c475b4a2', 'insert tableName=equipment_type; insert tableName=equipment_type; insert tableName=equipment_type; insert tableName=equipment_type; insert tableName=equipment_type; insert tableName=equipment_type; insert tableName=equipment_type; insert tableName...', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/equipment/init-fill-equipment-type-data.xml::map-equipment-types-to-maintenance::vivanchenko
INSERT INTO equipment_maintenances (maintenance_type, equipment_type) VALUES (2006, 3001);

INSERT INTO equipment_maintenances (maintenance_type, equipment_type) VALUES (2007, 3001);

INSERT INTO equipment_maintenances (maintenance_type, equipment_type) VALUES (2006, 3002);

INSERT INTO equipment_maintenances (maintenance_type, equipment_type) VALUES (2007, 3002);

INSERT INTO equipment_maintenances (maintenance_type, equipment_type) VALUES (2006, 3003);

INSERT INTO equipment_maintenances (maintenance_type, equipment_type) VALUES (2007, 3003);

INSERT INTO equipment_maintenances (maintenance_type, equipment_type) VALUES (2006, 3004);

INSERT INTO equipment_maintenances (maintenance_type, equipment_type) VALUES (2007, 3004);

INSERT INTO equipment_maintenances (maintenance_type, equipment_type) VALUES (2006, 3005);

INSERT INTO equipment_maintenances (maintenance_type, equipment_type) VALUES (2007, 3005);

INSERT INTO equipment_maintenances (maintenance_type, equipment_type) VALUES (2003, 3006);

INSERT INTO equipment_maintenances (maintenance_type, equipment_type) VALUES (2004, 3006);

INSERT INTO equipment_maintenances (maintenance_type, equipment_type) VALUES (2005, 3006);

INSERT INTO equipment_maintenances (maintenance_type, equipment_type) VALUES (2002, 3007);

INSERT INTO equipment_maintenances (maintenance_type, equipment_type) VALUES (2003, 3007);

INSERT INTO equipment_maintenances (maintenance_type, equipment_type) VALUES (2004, 3007);

INSERT INTO equipment_maintenances (maintenance_type, equipment_type) VALUES (2005, 3007);

INSERT INTO equipment_maintenances (maintenance_type, equipment_type) VALUES (2009, 3010);

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('map-equipment-types-to-maintenance', 'vivanchenko', 'src/main/resources/liquibase/equipment/init-fill-equipment-type-data.xml', NOW(), 17, '8:f18f949006affac226cc639c10ef418d', 'insert tableName=equipment_maintenances; insert tableName=equipment_maintenances; insert tableName=equipment_maintenances; insert tableName=equipment_maintenances; insert tableName=equipment_maintenances; insert tableName=equipment_maintenances; i...', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/equipment/init-fill-equipment-type-data.xml::add-init-supply-rates::vivanchenko
INSERT INTO supply_rates (id, title, equipment_type_id, amount) VALUES (4001, '1 ноутбук НР', 3001, 1);

INSERT INTO supply_rates (id, title, equipment_type_id, amount) VALUES (4011, '2 ноутбука НР', 3001, 2);

INSERT INTO supply_rates (id, title, equipment_type_id, amount) VALUES (4002, '1 ноутбук Lenovo', 3002, 1);

INSERT INTO supply_rates (id, title, equipment_type_id, amount) VALUES (4021, '2 ноутбука Lenovo', 3002, 2);

INSERT INTO supply_rates (id, title, equipment_type_id, amount) VALUES (4003, '1 ноутбук Dell', 3003, 1);

INSERT INTO supply_rates (id, title, equipment_type_id, amount) VALUES (4031, '2 ноутбука Dell', 3003, 2);

INSERT INTO supply_rates (id, title, equipment_type_id, amount) VALUES (4004, '1 ноутбук MSI', 3004, 1);

INSERT INTO supply_rates (id, title, equipment_type_id, amount) VALUES (4005, '1 рабочий ПК', 3005, 1);

INSERT INTO supply_rates (id, title, equipment_type_id, amount) VALUES (4006, '1 цветной принтер', 3006, 1);

INSERT INTO supply_rates (id, title, equipment_type_id, amount) VALUES (4007, '1 МФУ', 3007, 1);

INSERT INTO supply_rates (id, title, equipment_type_id, amount) VALUES (4009, '1 проектор', 3009, 1);

INSERT INTO supply_rates (id, title, equipment_type_id, amount) VALUES (4010, '1 телефон на рабочее место', 3010, 1);

INSERT INTO supply_rates (id, title, equipment_type_id, amount) VALUES (4110, '2 телефона на рабочее место', 3010, 2);

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('add-init-supply-rates', 'vivanchenko', 'src/main/resources/liquibase/equipment/init-fill-equipment-type-data.xml', NOW(), 18, '8:e55163bd69e60218432e104c37b18dbd', 'insert tableName=supply_rates; insert tableName=supply_rates; insert tableName=supply_rates; insert tableName=supply_rates; insert tableName=supply_rates; insert tableName=supply_rates; insert tableName=supply_rates; insert tableName=supply_rates;...', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/org-structure/init-fill-supply-rates-for-positions.xml::map-positions-and-supply-rates-main-unit::vivanchenko
INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (11, 4001);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (11, 4110);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (12, 4005);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (12, 4002);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (12, 4010);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (13, 4004);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (13, 4007);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (13, 4010);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (14, 4005);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (14, 4007);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (14, 4010);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (15, 4005);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (15, 4003);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (15, 4010);

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('map-positions-and-supply-rates-main-unit', 'vivanchenko', 'src/main/resources/liquibase/org-structure/init-fill-supply-rates-for-positions.xml', NOW(), 19, '8:63fa3eb8eee2806f1f777bb6e9ba28d3', 'insert tableName=positions_supply_rates; insert tableName=positions_supply_rates; insert tableName=positions_supply_rates; insert tableName=positions_supply_rates; insert tableName=positions_supply_rates; insert tableName=positions_supply_rates; i...', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/org-structure/init-fill-supply-rates-for-positions.xml::map-positions-and-supply-rates-sales-unit::vivanchenko
INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (21, 4001);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (21, 4110);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (21, 4009);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (21, 4006);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (22, 4001);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (22, 4010);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (22, 4005);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (22, 4007);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (23, 4001);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (23, 4010);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (23, 4005);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (24, 4001);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (24, 4010);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (24, 4005);

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('map-positions-and-supply-rates-sales-unit', 'vivanchenko', 'src/main/resources/liquibase/org-structure/init-fill-supply-rates-for-positions.xml', NOW(), 20, '8:d9ba0e20c5f8e70b5e9f42e886da2edf', 'insert tableName=positions_supply_rates; insert tableName=positions_supply_rates; insert tableName=positions_supply_rates; insert tableName=positions_supply_rates; insert tableName=positions_supply_rates; insert tableName=positions_supply_rates; i...', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/org-structure/init-fill-supply-rates-for-positions.xml::map-positions-and-supply-rates-engineers-unit::vivanchenko
INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (31, 4021);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (31, 4110);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (31, 4009);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (31, 4006);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (32, 4021);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (32, 4010);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (32, 4007);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (33, 4021);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (33, 4010);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (34, 4002);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (34, 4005);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (34, 4010);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (35, 4002);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (35, 4005);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (35, 4007);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (35, 4010);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (36, 4002);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (36, 4005);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (36, 4006);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (36, 4010);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (37, 4005);

INSERT INTO positions_supply_rates (position_id, sup_rate_id) VALUES (37, 4010);

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('map-positions-and-supply-rates-engineers-unit', 'vivanchenko', 'src/main/resources/liquibase/org-structure/init-fill-supply-rates-for-positions.xml', NOW(), 21, '8:c2ac3271266ad55c348379ac848238be', 'insert tableName=positions_supply_rates; insert tableName=positions_supply_rates; insert tableName=positions_supply_rates; insert tableName=positions_supply_rates; insert tableName=positions_supply_rates; insert tableName=positions_supply_rates; i...', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/equipment/init-fill-equipment-list.xml::fill-init-equipment-list::vivanchenko
INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3001SN1', 3001, 25023.2, '2000-02-12', 111);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3001SN2', 3001, 30500, '2019-05-12', 111);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date) VALUES ('3001SN3', 3001, 30500, '2017-04-02');

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3001SN4', 3001, 22000, '2016-05-11', 121);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3001SN10', 3001, 30500, '2015-02-12', 211);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date) VALUES ('3001SN11', 3001, 30500, '2012-06-27');

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, decommissioning_date, employee_id) VALUES ('3001SN12', 3001, 18000, '2000-02-12', '2020-02-12', 151);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3002SN1', 3002, 25000, '2014-02-12', 221);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date) VALUES ('3002SN2', 3002, 25000, '2013-09-26');

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3002SN3', 3002, 25000, '2018-12-03', 241);

INSERT INTO equipment (serial_number, type, purchase_price) VALUES ('3002SN4', 3002, 25000);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3002SN5', 3002, 25000, '2016-02-12', 141);

INSERT INTO equipment (serial_number, type, purchase_price) VALUES ('3002SN6', 3002, 25000);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3003SN1', 3003, 17000, '2015-02-12', 321);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3003SN2', 3003, 17000, '2011-02-12', 321);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date) VALUES ('3003SN3', 3003, 17000, '2011-02-12');

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3003SN4', 3003, 17000, '2008-02-12', 331);

INSERT INTO equipment (serial_number, type, purchase_price) VALUES ('3003SN5', 3003, 17000);

INSERT INTO equipment (serial_number, type, purchase_price) VALUES ('3003SN6', 3003, 17000);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3004SN1', 3004, 16000, '2008-05-11', 131);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3004SN2', 3004, 16000, '2019-02-12', 221);

INSERT INTO equipment (serial_number, type, purchase_price) VALUES ('3004SN3', 3004, 16000);

INSERT INTO equipment (serial_number, type, purchase_price) VALUES ('3004SN4', 3004, 16000);

INSERT INTO equipment (serial_number, type, purchase_price) VALUES ('3004SN5', 3004, 16000);

INSERT INTO equipment (serial_number, type, purchase_price) VALUES ('3004SN6', 3004, 16000);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3005SN1', 3005, 19220.50, '2015-02-12', 311);

INSERT INTO equipment (serial_number, type, purchase_price) VALUES ('3005SN2', 3005, 19220.50);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3005SN3', 3005, 19220.50, '2007-02-12', 121);

INSERT INTO equipment (serial_number, type, purchase_price) VALUES ('3005SN4', 3005, 19220.50);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3006SN1', 3006, 33500, '2013-09-12', 211);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3007SN1', 3007, 4505.50, '2009-11-12', 121);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, decommissioning_date) VALUES ('3007SN2', 3007, 4505.50, '2015-02-12', '2018-02-12');

INSERT INTO equipment (serial_number, type, purchase_price) VALUES ('3007SN3', 3007, 4505.50);

INSERT INTO equipment (serial_number, type, purchase_price) VALUES ('3007SN4', 3007, 4505.50);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3009SN1', 3009, 13650, '2020-11-12', 211);

INSERT INTO equipment (serial_number, type, purchase_price) VALUES ('3009SN2', 3009, 13650);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3010SN1', 3010, 1500, '2017-02-11', 131);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3010SN2', 3010, 1500, '2019-02-11', 111);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3010SN3', 3010, 1500, '2016-02-11', 121);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3010SN4', 3010, 1500, '2017-02-11', 311);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3010SN5', 3010, 1500, '2017-02-11', 321);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3010SN6', 3010, 1500, '2017-02-11', 141);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3010SN7', 3010, 1500, '2013-02-11', 221);

INSERT INTO equipment (serial_number, type, purchase_price, commissioning_date, employee_id) VALUES ('3010SN8', 3010, 1500, '2021-02-11', 331);

INSERT INTO equipment (serial_number, type, purchase_price) VALUES ('3010SN9', 3010, 1500);

INSERT INTO equipment (serial_number, type, purchase_price) VALUES ('3010SN10', 3010, 1500);

INSERT INTO equipment (serial_number, type, purchase_price) VALUES ('3010SN11', 3010, 1500);

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('fill-init-equipment-list', 'vivanchenko', 'src/main/resources/liquibase/equipment/init-fill-equipment-list.xml', NOW(), 22, '8:2aa36019f46692d44886ebf2d869e38e', 'insert tableName=equipment; insert tableName=equipment; insert tableName=equipment; insert tableName=equipment; insert tableName=equipment; insert tableName=equipment; insert tableName=equipment; insert tableName=equipment; insert tableName=equipm...', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/org-structure/add-login-to-employees.xml::add-login-column-to-employee-table::vivanchenko
ALTER TABLE employee ADD login TEXT;

ALTER TABLE employee ADD UNIQUE (login);

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('add-login-column-to-employee-table', 'vivanchenko', 'src/main/resources/liquibase/org-structure/add-login-to-employees.xml', NOW(), 23, '8:3fc84d29f6ebeadc4c05b2e0e9be334f', 'addColumn tableName=employee', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/org-structure/add-login-to-employees.xml::update-existing-employees-with-logins::vivanchenko
UPDATE employee SET login = 'oglavniy' WHERE id=111;

UPDATE employee SET login = 'skopeyka' WHERE id=121;

UPDATE employee SET login = 'istarshyna' WHERE id=131;

UPDATE employee SET login = 'vkadr' WHERE id=141;

UPDATE employee SET login = 'aadmin' WHERE id=151;

UPDATE employee SET login = 'pprodaznyi' WHERE id=211;

UPDATE employee SET login = 'orynochnaya' WHERE id=221;

UPDATE employee SET login = 'abaryha' WHERE id=241;

UPDATE employee SET login = 'yivanova' WHERE id=311;

UPDATE employee SET login = 'vfahivets' WHERE id=321;

UPDATE employee SET login = 'srazin' WHERE id=331;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('update-existing-employees-with-logins', 'vivanchenko', 'src/main/resources/liquibase/org-structure/add-login-to-employees.xml', NOW(), 24, '8:fc9ed4ac4617464961885b2a49d911e1', 'update tableName=employee; update tableName=employee; update tableName=employee; update tableName=employee; update tableName=employee; update tableName=employee; update tableName=employee; update tableName=employee; update tableName=employee; upda...', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Changeset src/main/resources/liquibase/changelog-master.xml::update-org-unit-names::vivanchenko
UPDATE org_unit SET title = 'Отдел продаж' WHERE id = 2;

UPDATE org_unit SET title = 'Инженерный отдел' WHERE id = 3;

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('update-org-unit-names', 'vivanchenko', 'src/main/resources/liquibase/changelog-master.xml', NOW(), 25, '8:c295c12e8acc88e79e33b3caab32a604', 'update tableName=org_unit; update tableName=org_unit', '', 'EXECUTED', NULL, NULL, '4.2.0', '5209367445');

-- Release Database Lock
UPDATE databasechangeloglock SET LOCKED = FALSE, LOCKEDBY = NULL, LOCKGRANTED = NULL WHERE ID = 1;

