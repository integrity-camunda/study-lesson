-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: src/main/resources/liquibase/changelog-camuda-db.xml
-- Ran at: 27.02.21 19:15
-- Against: camunda@jdbc:postgresql://washington.integrity.com.ua/CamundaDB
-- Liquibase version: 4.2.0
-- *********************************************************************

SET SEARCH_PATH TO public;

SET SEARCH_PATH TO public;

-- Create Database Lock Table
CREATE TABLE databasechangeloglock (ID INTEGER NOT NULL, LOCKED BOOLEAN NOT NULL, LOCKGRANTED TIMESTAMP WITHOUT TIME ZONE, LOCKEDBY VARCHAR(255), CONSTRAINT DATABASECHANGELOGLOCK_PKEY PRIMARY KEY (ID));

-- Initialize Database Lock Table
DELETE FROM databasechangeloglock;

INSERT INTO databasechangeloglock (ID, LOCKED) VALUES (1, FALSE);

-- Lock Database
UPDATE databasechangeloglock SET LOCKED = TRUE, LOCKEDBY = 'NB-38 (192.168.75.149)', LOCKGRANTED = '2021-02-27 19:15:57.958' WHERE ID = 1 AND LOCKED = FALSE;

SET SEARCH_PATH TO public;

-- Create Database Change Log Table
CREATE TABLE databasechangelog (ID VARCHAR(255) NOT NULL, AUTHOR VARCHAR(255) NOT NULL, FILENAME VARCHAR(255) NOT NULL, DATEEXECUTED TIMESTAMP WITHOUT TIME ZONE NOT NULL, ORDEREXECUTED INTEGER NOT NULL, EXECTYPE VARCHAR(10) NOT NULL, MD5SUM VARCHAR(35), DESCRIPTION VARCHAR(255), COMMENTS VARCHAR(255), TAG VARCHAR(255), LIQUIBASE VARCHAR(20), CONTEXTS VARCHAR(255), LABELS VARCHAR(255), DEPLOYMENT_ID VARCHAR(10));

SET SEARCH_PATH TO public;

SET SEARCH_PATH TO public;

-- Changeset src/main/resources/liquibase/changelog-camuda-db.xml::add-erp-users-to-camunda-db::vivanchenko
SET SEARCH_PATH TO public;

INSERT INTO act_id_user (id_, rev_, first_, last_, pwd_, salt_) VALUES ('oglavniy', 1, 'Олег', 'Главный', '{SHA-512}EWpkjQP9uEckYL6Rzsd260qzVxHu8u3ANttAnbCkFykTJimV1fvgSeyOMVg/CB6HLhonULpOyFgcS8WkZAfc7A==', 'L/c9JCR2N9e1YdhqeKyzkw==');

INSERT INTO act_id_user (id_, rev_, first_, last_, pwd_, salt_) VALUES ('skopeyka', 1, 'Светлана', 'Копейка', '{SHA-512}EWpkjQP9uEckYL6Rzsd260qzVxHu8u3ANttAnbCkFykTJimV1fvgSeyOMVg/CB6HLhonULpOyFgcS8WkZAfc7A==', 'L/c9JCR2N9e1YdhqeKyzkw==');

INSERT INTO act_id_user (id_, rev_, first_, last_, pwd_, salt_) VALUES ('istarshyna', 1, 'Иван', 'Старшина', '{SHA-512}EWpkjQP9uEckYL6Rzsd260qzVxHu8u3ANttAnbCkFykTJimV1fvgSeyOMVg/CB6HLhonULpOyFgcS8WkZAfc7A==', 'L/c9JCR2N9e1YdhqeKyzkw==');

INSERT INTO act_id_user (id_, rev_, first_, last_, pwd_, salt_) VALUES ('vkadr', 1, 'Виктория', 'Кадр', '{SHA-512}EWpkjQP9uEckYL6Rzsd260qzVxHu8u3ANttAnbCkFykTJimV1fvgSeyOMVg/CB6HLhonULpOyFgcS8WkZAfc7A==', 'L/c9JCR2N9e1YdhqeKyzkw==');

INSERT INTO act_id_user (id_, rev_, first_, last_, pwd_, salt_) VALUES ('aadmin', 1, 'Андрей', 'Ад', '{SHA-512}EWpkjQP9uEckYL6Rzsd260qzVxHu8u3ANttAnbCkFykTJimV1fvgSeyOMVg/CB6HLhonULpOyFgcS8WkZAfc7A==', 'L/c9JCR2N9e1YdhqeKyzkw==');

INSERT INTO act_id_user (id_, rev_, first_, last_, pwd_, salt_) VALUES ('pprodaznyi', 1, 'Петр', 'Продажный', '{SHA-512}EWpkjQP9uEckYL6Rzsd260qzVxHu8u3ANttAnbCkFykTJimV1fvgSeyOMVg/CB6HLhonULpOyFgcS8WkZAfc7A==', 'L/c9JCR2N9e1YdhqeKyzkw==');

INSERT INTO act_id_user (id_, rev_, first_, last_, pwd_, salt_) VALUES ('orynochnaya', 1, 'Ольга', 'Рыночная', '{SHA-512}EWpkjQP9uEckYL6Rzsd260qzVxHu8u3ANttAnbCkFykTJimV1fvgSeyOMVg/CB6HLhonULpOyFgcS8WkZAfc7A==', 'L/c9JCR2N9e1YdhqeKyzkw==');

INSERT INTO act_id_user (id_, rev_, first_, last_, pwd_, salt_) VALUES ('abaryha', 1, 'Антон', 'Барыга', '{SHA-512}EWpkjQP9uEckYL6Rzsd260qzVxHu8u3ANttAnbCkFykTJimV1fvgSeyOMVg/CB6HLhonULpOyFgcS8WkZAfc7A==', 'L/c9JCR2N9e1YdhqeKyzkw==');

INSERT INTO act_id_user (id_, rev_, first_, last_, pwd_, salt_) VALUES ('yivanova', 1, 'Юлия', 'Иванова', '{SHA-512}EWpkjQP9uEckYL6Rzsd260qzVxHu8u3ANttAnbCkFykTJimV1fvgSeyOMVg/CB6HLhonULpOyFgcS8WkZAfc7A==', 'L/c9JCR2N9e1YdhqeKyzkw==');

INSERT INTO act_id_user (id_, rev_, first_, last_, pwd_, salt_) VALUES ('vfahivets', 1, 'Виктор', 'Фахивец', '{SHA-512}EWpkjQP9uEckYL6Rzsd260qzVxHu8u3ANttAnbCkFykTJimV1fvgSeyOMVg/CB6HLhonULpOyFgcS8WkZAfc7A==', 'L/c9JCR2N9e1YdhqeKyzkw==');

INSERT INTO act_id_user (id_, rev_, first_, last_, pwd_, salt_) VALUES ('srazin', 1, 'Степан', 'Разин', '{SHA-512}EWpkjQP9uEckYL6Rzsd260qzVxHu8u3ANttAnbCkFykTJimV1fvgSeyOMVg/CB6HLhonULpOyFgcS8WkZAfc7A==', 'L/c9JCR2N9e1YdhqeKyzkw==');

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('add-erp-users-to-camunda-db', 'vivanchenko', 'src/main/resources/liquibase/changelog-camuda-db.xml', NOW(), 1, '8:909b083d377e633c0d46d03ef78e4cee', 'insert tableName=act_id_user; insert tableName=act_id_user; insert tableName=act_id_user; insert tableName=act_id_user; insert tableName=act_id_user; insert tableName=act_id_user; insert tableName=act_id_user; insert tableName=act_id_user; insert ...', '', 'EXECUTED', NULL, NULL, '4.2.0', '4446160186');

-- Changeset src/main/resources/liquibase/changelog-camuda-db.xml::add-erp-users-to-study-group::vivanchenko
SET SEARCH_PATH TO public;

INSERT INTO act_id_membership (user_id, group_id) VALUES ('oglavniy', 'studyusers');

INSERT INTO act_id_membership (user_id, group_id) VALUES ('skopeyka', 'studyusers');

INSERT INTO act_id_membership (user_id, group_id) VALUES ('istarshyna', 'studyusers');

INSERT INTO act_id_membership (user_id, group_id) VALUES ('vkadr', 'studyusers');

INSERT INTO act_id_membership (user_id, group_id) VALUES ('aadmin', 'studyusers');

INSERT INTO act_id_membership (user_id, group_id) VALUES ('pprodaznyi', 'studyusers');

INSERT INTO act_id_membership (user_id, group_id) VALUES ('orynochnaya', 'studyusers');

INSERT INTO act_id_membership (user_id, group_id) VALUES ('abaryha', 'studyusers');

INSERT INTO act_id_membership (user_id, group_id) VALUES ('yivanova', 'studyusers');

INSERT INTO act_id_membership (user_id, group_id) VALUES ('vfahivets', 'studyusers');

INSERT INTO act_id_membership (user_id, group_id) VALUES ('srazin', 'studyusers');

INSERT INTO databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, CONTEXTS, LABELS, LIQUIBASE, DEPLOYMENT_ID) VALUES ('add-erp-users-to-study-group', 'vivanchenko', 'src/main/resources/liquibase/changelog-camuda-db.xml', NOW(), 2, '8:359de051fb776ba4ab07b7d1495face4', 'insert tableName=act_id_membership; insert tableName=act_id_membership; insert tableName=act_id_membership; insert tableName=act_id_membership; insert tableName=act_id_membership; insert tableName=act_id_membership; insert tableName=act_id_members...', '', 'EXECUTED', NULL, NULL, '4.2.0', '4446160186');

-- Release Database Lock
SET SEARCH_PATH TO public;

UPDATE databasechangeloglock SET LOCKED = FALSE, LOCKEDBY = NULL, LOCKGRANTED = NULL WHERE ID = 1;

SET SEARCH_PATH TO public;

