package ua.com.integrity.bpm.camunda.study.service.impl;

import camundajar.impl.scala.Unit;
import com.google.common.collect.ImmutableList;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoSettings;
import ua.com.integrity.bpm.camunda.study.dao.EmployeeDao;
import ua.com.integrity.bpm.camunda.study.dao.GenericDao;
import ua.com.integrity.bpm.camunda.study.domain.Employee;
import ua.com.integrity.bpm.camunda.study.domain.OrgUnit;
import ua.com.integrity.bpm.camunda.study.domain.Position;
import ua.com.integrity.bpm.camunda.study.domain.equipment.EquipmentType;
import ua.com.integrity.bpm.camunda.study.domain.equipment.SupplyRate;
import ua.com.integrity.bpm.camunda.study.domain.vacation.Vacation;
import ua.com.integrity.bpm.camunda.study.dto.EmployeeDto;
import ua.com.integrity.bpm.camunda.study.dto.OrgUnitDto;
import ua.com.integrity.bpm.camunda.study.dto.PositionDto;
import ua.com.integrity.bpm.camunda.study.dto.vacation.VacationDto;
import ua.com.integrity.bpm.camunda.study.dto.vacation.VacationType;
import ua.com.integrity.bpm.camunda.study.mapper.ErpMapper;

import javax.inject.Inject;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@MockitoSettings
class OrgStructureServiceImplTest {

    @Mock
    private GenericDao<OrgUnit, Long> orgUnitDao;
    @Mock
    private GenericDao<Position, Long> positionDao;
    @Mock
    private EmployeeDao employeeDao;
    @Mock
    private GenericDao<Vacation, Long> vacationDao;
    @Mock
    private GenericDao<SupplyRate, Long> supplyRateDao;

    private ErpMapper mapper = Mappers.getMapper(ErpMapper.class);

    @InjectMocks
    OrgStructureServiceImpl orgStructureService;

    @BeforeEach
    void setUp() {
        orgStructureService.setMapper(mapper);
    }

    @Test
    void test_getUnit() {
        when(orgUnitDao.find(anyLong())).thenReturn(Optional.of(buildUnitExample()));
        Optional<OrgUnitDto> unit = orgStructureService.getUnit(2L);
        checkThatUnitCorrect(unit.get());
    }

    private static void checkThatUnitCorrect(OrgUnitDto unit) {
        assertThat(unit)
                .hasFieldOrPropertyWithValue("id", 2L)
                .hasFieldOrPropertyWithValue("title", "testTitle")
                .hasFieldOrPropertyWithValue("ascendant", 1L);
        assertThat(unit.getDescendants())
                .containsOnly(10L, 20L);
        assertThat(unit.getPositions())
                .containsOnly(115L, 116L);
    }

    private static OrgUnit buildUnitExample() {
        OrgUnit unit1 = OrgUnit.builder().id(1L).title("testAsc").build();
        OrgUnit unit10 = OrgUnit.builder().id(10L).title("test unit 10").build();
        OrgUnit unit20 = OrgUnit.builder().id(20L).title("test unit 10").build();
        Position pos1 = Position.builder().id(115L).title("testPosition").build();
        Position pos2 = Position.builder().id(116L).title("testPosition").build();
        return OrgUnit.builder().id(2L).title("testTitle").ascendant(unit1)
                .descendants(ImmutableList.of(unit10, unit20))
                .positions(ImmutableList.of(pos1, pos2))
                .build();
    }

    private static SupplyRate buildSupplyRateExample(long id) {
        return SupplyRate.builder()
                .id(id)
                .title("testSupplyRate")
                .amount(1)
                .equipmentType(buildEquipmentTypeExample(50L))
                .build();
    }

    private static EquipmentType buildEquipmentTypeExample(long id) {
        return EquipmentType.builder().id(id).build();
    }


    @Test
    void test_getUnit_empty_optional() {
        when(orgUnitDao.find(anyLong())).thenReturn(Optional.empty());
        Optional<OrgUnitDto> unit = orgStructureService.getUnit(2L);
        assertThat(unit).isEmpty();
    }

    @Test
    void test_getUnits() {
        when(orgUnitDao.findAll()).thenReturn(ImmutableList.of(buildUnitExample()));
        List<OrgUnitDto> units = orgStructureService.getUnits();
        assertThat(units).hasSize(1);
        for (OrgUnitDto unit : units) {
            checkThatUnitCorrect(unit);
        }
    }

    @Captor
    ArgumentCaptor<OrgUnit> orgUnitCaptor;
    @Test
    void test_createUnit() {
        when(orgUnitDao.create(any(OrgUnit.class))).thenReturn(OrgUnit.builder().id(25L).build());
        OrgUnitDto unit = orgStructureService.createUnit(buildOrgUnitDtoExample());
        assertThat(unit.getId()).isEqualTo(25L);
        verify(orgUnitDao).create(orgUnitCaptor.capture());
        checkUnitpassedCorrect(orgUnitCaptor.getValue());
    }

    private static void checkUnitpassedCorrect(OrgUnit unit) {
        assertThat(unit)
                .hasFieldOrPropertyWithValue("title", "testTitle")
                .hasFieldOrPropertyWithValue("ascendant.id", 2L);
        assertThat(unit.getDescendants())
                .extracting(OrgUnit::getId)
                .containsOnly(1L, 2L);
        assertThat(unit.getPositions())
                .extracting(Position::getId)
                .containsOnly(10L, 20L);
    }

    private static OrgUnitDto buildOrgUnitDtoExample() {
        return OrgUnitDto.builder().title("testTitle")
                .ascendant(2L)
                .descendants(ImmutableList.of(1L,2L))
                .positions(ImmutableList.of(10L, 20L))
                .build();
    }

    private static Position buildPositionExample(long positionId) {
        SupplyRate rate1 = buildSupplyRateExample(65L);
        SupplyRate rate2 = buildSupplyRateExample(66L);
        return Position.builder()
                .id(positionId)
                .title("testPosition")
                .index(1)
                .employee(buildEmployeeExample(1000L))
                .orgUnit(buildUnitExample())
                .supplyRates(new ArrayList<>(ImmutableList.of(rate1, rate2)))
                .build();
    }

    @Test
    void test_getPosition() {
        when(positionDao.find(2L)).thenReturn(Optional.of(buildPositionExample(2L)));
        PositionDto position = orgStructureService.getPosition(2L).get();
        assertThat(position)
                .hasFieldOrPropertyWithValue("id", 2L)
                .hasFieldOrPropertyWithValue("title", "testPosition")
                .hasFieldOrPropertyWithValue("employee", 1000L)
                .hasFieldOrPropertyWithValue("orgUnit", 2L);
        assertThat(position.getSupplyRates())
                .containsOnly(65L, 66L);
    }

    @Test
    void test_getPosition_empty() {
        when(positionDao.find(2L)).thenReturn(Optional.empty());
        Optional<PositionDto> opt = orgStructureService.getPosition(2L);
        assertThat(opt).isEmpty();
    }

    @Test
    void test_getPositions() {
        when(positionDao.findAll()).thenReturn(ImmutableList.of(buildPositionExample(2L)));
        List<PositionDto> positions = orgStructureService.getPositions();
        assertThat(positions).hasSize(1);
        for (PositionDto position : positions) {
            assertThat(position)
                    .hasFieldOrPropertyWithValue("id", 2L)
                    .hasFieldOrPropertyWithValue("title", "testPosition")
                    .hasFieldOrPropertyWithValue("employee", 1000L)
                    .hasFieldOrPropertyWithValue("orgUnit", 2L);
            assertThat(position.getSupplyRates())
                    .containsOnly(65L, 66L);
        }
    }

    @Captor
    ArgumentCaptor<Position> positionArgumentCaptor;

    @Test
    void test_createPosition() {
        when(positionDao.create(any(Position.class))).thenReturn(Position.builder().id(125l).build());
        PositionDto positionDto = orgStructureService.createPosition(buildPositionDtoExample());
        assertThat(positionDto.getId()).isEqualTo(125l);
        verify(positionDao).create(positionArgumentCaptor.capture());
        Position position = positionArgumentCaptor.getValue();
        assertThat(position)
                .hasFieldOrPropertyWithValue("title", "testPosition")
                .hasFieldOrPropertyWithValue("employee.id", 1000L)
                .hasFieldOrPropertyWithValue("orgUnit.id", 2L);
        assertThat(position.getSupplyRates())
                .extracting(SupplyRate::getId)
                .containsOnly(65L, 66L);
    }

    private static PositionDto buildPositionDtoExample() {
        return PositionDto.builder()
                .title("testPosition")
                .employee(1000L)
                .orgUnit(2L)
                .index(1)
                .supplyRates(ImmutableList.of(65L, 66L))
                .build();
    }

    @Test
    void test_addSupplyRate() {
        Position position = buildPositionExample(2L);
        when(positionDao.find(2L)).thenReturn(Optional.of(position));
        when(supplyRateDao.find(50L)).thenReturn(Optional.of(buildSupplyRateExample(50L)));
        PositionDto positionDto = orgStructureService.addSupplyRate(2L, 50L);
        assertThat(positionDto.getSupplyRates())
                .contains(50L);
    }

    @Test
    void test_addSupplyRate_throw_IllegalArgumentException_when_position_not_found() {
        when(positionDao.find(2L)).thenReturn(Optional.empty());
        assertThatThrownBy(()-> orgStructureService.addSupplyRate(2L,50L))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("No such position");

    }

    @Test
    void test_addSupplyRate_throw_IllegalArgumentException_when_supply_rate_not_found() {
        Position position = buildPositionExample(2L);
        when(positionDao.find(2L)).thenReturn(Optional.of(position));
        when(supplyRateDao.find(50L)).thenReturn(Optional.empty());
        assertThatThrownBy(()-> orgStructureService.addSupplyRate(2L,50L))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("No such supply rate");

    }

    @Test
    void test_addSupplyRate_throw_IllegalArgumentException_when_supply_rate_already_linked() {
        Position position = buildPositionExample(2L);
        when(positionDao.find(2L)).thenReturn(Optional.of(position));
        assertThatThrownBy(()-> orgStructureService.addSupplyRate(2L,65L))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Supply rate already linked to position.");

    }

    @Test
    void test_removeSupplyRate() {
        Position position = buildPositionExample(2L);
        when(positionDao.find(2L)).thenReturn(Optional.of(position));
        orgStructureService.removeSupplyRate(2L, 65L);
        assertThat(position.getSupplyRates())
                .extracting(SupplyRate::getId)
                .doesNotContain(65L);
    }

    @Test
    void test_removeSupplyRate_throw_IllegalArgumentException_when_position_not_found() {
        when(positionDao.find(2L)).thenReturn(Optional.empty());
        assertThatThrownBy(()-> orgStructureService.removeSupplyRate(2L,50L))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("No such position");
    }

    private static Employee buildEmployeeExample(long id) {
        return Employee.builder()
                .id(id)
                .firstName("JOHN")
                .lastName("DOE")
                .login("jdoe")
                .position(Position.builder().id(2L).build())
                .birthDate(LocalDate.parse("2000-01-01"))
                .hireDate(LocalDate.parse("2020-01-01"))
                .fireDate(LocalDate.parse("2030-01-01"))
                .build();
    }

    @Test
    void test_getEmployee() {
        when(employeeDao.find(2L)).thenReturn(Optional.of(buildEmployeeExample(2L)));
        EmployeeDto employee = orgStructureService.getEmployee(2L).get();
        assertThat(employee)
                .hasFieldOrPropertyWithValue("firstName", "JOHN")
                .hasFieldOrPropertyWithValue("lastName", "DOE")
                .hasFieldOrPropertyWithValue("login", "jdoe")
                .hasFieldOrPropertyWithValue("position", 2l);
        assertThat(employee.getBirthDate()).isEqualTo("2000-01-01");
        assertThat(employee.getHireDate()).isEqualTo("2020-01-01");
        assertThat(employee.getFireDate()).isEqualTo("2030-01-01");
    }

    @Test
    void test_getEmployee_empty() {
        when(employeeDao.find(2L)).thenReturn(Optional.empty());
        Optional<EmployeeDto> employee = orgStructureService.getEmployee(2L);
        assertThat(employee).isEmpty();
    }

    @Test
    void testGetEmployee_by_login() {
        when(employeeDao.findByLogin("jdoe")).thenReturn(Optional.of(buildEmployeeExample(2L)));
        EmployeeDto employee = orgStructureService.getEmployee("jdoe").get();
        assertThat(employee)
                .hasFieldOrPropertyWithValue("firstName", "JOHN")
                .hasFieldOrPropertyWithValue("lastName", "DOE")
                .hasFieldOrPropertyWithValue("login", "jdoe")
                .hasFieldOrPropertyWithValue("position", 2l);
        assertThat(employee.getBirthDate()).isEqualTo("2000-01-01");
        assertThat(employee.getHireDate()).isEqualTo("2020-01-01");
        assertThat(employee.getFireDate()).isEqualTo("2030-01-01");
    }

    @Test
    void test_getEmployee_by_login_empty() {
        when(employeeDao.findByLogin("jdoe")).thenReturn(Optional.empty());
        Optional<EmployeeDto> employee = orgStructureService.getEmployee("jdoe");
        assertThat(employee).isEmpty();
    }

    @Test
    void test_getEmployees() {
        when(employeeDao.findAll()).thenReturn(ImmutableList.of(buildEmployeeExample(2L)));
        List<EmployeeDto> employees = orgStructureService.getEmployees();
        assertThat(employees).hasSize(1);
        for (EmployeeDto employee : employees) {
            assertThat(employee)
                    .hasFieldOrPropertyWithValue("firstName", "JOHN")
                    .hasFieldOrPropertyWithValue("lastName", "DOE")
                    .hasFieldOrPropertyWithValue("login", "jdoe")
                    .hasFieldOrPropertyWithValue("position", 2l);
            assertThat(employee.getBirthDate()).isEqualTo("2000-01-01");
            assertThat(employee.getHireDate()).isEqualTo("2020-01-01");
            assertThat(employee.getFireDate()).isEqualTo("2030-01-01");
        }
    }

    @Captor
    ArgumentCaptor<Employee> employeeArgumentCaptor;

    @Test
    void test_newEmployee() {
        when(positionDao.find(anyLong())).thenReturn(Optional.of(Position.builder().id(30L).build()));
        when(employeeDao.findByLogin(anyString())).thenReturn(Optional.empty());
        when(employeeDao.create(any(Employee.class))).thenReturn(Employee.builder().id(25l).build());

        EmployeeDto newEmployee = orgStructureService.newEmployee(buildEmployeeDtoExample(2l));
        assertThat(newEmployee.getId()).isEqualTo(25L);

        verify(employeeDao).create(employeeArgumentCaptor.capture());
        Employee capturedValue = employeeArgumentCaptor.getValue();
        assertThat(capturedValue)
                .hasFieldOrPropertyWithValue("firstName", "JOHN")
                .hasFieldOrPropertyWithValue("lastName", "DOE")
                .hasFieldOrPropertyWithValue("login", "jdoe")
                .hasFieldOrPropertyWithValue("position.id", 30l);
        assertThat(capturedValue.getBirthDate()).isEqualTo("2000-01-01");
        assertThat(capturedValue.getHireDate()).isEqualTo("2020-01-01");
        assertThat(capturedValue.getFireDate()).isEqualTo("2030-01-01");

    }

    private EmployeeDto buildEmployeeDtoExample(long id) {
        return EmployeeDto.builder()
                .id(id)
                .id(id)
                .firstName("JOHN")
                .lastName("DOE")
                .login("jdoe")
                .position(30l)
                .birthDate(LocalDate.parse("2000-01-01"))
                .hireDate(LocalDate.parse("2020-01-01"))
                .fireDate(LocalDate.parse("2030-01-01"))
                .build();
    }

    @Test
    void test_newEmployee_throws_IllegalArgumentException_when_no_positions_found() {
        when(positionDao.find(anyLong())).thenReturn(Optional.empty());
        assertThatThrownBy(()-> orgStructureService.newEmployee(buildEmployeeDtoExample(2l)))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Cannot create new employee with unknown position");

    }

    @Test
    void test_newEmployee_throws_IllegalArgumentException_when_position_is_busy() {
        when(positionDao.find(anyLong())).thenReturn(Optional.of(Position.builder().id(30L).employee(Employee.builder().id(60l).build()).build()));
        assertThatThrownBy(()-> orgStructureService.newEmployee(buildEmployeeDtoExample(2l)))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Position is already linked to employee.");

    }

    @Test
    void test_newEmployee_throws_IllegalArgumentException_when_login_is_busy() {
        when(positionDao.find(anyLong())).thenReturn(Optional.of(Position.builder().id(30L).build()));
        when(employeeDao.findByLogin(anyString())).thenReturn(Optional.of(Employee.builder().id(60l).build()));
        assertThatThrownBy(()-> orgStructureService.newEmployee(buildEmployeeDtoExample(2l)))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Employee with same login exists");

    }

    @Test
    void test_updateEmployee() {
        when(employeeDao.find(2L)).thenReturn(Optional.of(buildEmployeeExample(2L)));
        when(employeeDao.findByLogin(anyString())).thenReturn(Optional.of(buildEmployeeExample(2L)));
        orgStructureService.updateEmployee(buildEmployeeDtoExample(2l));
        verify(employeeDao).update(any(Employee.class));
    }

    @Test
    void test_updateEmployee_findByLoginreturn_empty() {
        when(employeeDao.find(2L)).thenReturn(Optional.of(buildEmployeeExample(2L)));
        when(employeeDao.findByLogin(anyString())).thenReturn(Optional.empty());
        orgStructureService.updateEmployee(buildEmployeeDtoExample(2l));
        verify(employeeDao).update(any(Employee.class));
    }

    @Test
    void test_updateEmployee_throws_IllegalArgumentException_when_new_login_is_busy() {
        when(employeeDao.find(2L)).thenReturn(Optional.of(buildEmployeeExample(2L)));
        when(employeeDao.findByLogin(anyString())).thenReturn(Optional.of(Employee.builder().id(45l).build()));
        assertThatThrownBy(()-> orgStructureService.updateEmployee(buildEmployeeDtoExample(2l)))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("New login for employee already used by another employee");
    }

    @Test
    void test_updateEmployee_throws_IllegalArgumentException_when_employee_with_such_id_not_exist() {
        when(employeeDao.find(2L)).thenReturn(Optional.empty());
        assertThatThrownBy(()-> orgStructureService.updateEmployee(buildEmployeeDtoExample(2l)))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Employee not exist.");
    }

    @Test
    void test_getVacations() {
        when(vacationDao.findAll()).thenReturn(ImmutableList.of(buildVacationExample(10L)));
        List<VacationDto> vacations = orgStructureService.getVacations();
        assertThat(vacations).hasSize(1);
        for (VacationDto vacation : vacations) {
            assertThat(vacation)
                    .hasFieldOrPropertyWithValue("id", 10l)
                    .hasFieldOrPropertyWithValue("duration", 5)
                    .hasFieldOrPropertyWithValue("employee", 2l)
                    .hasFieldOrPropertyWithValue("type", VacationType.ANNUALLY);
            assertThat(vacation.getStartDate()).isEqualTo("2020-01-01");
        }
    }

    private Vacation buildVacationExample(long id) {
        return Vacation.builder()
                .id(id)
                .duration(5)
                .employee(buildEmployeeExample(2L))
                .startDate(LocalDate.parse("2020-01-01"))
                .type(VacationType.ANNUALLY)
                .build();
    }

    @Captor
    ArgumentCaptor<Vacation> vacationArgumentCaptor;

    @Test
    void test_newVacation() {
        when(employeeDao.find(2l)).thenReturn(Optional.of(buildEmployeeExample(2l)));
        when(vacationDao.create(any(Vacation.class))).thenReturn(Vacation.builder().id(15l).build());
        VacationDto vacationDto = orgStructureService.newVacation(VacationDto.builder().employee(2l).duration(3).type(VacationType.AT_OWN_EXPENSE).startDate(LocalDate.parse("2021-02-02")).build());
        assertThat(vacationDto.getId()).isEqualTo(15l);
        verify(vacationDao).create(vacationArgumentCaptor.capture());
        Vacation captured = vacationArgumentCaptor.getValue();
        assertThat(captured)
                .hasFieldOrPropertyWithValue("duration",3)
                .hasFieldOrPropertyWithValue("employee.id", 2l)
                .hasFieldOrPropertyWithValue("type", VacationType.AT_OWN_EXPENSE);
        assertThat(captured.getStartDate()).isEqualTo("2021-02-02");
    }

    @Test
    void test_newVacation_throw_IllegalArgumentException_when_no_employee_find() {
        when(employeeDao.find(2l)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> orgStructureService.newVacation(VacationDto.builder().employee(2l).duration(3).type(VacationType.AT_OWN_EXPENSE).startDate(LocalDate.parse("2021-02-02")).build()))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Cannot create vacation for not existing employee");
    }

    @Test
    void test_cancelVacation() {
        when(vacationDao.find(4l)).thenReturn(Optional.of(Vacation.builder().id(4l).build()));
        orgStructureService.cancelVacation(4l);
        verify(vacationDao).delete(eq(4l));
    }

    @Test
    void test_cancelVacation_unexist_vacation_id() {
        when(vacationDao.find(4l)).thenReturn(Optional.empty());
        orgStructureService.cancelVacation(4l);
        verify(vacationDao).find(eq(4l));
        verifyNoMoreInteractions(vacationDao);
    }
}