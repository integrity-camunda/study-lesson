package ua.com.integrity.bpm.camunda.study.api.rest;

import io.restassured.RestAssured;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.importer.ZipImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import ua.com.integrity.bpm.camunda.study.dto.api.EquipmentAssigningRequest;
import ua.com.integrity.bpm.camunda.study.dto.api.EquipmentCommissioningRequest;
import ua.com.integrity.bpm.camunda.study.dto.equipment.EquipmentDto;
import ua.com.integrity.bpm.camunda.study.dto.equipment.EquipmentTypeDto;
import ua.com.integrity.bpm.camunda.study.dto.equipment.MaintenanceDto;
import ua.com.integrity.bpm.camunda.study.dto.equipment.SupplyRateDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.ConsumptionRateDto;

import javax.ws.rs.core.MediaType;
import java.io.File;
import java.math.BigDecimal;
import java.net.URL;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.hamcrest.Matchers.*;

@RunWith(Arquillian.class)
public class EquipmentRestIT {

    @Deployment(testable = false)
    public static Archive<?> getEarArchive() {
        // Import the web archive that was created by Maven:
        File f = new File("./target/erp.war");
        if (f.exists() == false) {
            throw new RuntimeException("File " + f.getAbsolutePath() + " does not exist.");
        }
        WebArchive war = ShrinkWrap.create(ZipImporter.class, "org-structure-rest-erp-test.war").importFrom(f).as(WebArchive.class);
        war.addPackage("ua.com.integrity.bpm.camunda.study.api.rest");
        return war;
    }

    @ArquillianResource
    URL applicationUrl;

    String basePath;

    @Before
    public void setUp() throws Exception {
        basePath = applicationUrl.toString().concat("api/rest/equipment/");
    }

    @Test
    public void test_getEquipment() {
        RestAssured.when()
                .get(basePath + "3001SN1")
                .then()
                .statusCode(200)
                .body("serialNumber", is("3001SN1"))
                .body("commissioningDate", is("2000-02-12"))
                .body("user", is(111));
    }

    @Test
    public void test_getEquipment_unknown_id() {
        RestAssured.when()
                .get(basePath + "unknown")
                .then()
                .statusCode(404);
    }

    @Test
    public void test_getEquipmentByType_all() {
        List<String> sNums = RestAssured
                .with()
                .queryParam("type", 3001)
                .queryParam("withDecommissioned", true)
                .when()
                .get(basePath)
                .then()
                .statusCode(200)
                .extract().jsonPath().getList("serialNumber", String.class);
        assertThat(sNums).containsOnly(
                "3001SN1", "3001SN2",
                "3001SN3", "3001SN4", "3001SN10",
                "3001SN11", "3001SN12"
        );
    }

    @Test
    public void test_getEquipmentByType_non_decommissioned() {
        List<String> sNums = RestAssured
                .with()
                .queryParam("type", 3001)
                .when()
                .get(basePath)
                .then()
                .statusCode(200)
                .extract().jsonPath().getList("serialNumber", String.class);
        assertThat(sNums).containsOnly(
                "3001SN1", "3001SN2",
                "3001SN3", "3001SN4", "3001SN10",
                "3001SN11"
        );
    }

    @Test
    public void test_newEquipment() {
        RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(EquipmentDto.builder().serialNumber("testsn1")
                    .type(3004L).purchasePrice(new BigDecimal("28000"))
                        .build()
                )
                .when()
                .post(basePath)
                .then()
                .statusCode(200);
    }

    @Test
    public void test_newEquipment_invalid_body() {
        List errors = RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(EquipmentDto.builder().serialNumber("testsn1")
                        .type(3004L)
                        .build()
                )
                .when()
                .post(basePath)
                .then()
                .statusCode(400)
                .extract().as(List.class);
        assertThat(errors).hasSize(1);
    }

    @Test
    public void test_newEquipment_unknown_type() {
        RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(EquipmentDto.builder().serialNumber("testsn1")
                        .type(30013234L).purchasePrice(new BigDecimal("28000"))
                        .build()
                )
                .when()
                .post(basePath)
                .then()
                .statusCode(400)
                .body("message", is("Cannot create new equipment of unknown type"));
    }

    @Test
    public void test_commissionEquipment() {
        RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(new EquipmentCommissioningRequest("3004SN5", "2020-02-02"))
                .when()
                .patch(basePath + "3004SN5/commission")
                .then()
                .statusCode(200)
                .body("commissioningDate", is("2020-02-02"));
    }

    @Test
    public void test_commissionEquipment_body_mismatch() {
        RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(new EquipmentCommissioningRequest("3004SN5adad", "2020-02-02"))
                .when()
                .patch(basePath + "3004SN5/commission")
                .then()
                .statusCode(400)
                .body("message", is("Equipment s/n not match with request body"));
    }

    @Test
    public void test_commissionEquipment_date_format_mismatch() {
        RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(new EquipmentCommissioningRequest("3004SN5", "2020-02-42"))
                .when()
                .patch(basePath + "3004SN5/commission")
                .then()
                .statusCode(400)
                .body("message", is("Invalid commissioning date format"));
    }

    @Test
    public void test_commissionEquipment_invalid_body() {
        List errors = RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(new EquipmentCommissioningRequest())
                .when()
                .patch(basePath + "3004SN5/commission")
                .then()
                .statusCode(400)
                .extract().as(List.class);
        assertThat(errors).hasSize(2);
    }

    @Test
    public void test_decommissionEquipment() {
        RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(new EquipmentCommissioningRequest("3004SN5", "2020-02-02"))
                .when()
                .patch(basePath + "3004SN5/decommission")
                .then()
                .statusCode(200)
                .body("decommissioningDate", is("2020-02-02"));
    }

    @Test
    public void test_assignEquipment() {
        RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(new EquipmentAssigningRequest("3004SN5", 121L))
                .when()
                .patch(basePath + "3004SN5/assign")
                .then()
                .statusCode(200)
                .body("user", is(121));
    }

    @Test
    public void retrieveEquipment() {
        RestAssured
                .when()
                .patch(basePath + "3005SN1/retrieve")
                .then()
                .statusCode(200)
                .body("user", nullValue());
    }

    @Test
    public void test_getEquipmentType() {
        RestAssured
                .when()
                .get(basePath + "type/3001")
                .then()
                .statusCode(200)
                .body("title", is("Ноутбук НР"));
    }

    @Test
    public void test_getAllEquipmentType() {
        List<Long> types = RestAssured
                .when()
                .get(basePath + "type")
                .then()
                .statusCode(200)
                .extract().jsonPath().getList("id", Long.class);
        assertThat(types).contains(3001L, 3002L, 3003L, 3004L);
    }

    @Test
    public void test_newEquipmentType() {
        long id = RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(EquipmentTypeDto.builder().title("TEST type").lifeTimeInMonth(10).build())
                .when()
                .post(basePath + "type")
                .then()
                .statusCode(200)
                .extract().jsonPath().getLong("id");
        assertThat(id).isGreaterThanOrEqualTo(33000L);
    }

    @Test
    public void test_addMaintenance() {
        List<Long> ids = RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(MaintenanceDto.builder().id(2003L).build())
                .when()
                .put(basePath + "type/3001/maintenance")
                .then()
                .statusCode(200)
                .extract().jsonPath().getList("maintenanceSet", Long.class);
    }

    @Test
    public void test_removeMaintenance() {
        RestAssured
                .when()
                .delete(basePath + "type/3001/maintenance/2003")
                .then()
                .statusCode(200);
    }

    @Test
    public void test_addConsumptionRate() {
        List<Long> ids = RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(ConsumptionRateDto.builder().id(1056L).build())
                .when()
                .put(basePath + "type/3001/consumption-rate")
                .then()
                .statusCode(200)
                .extract().jsonPath().getList("consumptionRates", Long.class);
    }

    @Test
    public void test_removeConsumptionRate() {
        RestAssured
                .when()
                .delete(basePath + "type/3001/consumption-rate/1056")
                .then()
                .statusCode(200);
    }

    @Test
    public void test_getMaintenance() {
        RestAssured
                .when()
                .get(basePath + "maintenance/2001")
                .then()
                .statusCode(200)
                .body("title", is("Ежедневное ТО принтера"));
    }

    @Test
    public void test_getAllMaintenances() {
        List result = RestAssured
                .when()
                .get(basePath + "maintenance")
                .then()
                .statusCode(200)
                .extract().as(List.class);
        assertThat(result).hasSizeGreaterThanOrEqualTo(9);
    }

    @Test
    public void test_newMaintenance() {
        long id = RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(MaintenanceDto.builder().title("New type").build())
                .when()
                .post(basePath + "maintenance")
                .then()
                .statusCode(200)
                .extract().jsonPath().getLong("id");
        assertThat(id).isGreaterThanOrEqualTo(116000L);
    }

    @Test
    public void test_addConsumptionRateToMaintenance() {
        RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(ConsumptionRateDto.builder().id(1062L).build())
                .when()
                .put(basePath + "maintenance/2001/consumption-rate")
                .then()
                .statusCode(200);
    }

    @Test
    public void test_removeConsumptionRateFromMaintenance() {
        RestAssured
                .when()
                .delete(basePath + "maintenance/2001/consumption-rate/1062")
                .then()
                .statusCode(200);
    }

    @Test
    public void test_findSupplyRate() {
        RestAssured
                .when()
                .get(basePath + "supply-rate/4001")
                .then()
                .statusCode(200)
                .body("title", is("1 ноутбук НР"));
    }

    @Test
    public void test_getAllSupplyRates() {
        List result = RestAssured
                .when()
                .get(basePath + "supply-rate")
                .then()
                .statusCode(200)
                .extract().as(List.class);
        assertThat(result).hasSizeGreaterThanOrEqualTo(13);
    }

    @Test
    public void test_newSupplyRate() {
        long id = RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(SupplyRateDto.builder().amount(1).equipmentType(3001L).title("New test sup rate").build())
                .when()
                .post(basePath + "supply-rate")
                .then()
                .statusCode(200)
                .extract().jsonPath().getLong("id");
        assertThat(id).isGreaterThanOrEqualTo(22000L);
    }

    @Test
    public void updateSupplyRate() {
        RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(SupplyRateDto.builder().id(4007L).amount(1).equipmentType(3001L).title("Updated sup rate").build())
                .when()
                .put(basePath + "supply-rate/4007")
                .then()
                .statusCode(200);
    }
}