package ua.com.integrity.bpm.camunda.study.service.impl;

import com.google.common.collect.ImmutableList;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoSettings;
import ua.com.integrity.bpm.camunda.study.dao.GenericDao;
import ua.com.integrity.bpm.camunda.study.dao.material.BatchOfMaterialsDao;
import ua.com.integrity.bpm.camunda.study.dao.material.ConsumptionRateDao;
import ua.com.integrity.bpm.camunda.study.domain.materail.BatchOfMaterial;
import ua.com.integrity.bpm.camunda.study.domain.materail.ConsumptionRate;
import ua.com.integrity.bpm.camunda.study.domain.materail.Material;
import ua.com.integrity.bpm.camunda.study.dto.materail.BatchOfMaterialDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.ConsumptionRateDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.MaterialDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.MeasureUnit;
import ua.com.integrity.bpm.camunda.study.mapper.ErpMapper;

import javax.inject.Inject;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@MockitoSettings
class MaterialsServiceImplTest {

    private ErpMapper mapper = Mappers.getMapper(ErpMapper.class);
    @Mock
    private GenericDao<Material, Long> materialDao;
    @Mock
    private BatchOfMaterialsDao batchesDao;
    @Mock
    private ConsumptionRateDao consumptionRateDao;

    @InjectMocks
    MaterialsServiceImpl materialsService;

    @BeforeEach
    void setup() {
        materialsService.setMapper(mapper);
    }

    @Captor
    ArgumentCaptor<Material> materialArgumentCaptor;
    @Test
    void test_addMaterial() {
        when(materialDao.create(any(Material.class))).thenReturn(Material.builder().id(1l).build());
        MaterialDto newMaterial = materialsService.addMaterial(MaterialDto.builder().measureUnit(MeasureUnit.PCS).title("testTitle").build());
        assertThat(newMaterial.getId()).isEqualTo(1l);
        verify(materialDao).create(materialArgumentCaptor.capture());
        assertThat(materialArgumentCaptor.getValue())
                .hasFieldOrPropertyWithValue("title", "testTitle")
                .hasFieldOrPropertyWithValue("measureUnit", MeasureUnit.PCS);
    }

    @Test
    void test_allMaterials() {
        when(materialDao.findAll()).thenReturn(ImmutableList.of(Material.builder().id(2l).measureUnit(MeasureUnit.PCS).title("testTitle").build()));
        List<MaterialDto> allMaterials = materialsService.allMaterials();
        assertThat(allMaterials).hasSize(1);
        for (MaterialDto material : allMaterials) {
            assertThat(material)
                    .hasFieldOrPropertyWithValue("id", 2l)
                    .hasFieldOrPropertyWithValue("title", "testTitle")
                    .hasFieldOrPropertyWithValue("measureUnit", MeasureUnit.PCS);
        }

    }

    @Captor
    ArgumentCaptor<BatchOfMaterial> batchOfMaterialArgumentCaptor;

    @Test
    void test_addNewBatch() {
        when(materialDao.find(2l)).thenReturn(Optional.of(Material.builder().id(2l).build()));
        when(batchesDao.create(any(BatchOfMaterial.class))).thenReturn(BatchOfMaterial.builder().id(15l).build());
        BatchOfMaterialDto newBatchDto = BatchOfMaterialDto.builder()
                .material(MaterialDto.builder().id(2l).build())
                .purchaseDate(LocalDate.parse("2020-01-01"))
                .price(BigDecimal.valueOf(1000))
                .purchaseAmount(BigDecimal.valueOf(100))
                .build();
        BatchOfMaterialDto createdBatch = materialsService.addNewBatch(newBatchDto);
        assertThat(createdBatch.getId()).isEqualTo(15l);

        verify(batchesDao).create(batchOfMaterialArgumentCaptor.capture());
        assertThat(batchOfMaterialArgumentCaptor.getValue())
                .hasFieldOrPropertyWithValue("material.id", 2l)
                .hasFieldOrPropertyWithValue("purchaseDate", LocalDate.parse("2020-01-01"))
                .hasFieldOrPropertyWithValue("price", BigDecimal.valueOf(1000))
                .hasFieldOrPropertyWithValue("purchaseAmount", BigDecimal.valueOf(100))
                .hasFieldOrPropertyWithValue("remainingAmount", BigDecimal.valueOf(100));
    }

    @Test
    void test_addNewBatch_throws_IllegalArgumentException_when_no_material_found() {
        when(materialDao.find(2l)).thenReturn(Optional.empty());
        BatchOfMaterialDto newBatchDto = BatchOfMaterialDto.builder()
                .material(MaterialDto.builder().id(2l).build())
                .purchaseDate(LocalDate.parse("2020-01-01"))
                .price(BigDecimal.valueOf(1000))
                .purchaseAmount(BigDecimal.valueOf(100))
                .build();
        assertThatThrownBy(()-> materialsService.addNewBatch(newBatchDto))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("No such material");
    }


    @Test
    void test_writeOffFromBatch() {
        BatchOfMaterial batch = BatchOfMaterial.builder()
                .id(3l)
                .material(Material.builder().id(2l).build())
                .purchaseDate(LocalDate.parse("2020-01-01"))
                .price(BigDecimal.valueOf(1000))
                .purchaseAmount(BigDecimal.valueOf(100))
                .build();
        when(batchesDao.find(3l)).thenReturn(Optional.of(batch));

        materialsService.writeOffFromBatch(3l,new BigDecimal("25.50"));

        verify(batchesDao).update(batchOfMaterialArgumentCaptor.capture());

        assertThat(batchOfMaterialArgumentCaptor.getValue())
                .hasFieldOrPropertyWithValue("remainingAmount", new BigDecimal("74.50"));
    }

    @Test
    void test_writeOffFromBatch_throws_IllegalArgumentException_when_withdrawing_too_many() {
        BatchOfMaterial batch = BatchOfMaterial.builder()
                .id(3l)
                .material(Material.builder().id(2l).build())
                .purchaseDate(LocalDate.parse("2020-01-01"))
                .price(BigDecimal.valueOf(1000))
                .purchaseAmount(BigDecimal.valueOf(100))
                .build();
        when(batchesDao.find(3l)).thenReturn(Optional.of(batch));

        assertThatThrownBy(()-> materialsService.writeOffFromBatch(3l,new BigDecimal("105.50")))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Amount to withdraw 105.50 is greater that amount remaining in batch (100)");

    }

    @Test
    void test_writeOffFromBatch_throws_IllegalArgumentException_when_batch_not_found() {
        when(batchesDao.find(3l)).thenReturn(Optional.empty());

        assertThatThrownBy(()-> materialsService.writeOffFromBatch(3l,new BigDecimal("105.50")))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Cannot find batch with such id");

    }

    @Captor
    ArgumentCaptor<ConsumptionRate> consumptionRateArgumentCaptor;

    @Test
    void test_newRate() {
        when(materialDao.find(2l)).thenReturn(Optional.of(Material.builder().id(2l).build()));
        when(consumptionRateDao.create(any(ConsumptionRate.class))).thenReturn(ConsumptionRate.builder().id(100l).build());
        ConsumptionRateDto newRate = ConsumptionRateDto.builder()
                .amount(BigDecimal.valueOf(10))
                .material(MaterialDto.builder().id(2l).build())
                .title("testRate")
                .build();
        ConsumptionRateDto consumptionRateDto = materialsService.newRate(newRate);
        assertThat(consumptionRateDto.getId()).isEqualTo(100l);

        verify(consumptionRateDao).create(consumptionRateArgumentCaptor.capture());

        assertThat(consumptionRateArgumentCaptor.getValue())
                .hasFieldOrPropertyWithValue("amount", BigDecimal.valueOf(10))
                .hasFieldOrPropertyWithValue("material.id", 2l)
                .hasFieldOrPropertyWithValue("title", "testRate");

    }

    @Test
    void test_newRate_throw_IllegalArgumetnException_when_material_not_found() {
        when(materialDao.find(2l)).thenReturn(Optional.empty());
        ConsumptionRateDto newRate = ConsumptionRateDto.builder()
                .amount(BigDecimal.valueOf(10))
                .material(MaterialDto.builder().id(2l).build())
                .title("testRate")
                .build();
        assertThatThrownBy(()->materialsService.newRate(newRate))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("No such material");

    }

    @Test
    void test_updateRate() {
        when(materialDao.find(2l)).thenReturn(Optional.of(Material.builder().id(2l).build()));
        when(consumptionRateDao.find(100l)).thenReturn(Optional.of(ConsumptionRate.builder().id(100l).build()));
        ConsumptionRateDto newRate = ConsumptionRateDto.builder()
                .id(100l)
                .amount(BigDecimal.valueOf(10))
                .material(MaterialDto.builder().id(2l).build())
                .title("testRate")
                .build();
        materialsService.updateRate(newRate);
        verify(consumptionRateDao).update(consumptionRateArgumentCaptor.capture());

        assertThat(consumptionRateArgumentCaptor.getValue())
                .hasFieldOrPropertyWithValue("id", 100l)
                .hasFieldOrPropertyWithValue("amount", BigDecimal.valueOf(10))
                .hasFieldOrPropertyWithValue("material.id", 2l)
                .hasFieldOrPropertyWithValue("title", "testRate");
    }

    @Test
    void test_updateRate_throw_IllegalArumentException_when_material_not_found() {
        when(materialDao.find(2l)).thenReturn(Optional.empty());
        when(consumptionRateDao.find(100l)).thenReturn(Optional.of(ConsumptionRate.builder().id(100l).build()));
        ConsumptionRateDto newRate = ConsumptionRateDto.builder()
                .id(100l)
                .amount(BigDecimal.valueOf(10))
                .material(MaterialDto.builder().id(2l).build())
                .title("testRate")
                .build();
        assertThatThrownBy(()-> materialsService.updateRate(newRate))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("No such material");
    }

    @Test
    void test_updateRate_throw_IllegalArumentException_when_rate_not_found() {
        when(consumptionRateDao.find(100l)).thenReturn(Optional.empty());
        ConsumptionRateDto newRate = ConsumptionRateDto.builder()
                .id(100l)
                .amount(BigDecimal.valueOf(10))
                .material(MaterialDto.builder().id(2l).build())
                .title("testRate")
                .build();
        assertThatThrownBy(()-> materialsService.updateRate(newRate))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Cannot find consumption rate with id 100");
    }

}