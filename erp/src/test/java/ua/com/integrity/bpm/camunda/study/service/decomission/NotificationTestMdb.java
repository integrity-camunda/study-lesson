package ua.com.integrity.bpm.camunda.study.service.decomission;

import lombok.SneakyThrows;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;

@MessageDriven(
        activationConfig = {
                @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "java:jboss/activemq/equipment-decommissioning-notifications"),
                @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
        }
)
public class NotificationTestMdb implements MessageListener {

    @EJB
    NotificationTestHolder notificationTestHolder;

    @Override
    @SneakyThrows
    public void onMessage(Message message) {
        String body = message.getBody(String.class);
        notificationTestHolder.getNotificationList().add(body);
    }
}
