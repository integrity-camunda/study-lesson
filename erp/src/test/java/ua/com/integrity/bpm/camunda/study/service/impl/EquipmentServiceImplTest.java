package ua.com.integrity.bpm.camunda.study.service.impl;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoSettings;
import ua.com.integrity.bpm.camunda.study.dao.GenericDao;
import ua.com.integrity.bpm.camunda.study.dao.equipment.EquipmentDao;
import ua.com.integrity.bpm.camunda.study.dao.equipment.SupplyRateDao;
import ua.com.integrity.bpm.camunda.study.domain.Employee;
import ua.com.integrity.bpm.camunda.study.domain.equipment.Equipment;
import ua.com.integrity.bpm.camunda.study.domain.equipment.EquipmentType;
import ua.com.integrity.bpm.camunda.study.domain.equipment.Maintenance;
import ua.com.integrity.bpm.camunda.study.domain.equipment.SupplyRate;
import ua.com.integrity.bpm.camunda.study.domain.materail.ConsumptionRate;
import ua.com.integrity.bpm.camunda.study.dto.equipment.EquipmentDto;
import ua.com.integrity.bpm.camunda.study.dto.equipment.SupplyRateDto;
import ua.com.integrity.bpm.camunda.study.mapper.ErpMapper;

import javax.inject.Inject;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@MockitoSettings
class EquipmentServiceImplTest {

    @Mock
    private EquipmentDao equipmentDao;
    @Mock
    private GenericDao<EquipmentType, Long> equipmentTypeDao;
    @Mock
    private SupplyRateDao supplyRateDao;
    @Mock
    private GenericDao<Maintenance, Long> maintenanceDao;
    @Mock
    private GenericDao<Employee, Long> employeeDao;
    @Mock
    private GenericDao<ConsumptionRate, Long> consumptionRateDao;

    private ErpMapper mapper = Mappers.getMapper(ErpMapper.class);

    @InjectMocks
    EquipmentServiceImpl equipmentService;

    @BeforeEach
    void setUp() {
        equipmentService.setMapper(mapper);
    }

    @Captor
    ArgumentCaptor<Equipment> equipmentArgumentCaptor;

    @Test
    void test_newEquipment() {
        when(equipmentTypeDao.find(2l)).thenReturn(Optional.of(EquipmentType.builder().id(2l).build()));
        when(equipmentDao.create(any(Equipment.class))).thenReturn(Equipment.builder().build());
        EquipmentDto newEquipment = EquipmentDto.builder()
                .serialNumber("testSN")
                .user(1l)
                .type(2l)
                .purchasePrice(BigDecimal.valueOf(1000))
                .balancePrice(BigDecimal.valueOf(1000))
                .commissioningDate(LocalDate.parse("2020-01-01"))
                .decommissioningDate(LocalDate.parse("2030-01-01"))
                .build();
        equipmentService.newEquipment(newEquipment);
        verify(equipmentDao).create(equipmentArgumentCaptor.capture());
        assertThat(equipmentArgumentCaptor.getValue())
                .hasFieldOrPropertyWithValue("serialNumber","testSN")
                .hasFieldOrPropertyWithValue("user.id",1l)
                .hasFieldOrPropertyWithValue("type.id",2l)
                .hasFieldOrPropertyWithValue("purchasePrice",BigDecimal.valueOf(1000))
                .hasFieldOrPropertyWithValue("balancePrice",BigDecimal.valueOf(1000))
                .hasFieldOrPropertyWithValue("commissioningDate",LocalDate.parse("2020-01-01"))
                .hasFieldOrPropertyWithValue("decommissioningDate",LocalDate.parse("2030-01-01"));
    }

    @Test
    void test_newEquipment_throw_IllegalArgumentException_when_equipment_type_not_found() {
        when(equipmentTypeDao.find(2l)).thenReturn(Optional.empty());
        EquipmentDto newEquipment = EquipmentDto.builder()
                .serialNumber("testSN")
                .user(1l)
                .type(2l)
                .purchasePrice(BigDecimal.valueOf(1000))
                .balancePrice(BigDecimal.valueOf(1000))
                .commissioningDate(LocalDate.parse("2020-01-01"))
                .decommissioningDate(LocalDate.parse("2030-01-01"))
                .build();
        assertThatThrownBy(()-> equipmentService.newEquipment(newEquipment))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Cannot create new equipment of unknown type");
    }

    @Test
    void test_commissionEquipment() {
        when(equipmentDao.find("testSN")).thenReturn(Optional.of(Equipment.builder().serialNumber("testSN").build()));

        equipmentService.commissionEquipment("testSN", LocalDate.parse("2025-05-05"));
        verify(equipmentDao).update(equipmentArgumentCaptor.capture());
        assertThat(equipmentArgumentCaptor.getValue())
                .hasFieldOrPropertyWithValue("commissioningDate",LocalDate.parse("2025-05-05"));
    }

    @Test
    void test_commissionEquipment_throw_IllegalArgumentException_when_no_equipment_was_found() {
        when(equipmentDao.find("testSN")).thenReturn(Optional.empty());

        assertThatThrownBy(()-> equipmentService.commissionEquipment("testSN", LocalDate.parse("2025-05-05")))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Equipment with such s/n not exist");
    }

    @Test
    void test_decommissionEquipment() {
        when(equipmentDao.find("testSN")).thenReturn(Optional.of(Equipment.builder().serialNumber("testSN").build()));

        equipmentService.decommissionEquipment("testSN", LocalDate.parse("2025-05-05"));
        verify(equipmentDao).update(equipmentArgumentCaptor.capture());
        assertThat(equipmentArgumentCaptor.getValue())
                .hasFieldOrPropertyWithValue("decommissioningDate",LocalDate.parse("2025-05-05"));
    }

    @Test
    void test_decommissionEquipment_throw_IllegalArgumentException_when_no_equipment_was_found() {
        when(equipmentDao.find("testSN")).thenReturn(Optional.empty());

        assertThatThrownBy(()-> equipmentService.decommissionEquipment("testSN", LocalDate.parse("2025-05-05")))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Equipment with such s/n not exist");
    }

    @Test
    void test_assignEquipment() {
        when(equipmentDao.find("testSN")).thenReturn(Optional.of(Equipment.builder().serialNumber("testSN").build()));
        when(employeeDao.find(2l)).thenReturn(Optional.of(Employee.builder().id(2l).build()));

        equipmentService.assignEquipment("testSN", 2l);

        verify(equipmentDao).update(equipmentArgumentCaptor.capture());
        assertThat(equipmentArgumentCaptor.getValue())
                .hasFieldOrPropertyWithValue("serialNumber", "testSN")
                .hasFieldOrPropertyWithValue("user.id", 2l);
    }

    @Test
    void test_assignEquipment_throw_IllegalArgumentException_when_employee_not_found() {
        when(equipmentDao.find("testSN")).thenReturn(Optional.of(Equipment.builder().serialNumber("testSN").build()));
        when(employeeDao.find(2l)).thenReturn(Optional.empty());

        assertThatThrownBy(()-> equipmentService.assignEquipment("testSN", 2l))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Employee not exist");

    }

    @Test
    void test_assignEquipment_throw_IllegalArgumentException_when_equipment_not_found() {
        when(equipmentDao.find("testSN")).thenReturn(Optional.empty());

        assertThatThrownBy(()-> equipmentService.assignEquipment("testSN", 2l))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Equipment with such s/n not exist");

    }

    @Test
    void test_retrieveEquipment() {
        when(equipmentDao.find("testSN")).thenReturn(Optional.of(Equipment.builder().serialNumber("testSN").user(Employee.builder().id(1l).build()).build()));

        equipmentService.retrieveEquipment("testSN");

        verify(equipmentDao).update(equipmentArgumentCaptor.capture());
        assertThat(equipmentArgumentCaptor.getValue())
                .hasFieldOrPropertyWithValue("serialNumber", "testSN")
                .hasFieldOrPropertyWithValue("user", null);
    }

    @Test
    void test_retrieveEquipment_throw_IllegalArgumentException_when_equipment_not_found() {
        when(equipmentDao.find("testSN")).thenReturn(Optional.empty());

        assertThatThrownBy(()-> equipmentService.retrieveEquipment("testSN"))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Equipment with such s/n not exist");

    }

    @Captor
    ArgumentCaptor<EquipmentType> equipmentTypeArgumentCaptor;

    @Test
    void test_addMaintenance() {
        when(equipmentTypeDao.find(1l)).thenReturn(Optional.of(EquipmentType.builder().id(1l).title("testType").build()));
        when(maintenanceDao.find(2l)).thenReturn(Optional.of(Maintenance.builder().id(2l).title("testMaintenance").build()));

        equipmentService.addMaintenance(1l, 2l);
        verify(equipmentTypeDao).update(equipmentTypeArgumentCaptor.capture());
        EquipmentType captured = equipmentTypeArgumentCaptor.getValue();
        assertThat(captured)
                .hasFieldOrPropertyWithValue("id", 1l);
        assertThat(captured.getMaintenanceSet())
                .hasSize(1)
                .extracting(Maintenance::getId, Maintenance::getTitle)
                .containsOnly(tuple(2l, "testMaintenance"));
    }

    @Test
    void test_addMaintenance_not_update_equipmentType_when_maintenance_already_exist() {
        when(equipmentTypeDao.find(1l)).thenReturn(Optional.of(EquipmentType.builder().id(1l).title("testType").maintenanceSet(Collections.singleton(Maintenance.builder().id(2l).title("testMaintenance").build())).build()));
        when(maintenanceDao.find(2l)).thenReturn(Optional.of(Maintenance.builder().id(2l).title("testMaintenance").build()));

        equipmentService.addMaintenance(1l, 2l);
        verify(equipmentTypeDao).find(eq(1l));
        verifyNoMoreInteractions(equipmentTypeDao);

    }

    @Test
    void test_addMaintenance_throw_IllegalArgumentException_when_maintenece_not_found() {
        when(equipmentTypeDao.find(1l)).thenReturn(Optional.of(EquipmentType.builder().id(1l).title("testType").build()));
        when(maintenanceDao.find(2l)).thenReturn(Optional.empty());

        assertThatThrownBy(()-> equipmentService.addMaintenance(1l, 2l))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Maintenance not exist");

    }

    @Test
    void test_addMaintenance_throw_IllegalArgumentException_when_type_not_found() {
        when(equipmentTypeDao.find(1l)).thenReturn(Optional.empty());

        assertThatThrownBy(()-> equipmentService.addMaintenance(1l, 2l))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("EquipmentType not exist");
    }

    @Test
    void test_removeMaintenance_when_maintenance_already_exist() {
        when(equipmentTypeDao.find(1l)).thenReturn(Optional.of(EquipmentType.builder().id(1l).title("testType").maintenanceSet(Stream.of(Maintenance.builder().id(2l).title("testMaintenance").build()).collect(Collectors.toSet())).build()));
        when(maintenanceDao.find(2l)).thenReturn(Optional.of(Maintenance.builder().id(2l).title("testMaintenance").build()));

        equipmentService.removeMaintenance(1l, 2l);
        verify(equipmentTypeDao).update(equipmentTypeArgumentCaptor.capture());
        EquipmentType captured = equipmentTypeArgumentCaptor.getValue();
        assertThat(captured)
                .hasFieldOrPropertyWithValue("id", 1l);
        assertThat(captured.getMaintenanceSet())
                .hasSize(0);
    }

    @Test
    void test_removeMaintenance_not_update_equipmentType_when_maintenance_not_exist() {
        when(equipmentTypeDao.find(1l)).thenReturn(Optional.of(EquipmentType.builder().id(1l).title("testType").maintenanceSet(Collections.singleton(Maintenance.builder().id(3l).title("testMaintenance2").build())).build()));
        when(maintenanceDao.find(2l)).thenReturn(Optional.of(Maintenance.builder().id(2l).title("testMaintenance").build()));

        equipmentService.removeMaintenance(1l, 2l);
        verify(equipmentTypeDao).find(eq(1l));
        verifyNoMoreInteractions(equipmentTypeDao);

    }

    @Test
    void test_removeMaintenance_throw_IllegalArgumentException_when_maintenece_not_found() {
        when(equipmentTypeDao.find(1l)).thenReturn(Optional.of(EquipmentType.builder().id(1l).title("testType").build()));
        when(maintenanceDao.find(2l)).thenReturn(Optional.empty());

        assertThatThrownBy(()-> equipmentService.removeMaintenance(1l, 2l))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Maintenance not exist");

    }

    @Test
    void test_removeMaintenance_throw_IllegalArgumentException_when_type_not_found() {
        when(equipmentTypeDao.find(1l)).thenReturn(Optional.empty());

        assertThatThrownBy(()-> equipmentService.removeMaintenance(1l, 2l))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("EquipmentType not exist");
    }

    @Test
    void test_addConsumptionRate() {
        when(equipmentTypeDao.find(1l)).thenReturn(Optional.of(EquipmentType.builder().id(1l).title("testType").build()));
        when(consumptionRateDao.find(2l)).thenReturn(Optional.of(ConsumptionRate.builder().id(2l).title("testRate").build()));

        equipmentService.addConsumptionRate(1l, 2l);
        verify(equipmentTypeDao).update(equipmentTypeArgumentCaptor.capture());
        EquipmentType captured = equipmentTypeArgumentCaptor.getValue();
        assertThat(captured)
                .hasFieldOrPropertyWithValue("id", 1l);
        assertThat(captured.getConsumptionRates())
                .hasSize(1)
                .extracting(ConsumptionRate::getId, ConsumptionRate::getTitle)
                .containsOnly(tuple(2l, "testRate"));
    }

    @Test
    void test_addConsumptionRate_not_update_equipmentType_when_maintenance_already_exist() {
        when(equipmentTypeDao.find(1l)).thenReturn(Optional.of(EquipmentType.builder().id(1l).title("testType").consumptionRates(Collections.singleton(ConsumptionRate.builder().id(2l).title("testRate").build())).build()));
        when(consumptionRateDao.find(2l)).thenReturn(Optional.of(ConsumptionRate.builder().id(2l).title("testRate").build()));

        equipmentService.addConsumptionRate(1l, 2l);
        verify(equipmentTypeDao).find(eq(1l));
        verifyNoMoreInteractions(equipmentTypeDao);

    }

    @Test
    void test_addConsumptionRate_throw_IllegalArgumentException_when_maintenece_not_found() {
        when(equipmentTypeDao.find(1l)).thenReturn(Optional.of(EquipmentType.builder().id(1l).title("testType").build()));
        when(consumptionRateDao.find(2l)).thenReturn(Optional.empty());

        assertThatThrownBy(()-> equipmentService.addConsumptionRate(1l, 2l))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("ConsumptionRate not exist");

    }

    @Test
    void test_addConsumptionRate_throw_IllegalArgumentException_when_type_not_found() {
        when(equipmentTypeDao.find(1l)).thenReturn(Optional.empty());

        assertThatThrownBy(()-> equipmentService.addConsumptionRate(1l, 2l))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("EquipmentType not exist");
    }

    @Test
    void test_removeConsumptionRate_when_rate_already_exist() {
        when(equipmentTypeDao.find(1l)).thenReturn(Optional.of(EquipmentType.builder().id(1l).title("testType").consumptionRates(Stream.of(ConsumptionRate.builder().id(2l).title("testRate").build()).collect(Collectors.toSet())).build()));
        when(consumptionRateDao.find(2l)).thenReturn(Optional.of(ConsumptionRate.builder().id(2l).title("testRate").build()));

        equipmentService.removeConsumptionRate(1l, 2l);
        verify(equipmentTypeDao).update(equipmentTypeArgumentCaptor.capture());
        EquipmentType captured = equipmentTypeArgumentCaptor.getValue();
        assertThat(captured)
                .hasFieldOrPropertyWithValue("id", 1l);
        assertThat(captured.getConsumptionRates())
                .hasSize(0);
    }

    @Test
    void test_removeConsumptionRate_not_update_equipmentType_when_rate_not_exist() {
        when(equipmentTypeDao.find(1l)).thenReturn(Optional.of(EquipmentType.builder().id(1l).title("testType").consumptionRates(Collections.singleton(ConsumptionRate.builder().id(3l).title("testRate2").build())).build()));
        when(consumptionRateDao.find(2l)).thenReturn(Optional.of(ConsumptionRate.builder().id(2l).title("testRate").build()));

        equipmentService.removeConsumptionRate(1l, 2l);
        verify(equipmentTypeDao).find(eq(1l));
        verifyNoMoreInteractions(equipmentTypeDao);

    }

    @Test
    void test_removeConsumptionRate_throw_IllegalArgumentException_when_rate_not_found() {
        when(equipmentTypeDao.find(1l)).thenReturn(Optional.of(EquipmentType.builder().id(1l).title("testType").build()));
        when(consumptionRateDao.find(2l)).thenReturn(Optional.empty());

        assertThatThrownBy(()-> equipmentService.removeConsumptionRate(1l, 2l))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("ConsumptionRate not exist");

    }

    @Test
    void test_removeConsumptionRate_throw_IllegalArgumentException_when_type_not_found() {
        when(equipmentTypeDao.find(1l)).thenReturn(Optional.empty());

        assertThatThrownBy(()-> equipmentService.removeConsumptionRate(1l, 2l))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("EquipmentType not exist");
    }

    @Test
    void addConsumptionRateToMaintenance() {
    }

    @Captor
    ArgumentCaptor<Maintenance> maintenanceArgumentCaptor;

    @Test
    void test_addConsumptionRateToMaintenance() {
        when(maintenanceDao.find(1l)).thenReturn(Optional.of(Maintenance.builder().id(1l).title("testMaintenance").build()));
        when(consumptionRateDao.find(2l)).thenReturn(Optional.of(ConsumptionRate.builder().id(2l).title("testRate").build()));

        equipmentService.addConsumptionRateToMaintenance(1l, 2l);
        verify(maintenanceDao).update(maintenanceArgumentCaptor.capture());
        Maintenance captured = maintenanceArgumentCaptor.getValue();
        assertThat(captured)
                .hasFieldOrPropertyWithValue("id", 1l);
        assertThat(captured.getConsumptionRates())
                .hasSize(1)
                .extracting(ConsumptionRate::getId, ConsumptionRate::getTitle)
                .containsOnly(tuple(2l, "testRate"));
    }

    @Test
    void test_addConsumptionRateToMaintenance_not_update_equipmentType_when_rate_already_exist() {
        when(maintenanceDao.find(1l)).thenReturn(Optional.of(Maintenance.builder().id(1l).title("testMaintenance").consumptionRates(Collections.singleton(ConsumptionRate.builder().id(2l).title("testRate").build())).build()));
        when(consumptionRateDao.find(2l)).thenReturn(Optional.of(ConsumptionRate.builder().id(2l).title("testRate").build()));

        equipmentService.addConsumptionRateToMaintenance(1l, 2l);
        verify(maintenanceDao).find(eq(1l));
        verifyNoMoreInteractions(maintenanceDao);

    }

    @Test
    void test_addConsumptionRateToMaintenance_throw_IllegalArgumentException_when_rate_not_found() {
        when(maintenanceDao.find(1l)).thenReturn(Optional.of(Maintenance.builder().id(1l).title("testMaintenance").build()));
        when(consumptionRateDao.find(2l)).thenReturn(Optional.empty());

        assertThatThrownBy(()-> equipmentService.addConsumptionRateToMaintenance(1l, 2l))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("ConsumptionRate not exist");

    }

    @Test
    void test_addConsumptionRateToMaintenance_throw_IllegalArgumentException_when_type_not_found() {
        when(maintenanceDao.find(1l)).thenReturn(Optional.empty());

        assertThatThrownBy(()-> equipmentService.addConsumptionRateToMaintenance(1l, 2l))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Maintenance not exist");
    }

    @Test
    void removeConsumptionRateFromMaintenance() {
    }

    @Test
    void test_removeConsumptionRateFromMaintenance() {
        when(maintenanceDao.find(1l)).thenReturn(Optional.of(Maintenance.builder().id(1l).title("testMaintenance").consumptionRates(Stream.of(ConsumptionRate.builder().id(2l).title("testRate").build()).collect(Collectors.toSet())).build()));
        when(consumptionRateDao.find(2l)).thenReturn(Optional.of(ConsumptionRate.builder().id(2l).title("testRate").build()));

        equipmentService.removeConsumptionRateFromMaintenance(1l, 2l);
        verify(maintenanceDao).update(maintenanceArgumentCaptor.capture());
        Maintenance captured = maintenanceArgumentCaptor.getValue();
        assertThat(captured)
                .hasFieldOrPropertyWithValue("id", 1l);
        assertThat(captured.getConsumptionRates())
                .hasSize(0);
    }

    @Test
    void test_removeConsumptionRateFromMaintenance_not_update_maintenence_when_rate_not_exist() {
        when(maintenanceDao.find(1l)).thenReturn(Optional.of(Maintenance.builder().id(1l).title("testMaintenance").consumptionRates(Collections.singleton(ConsumptionRate.builder().id(3l).title("testRate2").build())).build()));
        when(consumptionRateDao.find(2l)).thenReturn(Optional.of(ConsumptionRate.builder().id(2l).title("testRate").build()));

        equipmentService.removeConsumptionRateFromMaintenance(1l, 2l);
        verify(maintenanceDao).find(eq(1l));
        verifyNoMoreInteractions(maintenanceDao);

    }

    @Test
    void test_removeConsumptionRateFromMaintenance_throw_IllegalArgumentException_when_rate_not_found() {
        when(maintenanceDao.find(1l)).thenReturn(Optional.of(Maintenance.builder().id(1l).title("testMaintenance").build()));
        when(consumptionRateDao.find(2l)).thenReturn(Optional.empty());

        assertThatThrownBy(()-> equipmentService.removeConsumptionRateFromMaintenance(1l, 2l))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("ConsumptionRate not exist");

    }

    @Test
    void test_removeConsumptionRateFromMaintenance_throw_IllegalArgumentException_when_maintenence_not_found() {
        when(maintenanceDao.find(1l)).thenReturn(Optional.empty());

        assertThatThrownBy(()-> equipmentService.removeConsumptionRateFromMaintenance(1l, 2l))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Maintenance not exist");
    }

    @Captor
    ArgumentCaptor<SupplyRate> supplyRateArgumentCaptor;

    @Test
    void test_newSupplyRate() {
        when(equipmentTypeDao.find(1l)).thenReturn(Optional.of(EquipmentType.builder().id(1l).build()));
        when(supplyRateDao.create(any())).thenReturn(SupplyRate.builder().id(2l).build());
        SupplyRateDto newRate = SupplyRateDto.builder()
                .equipmentType(1l)
                .amount(2)
                .title("testRate")
                .build();

        SupplyRateDto created = equipmentService.newSupplyRate(newRate);
        assertThat(created.getId()).isEqualTo(2l);

        verify(supplyRateDao).create(supplyRateArgumentCaptor.capture());

        assertThat(supplyRateArgumentCaptor.getValue())
                .hasFieldOrPropertyWithValue("title", "testRate")
                .hasFieldOrPropertyWithValue("equipmentType.id", 1l)
                .hasFieldOrPropertyWithValue("amount", 2);
    }

    @Test
    void test_newSupplyRate_throw_IllegalArgumentException_when_no_type_found() {
        when(equipmentTypeDao.find(1l)).thenReturn(Optional.empty());
        SupplyRateDto newRate = SupplyRateDto.builder()
                .equipmentType(1l)
                .amount(2)
                .title("testRate")
                .build();

        assertThatThrownBy(()-> equipmentService.newSupplyRate(newRate))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("EquipmentType not exist");

    }

    @Test
    void test_updateSupplyRate() {
        when(equipmentTypeDao.find(1l)).thenReturn(Optional.of(EquipmentType.builder().id(1l).build()));
        when(supplyRateDao.find(2l)).thenReturn(Optional.of(SupplyRate.builder().id(2l).build()));
        SupplyRateDto newRate = SupplyRateDto.builder()
                .id(2l)
                .equipmentType(1l)
                .amount(2)
                .title("testRate")
                .build();

        equipmentService.updateSupplyRate(newRate);

        verify(supplyRateDao).update(supplyRateArgumentCaptor.capture());

        assertThat(supplyRateArgumentCaptor.getValue())
                .hasFieldOrPropertyWithValue("title", "testRate")
                .hasFieldOrPropertyWithValue("equipmentType.id", 1l)
                .hasFieldOrPropertyWithValue("amount", 2);

    }

    @Test
    void test_updateSupplyRate_throw_IllegalArgumentException_when_type_not_found() {
        when(equipmentTypeDao.find(1l)).thenReturn(Optional.empty());
        when(supplyRateDao.find(2l)).thenReturn(Optional.of(SupplyRate.builder().id(2l).build()));
        SupplyRateDto newRate = SupplyRateDto.builder()
                .id(2l)
                .equipmentType(1l)
                .amount(2)
                .title("testRate")
                .build();

        assertThatThrownBy(()-> equipmentService.updateSupplyRate(newRate))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("EquipmentType not exist");

    }

    @Test
    void test_updateSupplyRate_throw_IllegalArgumentException_when_rate_not_found() {
        when(supplyRateDao.find(2l)).thenReturn(Optional.empty());
        SupplyRateDto newRate = SupplyRateDto.builder()
                .id(2l)
                .equipmentType(1l)
                .amount(2)
                .title("testRate")
                .build();

        assertThatThrownBy(()-> equipmentService.updateSupplyRate(newRate))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("SupplyRate not exist");

    }

}