package ua.com.integrity.bpm.camunda.study.api.rest;

import io.restassured.RestAssured;
import org.assertj.core.api.Assertions;
import org.hamcrest.Matchers;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.importer.ZipImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import ua.com.integrity.bpm.camunda.study.dto.EmployeeDto;
import ua.com.integrity.bpm.camunda.study.dto.OrgUnitDto;
import ua.com.integrity.bpm.camunda.study.dto.PositionDto;
import ua.com.integrity.bpm.camunda.study.dto.equipment.SupplyRateDto;
import ua.com.integrity.bpm.camunda.study.dto.vacation.VacationDto;
import ua.com.integrity.bpm.camunda.study.dto.vacation.VacationType;

import javax.ws.rs.core.MediaType;
import java.io.File;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class OrgStructureRestIT {

    @Deployment(testable = false)
    public static Archive<?> getEarArchive() {
        // Import the web archive that was created by Maven:
        File f = new File("./target/erp.war");
        if (f.exists() == false) {
            throw new RuntimeException("File " + f.getAbsolutePath() + " does not exist.");
        }
        WebArchive war = ShrinkWrap.create(ZipImporter.class, "org-structure-rest-erp-test.war").importFrom(f).as(WebArchive.class);
        war.addPackage("ua.com.integrity.bpm.camunda.study.api.rest");
        return war;
    }

    @ArquillianResource
    URL applicationUrl;

    String basePath;

    @Before
    public void setUp() throws Exception {
        basePath = applicationUrl.toString().concat("api/rest/org-structure/");
    }

    @Test
    public void test_getUnits() {
        List<String> unitTitles = RestAssured.when()
                .get(basePath + "unit")
                .then()
                .statusCode(200)
                .extract().jsonPath()
                .getList("title", String.class);
        assertThat(unitTitles)
                .contains("ООО ТЕСТОВАЯ КОМПАНИЯ", "Инженерный отдел", "Отдел продаж");
    }

    @Test
    public void testGetUnit() {
        RestAssured.when()
                .get(basePath + "unit/1")
                .then()
                .statusCode(200)
                .body("title", is("ООО ТЕСТОВАЯ КОМПАНИЯ"));
    }

    @Test
    public void testGetUnit_with_unknown_id() {
        RestAssured.when()
                .get(basePath + "unit/987987")
                .then()
                .statusCode(404);
    }

    @Test
    public void test_createUnit() {
        long unitId = RestAssured.with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(OrgUnitDto.builder().title("New test unit").ascendant(1L).build())
                .post(basePath + "unit")
                .then()
                .statusCode(200)
                .extract().jsonPath().getLong("id");
        assertThat(unitId).isGreaterThanOrEqualTo(5000L);
    }

    @Test
    public void test_getPosition() {
        RestAssured.when()
                .get(basePath + "position/22")
                .then()
                .statusCode(200)
                .body("title", is("Главный сцециалист по продажам"));
    }

    @Test
    public void test_getPosition_unknown_id() {
        RestAssured.when()
                .get(basePath + "position/656565")
                .then()
                .statusCode(404);
    }

    @Test
    public void test_getPositions() {
        List<Long> positionIds = RestAssured.when()
                .get(basePath + "position")
                .then()
                .statusCode(200)
                .extract().jsonPath().getList("id", Long.class);
        assertThat(positionIds)
                .contains(11L, 12L, 13L, 14L, 15L, 21L, 22L, 23L, 31L, 32L, 33L);
    }

    @Test
    public void test_createPosition() {
        long positionId = RestAssured.with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(PositionDto.builder().title("Test position").orgUnit(3L).index(20).build())
                .when()
                .post(basePath + "position")
                .then()
                .statusCode(200)
                .extract().jsonPath().getLong("id");
        assertThat(positionId).isGreaterThanOrEqualTo(5000L);
    }

    @Test
    public void test_createPosition_with_invalid_body() {
        RestAssured.with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(PositionDto.builder().title("Test position").build())
                .when()
                .post(basePath + "position")
                .then()
                .statusCode(400);
    }

    @Test
    public void test_addSupplyRate() {
        List<Long> supplyRates = RestAssured.with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(SupplyRateDto.builder().id(4021L).build())
                .when()
                .patch(basePath + "position/15/supply-rate")
                .then()
                .statusCode(200)
                .extract().jsonPath().getList("supplyRates", Long.class);
        assertThat(supplyRates).contains(4021L);
    }

    @Test
    public void test_addSupplyRate_unknown_position() {
        RestAssured.with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(SupplyRateDto.builder().id(4021L).build())
                .when()
                .patch(basePath + "position/45454/supply-rate")
                .then()
                .statusCode(400)
                .body("message", is("No such position"));
    }

    @Test
    public void test_addSupplyRate_unknown_supply_rate() {
        RestAssured.with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(SupplyRateDto.builder().id(4021231311L).build())
                .patch(basePath + "position/15/supply-rate")
                .then()
                .statusCode(400)
                .body("message", is("No such supply rate"));
    }

    @Test
    public void test_addSupplyRate_linked_supply_rate() {
        RestAssured.with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(SupplyRateDto.builder().id(4003L).build())
                .when()
                .patch(basePath + "position/15/supply-rate")
                .then()
                .statusCode(400)
                .body("message", is("Supply rate already linked to position."));
    }


    @Test
    public void test_removeSupplyRate() {
        List<Long> supplyRates = RestAssured.when()
                .delete(basePath + "position/15/supply-rate/4010")
                .then()
                .statusCode(200)
                .extract().jsonPath().getList("supplyRates", Long.class);
        assertThat(supplyRates).doesNotContain(4010L);
    }

    @Test
    public void test_removeSupplyRate_unknown_position() {
        RestAssured.when()
                .delete(basePath + "position/568484/supply-rate/4010")
                .then()
                .statusCode(400)
                .body("message", is("No such position"));
    }

    @Test
    public void test_getEmployee() {
        RestAssured.when()
                .get(basePath + "employee/221")
                .then()
                .statusCode(200)
                .body("firstName", is("Ольга"))
                .body("birthDate", is("2000-07-25"));
    }

    @Test
    public void test_getEmployee_unknown() {
        RestAssured.when()
                .get(basePath + "employee/656584")
                .then()
                .statusCode(404);
    }

    @Test
    public void testGetEmployeeByLogin() {
        RestAssured.when()
                .get(basePath + "employee/login/orynochnaya")
                .then()
                .statusCode(200)
                .body("id", is(221))
                .body("firstName", is("Ольга"))
                .body("birthDate", is("2000-07-25"));
    }

    @Test
    public void testGetEmployeeByLogin_unknown_login() {
        RestAssured.when()
                .get(basePath + "employee/login/unknown")
                .then()
                .statusCode(404);
    }

    @Test
    public void test_getEmployees() {
        List<Long> employeeIds = RestAssured.when()
                .get(basePath + "employee")
                .then()
                .statusCode(200)
                .extract().jsonPath().getList("id", Long.class);
        assertThat(employeeIds).contains(111L, 121L, 131L, 141L, 151L, 211L, 311L);
    }

    @Test
    public void test_newEmployee() {
        long id = RestAssured.with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(EmployeeDto.builder()
                        .firstName("TEST").lastName("TESTOV").login("ttestov")
                        .birthDate(LocalDate.parse("1990-01-01"))
                        .hireDate(LocalDate.parse("2020-02-02"))
                        .position(36L).build())
                .when()
                .post(basePath + "employee")
                .then()
                .statusCode(200)
                .extract().jsonPath()
                .getLong("id");
        assertThat(id).isGreaterThanOrEqualTo(5000L);
    }

    @Test
    public void test_newEmployee_with_unknown_position() {
        String message = RestAssured.with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(EmployeeDto.builder()
                        .firstName("TEST").lastName("TESTOV").login("ttestov")
                        .birthDate(LocalDate.parse("1990-01-01"))
                        .hireDate(LocalDate.parse("2020-02-02"))
                        .position(4654L).build())
                .when()
                .post(basePath + "employee")
                .then()
                .statusCode(400)
                .extract().jsonPath().getString("message");
        assertThat(message).isEqualTo("Cannot create new employee with unknown position");
    }

    @Test
    public void test_newEmployee_with_linked_position() {
        String message = RestAssured.with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(EmployeeDto.builder()
                        .firstName("TEST").lastName("TESTOV").login("linkedtestov")
                        .birthDate(LocalDate.parse("1990-01-01"))
                        .hireDate(LocalDate.parse("2020-02-02"))
                        .position(12L).build())
                .when()
                .post(basePath + "employee")
                .then()
                .statusCode(400)
                .extract().jsonPath().getString("message");
        assertThat(message).isEqualTo("Position is already linked to employee.");
    }

    @Test
    public void test_newEmployee_with_invalid_body() {
        List result = RestAssured.with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(EmployeeDto.builder()
                        .firstName("TEST").lastName("TESTOV")
                        .position(36L).build())
                .when()
                .post(basePath + "employee")
                .then()
                .statusCode(400).extract().as(List.class);
        assertThat(result).hasSizeGreaterThanOrEqualTo(3);
    }

    @Test
    public void test_updateEmployee() {
        RestAssured.with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(EmployeeDto.builder().id(331L)
                        .firstName("TEST").lastName("TESTOVNEW").login("ttestovnew")
                        .birthDate(LocalDate.parse("1990-01-01"))
                        .hireDate(LocalDate.parse("2020-02-02"))
                        .position(33L).build())
                .when()
                .put(basePath + "employee/331")
                .then()
                .statusCode(204);
    }

    @Test
    public void test_updateEmployee_id_mismatch() {
        RestAssured.with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(EmployeeDto.builder().id(331L)
                        .firstName("TEST").lastName("TESTOV").login("ttestov")
                        .birthDate(LocalDate.parse("1990-01-01"))
                        .hireDate(LocalDate.parse("2020-02-02"))
                        .position(33L).build())
                .when()
                .put(basePath + "employee/3315454")
                .then()
                .statusCode(400)
        .body("message", is("Employee id not matches with update body"));
    }

    @Test
    public void test_updateEmployee_invalid_body() {
        List result = RestAssured.with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(EmployeeDto.builder().id(331L)
                        .firstName("TEST").lastName("TESTOV")
                        .position(33L).build())
                .when()
                .put(basePath + "employee/331")
                .then()
                .statusCode(400)
                .extract().as(List.class);
        assertThat(result).hasSizeGreaterThanOrEqualTo(3);
    }

    @Test
    public void test_getVacations() {
        RestAssured.when()
                .get(basePath + "vacation")
                .then()
                .statusCode(200);
    }

    @Test
    public void test_newVacation() {
        long id = RestAssured.with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(VacationDto.builder().employee(121L)
                        .startDate(LocalDate.parse("2020-01-01"))
                        .duration(10).type(VacationType.ANNUALLY)
                        .build())
                .when()
                .post(basePath + "vacation")
                .then()
                .statusCode(200)
                .extract().jsonPath().getLong("id");
        assertThat(id).isGreaterThanOrEqualTo(5000L);
    }

    @Test
    public void test_newVacation_unknown_employee() {
        RestAssured.with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(VacationDto.builder().employee(12311L)
                        .startDate(LocalDate.parse("2020-01-01"))
                        .duration(10).type(VacationType.ANNUALLY)
                        .build())
                .when()
                .post(basePath + "vacation")
                .then()
                .statusCode(400)
                .body("message", is("Cannot create vacation for not existing employee"));
    }

    @Test
    public void test_newVacation_invalid_body() {
        List result = RestAssured.with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(VacationDto.builder()
                        .duration(10).type(VacationType.ANNUALLY)
                        .build())
                .when()
                .post(basePath + "vacation")
                .then()
                .statusCode(400)
                .extract().as(List.class);
        assertThat(result).hasSize(2);
    }

    @Test
    public void cancelVacation() {
        RestAssured.when()
                .delete(basePath + "vacation/40404")
                .then()
                .statusCode(204);
    }
}