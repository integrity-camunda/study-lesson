package ua.com.integrity.bpm.camunda.study.service.decomission;

import lombok.Getter;
import lombok.Setter;

import javax.ejb.Singleton;
import java.util.LinkedList;
import java.util.List;

@Singleton
@Getter @Setter
public class NotificationTestHolder {
    private List<String> notificationList = new LinkedList<>();
}
