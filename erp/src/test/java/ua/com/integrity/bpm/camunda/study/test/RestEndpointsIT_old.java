package ua.com.integrity.bpm.camunda.study.test;

import io.restassured.RestAssured;
import lombok.SneakyThrows;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.importer.ZipImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.ScopeType;
import org.jboss.shrinkwrap.resolver.api.maven.coordinate.MavenDependencies;
import org.jboss.shrinkwrap.resolver.api.maven.coordinate.MavenDependency;
import org.junit.Test;
import org.junit.runner.RunWith;
import ua.com.integrity.bpm.camunda.study.domain.OrgUnit;
import ua.com.integrity.bpm.camunda.study.dto.EmployeeDto;
import ua.com.integrity.bpm.camunda.study.dto.OrgUnitDto;
import ua.com.integrity.bpm.camunda.study.dto.PositionDto;
import ua.com.integrity.bpm.camunda.study.dto.api.EquipmentCommissioningRequest;
import ua.com.integrity.bpm.camunda.study.dto.equipment.EquipmentDto;
import ua.com.integrity.example.wsimport.orgstructure.OrgStructure;
import ua.com.integrity.example.wsimport.orgstructure.OrgStructureWs;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

/**
 * Sample integration test: demonstrates how to create the WAR file using the ShrinkWrap API.
 * 
 * Delete this file if no integration test is required.
 */
@RunWith(Arquillian.class)
public class RestEndpointsIT_old {

    /**
     * Creates the WAR file that is deployed to the server.
     *
     * @return WAR archive
     */
    @Deployment(testable = false)
    public static Archive<?> getEarArchive() {
        // Import the web archive that was created by Maven:
        File f = new File("./target/erp.war");
        if (f.exists() == false) {
            throw new RuntimeException("File " + f.getAbsolutePath() + " does not exist.");
        }
//        MavenDependency[] dependencies = new MavenDependency[] {
//                MavenDependencies.createDependency("org.jboss.shrinkwrap.resolver:shrinkwrap-resolver-impl-maven:jar:2.2.7", ScopeType.TEST, false),
//                MavenDependencies.createDependency("io.rest-assured:rest-assured:jar:4.3.3", ScopeType.TEST, false),
//                MavenDependencies.createDependency("org.assertj:assertj-core:jar:3.19.0", ScopeType.TEST, false)
//        };
//
//        File[] libs = Maven.configureResolver()
//                .addDependencies(dependencies)
//                .resolve().withTransitivity().asFile();

        WebArchive war = ShrinkWrap.create(ZipImporter.class, "erp-test-arch.war").importFrom(f).as(WebArchive.class);

        // Add the package containing the test classes:
        war.addPackage("ua.com.integrity.bpm.camunda.study.test");
//        war.addAsLibraries(libs);

        // Export the WAR file to examine it in case of problems:
        // war.as(ZipExporter.class).exportTo(new File("c:\\temp\\test.war"), true);

        return war;
    }

    @ArquillianResource
    URL applicationUrl;


    /**
     * A sample test...
     */
    @Test
    public void test_that_org_unit_has_no_less_than_3_records() {
        String path = applicationUrl.toString().concat("api/rest/org-structure/unit");
        ArrayList result = RestAssured.when()
                .get(path)
                .then()
                .statusCode(200)
                .extract()
                .as(ArrayList.class);
        assertThat(result).hasSizeGreaterThanOrEqualTo(3);
    }

    @Test
    public void test_that_org_unit_with_id_2_has_correct_title() {
        String path = applicationUrl.toString().concat("api/rest/org-structure/unit/2");
        RestAssured.when()
                .get(path)
                .then()
                .statusCode(200)
                .body("title", Matchers.is("Инженерный отдел"));
    }

    @Test
    public void test_that_org_unit_will_successfully_created_through_REST() {
        String path = applicationUrl.toString().concat("api/rest/org-structure/unit");
        OrgUnitDto testUnit = RestAssured.with()
                .body(OrgUnitDto.builder().ascendant(1L).title("testUnit").build())
                .contentType(MediaType.APPLICATION_JSON)
                .when()
                .post(path)
                .then()
                .statusCode(200)
                .extract()
                .as(OrgUnitDto.class);
        Long testUnitId = testUnit.getId();
        assertThat(testUnitId).isNotNull();

        RestAssured.when()
                .get(path + "/" + testUnitId)
                .then()
                .statusCode(200)
                .body("title", Matchers.is("testUnit"));
    }

    @Test
    public void test_that_new_employee_creating_will_be_successfull() {
        String path = applicationUrl.toString().concat("api/rest/org-structure/employee");
        EmployeeDto employeeDto = EmployeeDto.builder()
                .birthDate(LocalDate.parse("1992-07-09"))
                .hireDate(LocalDate.parse("2020-02-02"))
                .lastName("Ivanchenko")
                .firstName("Viktor")
                .position(34L)
                .build();
        EmployeeDto result = RestAssured.with()
                .body(employeeDto)
                .contentType(MediaType.APPLICATION_JSON)
                .when()
                .post(path)
                .then()
                .statusCode(200)
                .extract()
                .as(EmployeeDto.class);
        RestAssured.when()
                .get(path + "/" + result.getId())
                .then()
                .statusCode(200)
                .body("lastName", Matchers.is("Ivanchenko"))
                .body("birthDate", Matchers.is("1992-07-09"))
                .body("hireDate", Matchers.is("2020-02-02"));
    }

    @Test
    public void test_that_new_position_creating_will_be_successfull() {
        String path = applicationUrl.toString().concat("api/rest/org-structure/position");
        PositionDto positionDto = PositionDto.builder().orgUnit(3L).title("newPosition").index(20).build();
        PositionDto result = RestAssured.with()
                .body(positionDto)
                .contentType(MediaType.APPLICATION_JSON)
                .when()
                .post(path)
                .then()
                .statusCode(200)
                .extract()
                .as(PositionDto.class);
        RestAssured.when()
                .get(path + "/" + result.getId())
                .then()
                .statusCode(200)
                .body("title", Matchers.is("newPosition"));
    }

    @Test
    public void test_that_new_equipment_creating_will_be_successfull() {
        String path = applicationUrl.toString().concat("api/rest/equipment");
        EquipmentDto equipmentDto = EquipmentDto.builder().serialNumber("testSerNum")
                .purchasePrice(new BigDecimal("202020.05")).type(3001L)
                .build();
        RestAssured.with()
                .body(equipmentDto)
                .contentType(MediaType.APPLICATION_JSON)
                .when()
                .post(path)
                .then()
                .statusCode(200);
        ArrayList by3001Type = RestAssured.with()
                .queryParam("type", 3001)
                .when()
                .get(path)
                .then()
                .statusCode(200)
                .extract().as(ArrayList.class);
        assertThat(by3001Type).hasSize(8);
    }

    @Test
    public void test_that_new_equipment_commissioning_will_be_successfull() {
        String sNum = "3001SN2";
        String path = applicationUrl.toString().concat("api/rest/equipment/3001SN2/commission");

        EquipmentCommissioningRequest request = new EquipmentCommissioningRequest(sNum, "2021-02-02");
        EquipmentDto equipmentDto = RestAssured.with()
                .body(request)
                .contentType(MediaType.APPLICATION_JSON)
                .when()
                .patch(path)
                .then()
                .statusCode(200)
                .extract().as(EquipmentDto.class);
        assertThat(equipmentDto.getSerialNumber()).isEqualTo(sNum);
        assertThat(equipmentDto.getCommissioningDate().toString()).isEqualTo("2021-02-02");

    }

    @Test
    public void test_that_new_equipment_decommissioning_will_be_successfull() {
        String sNum = "3001SN2";
        String path = applicationUrl.toString().concat("api/rest/equipment/3001SN2/decommission");

        EquipmentCommissioningRequest request = new EquipmentCommissioningRequest(sNum, "2021-02-02");
        EquipmentDto equipmentDto = RestAssured.with()
                .body(request)
                .contentType(MediaType.APPLICATION_JSON)
                .when()
                .patch(path)
                .then()
                .statusCode(200)
                .extract().as(EquipmentDto.class);
        assertThat(equipmentDto.getSerialNumber()).isEqualTo(sNum);
        assertThat(equipmentDto.getDecommissioningDate().toString()).isEqualTo("2021-02-02");

    }

}
