package ua.com.integrity.bpm.camunda.study.service.decomission;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.importer.ZipImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ejb.EJB;
import java.io.File;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Arquillian.class)
public class EquipmentDecommissionNotificationServiceIT {

    @Deployment
    public static Archive<?> getEarArchive() {
        // Import the web archive that was created by Maven:

        File[] testDependencies = Maven.configureResolver()
                .resolve("org.assertj:assertj-core:jar:3.19.0")
                .withTransitivity()
                .asFile();

        File f = new File("./target/erp.war");
        if (f.exists() == false) {
            throw new RuntimeException("File " + f.getAbsolutePath() + " does not exist.");
        }
        WebArchive war = ShrinkWrap.create(ZipImporter.class, "org-structure-rest-erp-test.war").importFrom(f).as(WebArchive.class);
        war.addPackage("ua.com.integrity.bpm.camunda.study.service.decomission");
        war.addAsLibraries(testDependencies);
        return war;
    }

    @Before
    public void setUp() throws Exception {
    }

    @EJB
    EquipmentDecommissionNotificationService equipmentDecommissionNotificationService;

    @EJB
    NotificationTestHolder notificationTestHolder;

    @Test
    public void test_createDecommissioningNotification() throws InterruptedException {

        Set<String> expectedSnForDecommision = Stream.of(
                "3001SN1","3001SN11","3002SN1","3002SN2","3002SN5",
                "3003SN1","3003SN2","3003SN3","3003SN4","3004SN1",
                "3004SN2","3005SN3","3006SN1","3007SN1","3010SN1","3010SN2","3010SN3",
                "3010SN4","3010SN5","3010SN6","3010SN7","3005SN1"
        ).collect(Collectors.toSet());

        equipmentDecommissionNotificationService.createDecommissioningNotification();
        TimeUnit.SECONDS.sleep(3);
        assertThat(notificationTestHolder.getNotificationList())
                .hasSize(22);
    }
}