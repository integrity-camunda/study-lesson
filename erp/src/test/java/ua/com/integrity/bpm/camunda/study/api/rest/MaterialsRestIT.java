package ua.com.integrity.bpm.camunda.study.api.rest;

import io.restassured.RestAssured;
import org.assertj.core.api.Assertions;
import org.hamcrest.Matchers;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.importer.ZipImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import ua.com.integrity.bpm.camunda.study.dto.api.BatchWriteOffRequestDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.BatchOfMaterialDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.ConsumptionRateDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.MaterialDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.MeasureUnit;

import javax.ws.rs.core.MediaType;
import java.io.File;
import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class MaterialsRestIT {

    @Deployment(testable = false)
    public static Archive<?> getEarArchive() {
        // Import the web archive that was created by Maven:
        File f = new File("./target/erp.war");
        if (f.exists() == false) {
            throw new RuntimeException("File " + f.getAbsolutePath() + " does not exist.");
        }
        WebArchive war = ShrinkWrap.create(ZipImporter.class, "org-structure-rest-erp-test.war").importFrom(f).as(WebArchive.class);
        war.addPackage("ua.com.integrity.bpm.camunda.study.api.rest");
        return war;
    }

    @ArquillianResource
    URL applicationUrl;

    String basePath;

    @Before
    public void setUp() throws Exception {
        basePath = applicationUrl.toString().concat("api/rest/material/");
    }

    @Test
    public void test_addMaterial() {
        long materialId = RestAssured.with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(MaterialDto.builder().title("TEST material").measureUnit(MeasureUnit.PCS).build())
                .when()
                .post(basePath)
                .then()
                .statusCode(200)
                .extract().jsonPath().getLong("id");
        assertThat(materialId).isGreaterThanOrEqualTo(99000L);
    }

    @Test
    public void test_addMaterial_invalid_body() {
        List errors = RestAssured.with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(MaterialDto.builder().title("TEST material").build())
                .when()
                .post(basePath)
                .then()
                .statusCode(400)
                .extract().as(List.class);
        assertThat(errors).hasSize(1);
    }

    @Test
    public void test_allMaterials() {
        List<Long> ids = RestAssured.when()
                .get(basePath)
                .then()
                .statusCode(200)
                .extract().jsonPath().getList("id", Long.class);
        assertThat(ids).contains(51L, 52L, 53L, 54L, 55L, 56L, 57L, 58L, 59L, 60L);
    }

    @Test
    public void test_getBatch() {
        RestAssured.when()
                .get(basePath + "batch/511")
                .then()
                .statusCode(200)
                .body("id", is(511))
                .body("material.id", is(51))
                .body("purchaseDate", is("2020-05-25"))
                .body("purchaseAmount", is(1000))
                .body("remainingAmount", is(202))
                .body("price", is(88.5F));
    }

    @Test
    public void test_getBatch_unknown_id() {
        RestAssured.when()
                .get(basePath + "batch/545845545")
                .then()
                .statusCode(404);
    }

    @Test
    public void test_getAllBatches() {
        List batches = RestAssured.when()
                .get(basePath + "batch")
                .then()
                .statusCode(200)
                .extract().as(List.class);
        assertThat(batches).hasSizeGreaterThanOrEqualTo(10);
    }

    @Test
    public void test_getAllBatches_by_material_52() {
        List<Long> batchIds = RestAssured.with()
                .queryParam("material", 52)
                .when()
                .get(basePath + "batch")
                .then()
                .statusCode(200)
                .extract().jsonPath().getList("id", Long.class);
        assertThat(batchIds).containsOnly(521L, 522L);
    }

    @Test
    public void test_addNewBatch() {
        long batchId = RestAssured.with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(BatchOfMaterialDto.builder().material(MaterialDto.builder().id(60L).build())
                        .price(new BigDecimal("115.50")).purchaseAmount(new BigDecimal("24"))
                        .purchaseDate(LocalDate.parse("2021-01-01")).build()
                )
                .when()
                .post(basePath + "batch")
                .then()
                .statusCode(200)
                .extract().jsonPath().getLong("id");
        assertThat(batchId).isGreaterThanOrEqualTo(166000L);
    }

    @Test
    public void test_addNewBatch_unknown_material() {
        RestAssured.with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(BatchOfMaterialDto.builder().material(MaterialDto.builder().id(45454L).build())
                        .purchaseDate(LocalDate.parse("2021-01-01")).price(new BigDecimal("115.50")).purchaseAmount(new BigDecimal("24")).build()
                )
                .when()
                .post(basePath + "batch")
                .then()
                .statusCode(400)
                .body("message", is("No such material"));
    }

    @Test
    public void test_addNewBatch_invalid_body() {
        List errors = RestAssured.with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(BatchOfMaterialDto.builder().purchaseDate(LocalDate.parse("2021-01-01"))
                        .price(new BigDecimal("115.50")).purchaseAmount(new BigDecimal("24")).build()
                )
                .when()
                .post(basePath + "batch")
                .then()
                .statusCode(400)
                .extract().as(List.class);
        assertThat(errors).hasSize(1);
    }

    @Test
    public void test_deleteBatch() {
        RestAssured.when()
                .delete(basePath + "batch/591")
                .then()
                .statusCode(204);
    }

    @Test
    public void test_writeOffFromBatch() {
        RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(new BatchWriteOffRequestDto(611L, "5.4"))
                .when()
                .patch(basePath + "batch/611")
                .then()
                .statusCode(204);
        RestAssured.when()
                .get(basePath + "batch/611")
                .then()
                .statusCode(200)
                .body("remainingAmount", is(100.0F));
    }

    @Test
    public void test_getRate() {
        RestAssured.when()
                .get(basePath + "consumption-rate/1051")
                .then()
                .statusCode(200)
                .body("material.id", is(51))
                .body("durationUnit", is("WEEKS"))
                .body("amount", is(55));
    }

    @Test
    public void test_getRate_unknown_id() {
        RestAssured.when()
                .get(basePath + "consumption-rate/1240933")
                .then()
                .statusCode(404);
    }

    @Test
    public void test_getAllRates() {
        List rates = RestAssured.when()
                .get(basePath + "consumption-rate")
                .then()
                .statusCode(200)
                .extract().as(List.class);
        assertThat(rates).hasSizeGreaterThanOrEqualTo(12);
    }

    @Test
    public void test_getAllRates_by_material() {
        List<Long> ratesId = RestAssured
                .with()
                .queryParam("material", 51)
                .when()
                .get(basePath + "consumption-rate")
                .then()
                .statusCode(200)
                .extract().jsonPath().getList("id", Long.class);
        assertThat(ratesId).containsOnly(1051L);
    }

    @Test
    public void test_newRate() {
        long id = RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(ConsumptionRateDto.builder().material(MaterialDto.builder().id(55L).build())
                        .title("Test consumption rate").build()
                )
                .when()
                .post(basePath + "consumption-rate")
                .then()
                .statusCode(200)
                .extract().jsonPath().getLong("id");
        assertThat(id).isGreaterThanOrEqualTo(22000L);

    }

    @Test
    public void test_newRate_unknown_material() {
        RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(ConsumptionRateDto.builder().material(MaterialDto.builder().id(565464L).build())
                        .title("Test consumption rate").build()
                )
                .when()
                .post(basePath + "consumption-rate")
                .then()
                .statusCode(400)
                .body("message", is("No such material"));

    }

    @Test
    public void test_newRate_invalid_body() {
        List errors = RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(ConsumptionRateDto.builder()
                        .title("Test consumption rate").build()
                )
                .when()
                .post(basePath + "consumption-rate")
                .then()
                .statusCode(400)
                .extract().as(List.class);
        assertThat(errors).hasSize(1);

    }

    @Test
    public void test_updateRate() {
        RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(ConsumptionRateDto.builder().id(1060L).material(MaterialDto.builder().id(60L).build())
                        .title("New updated rate").amount(new BigDecimal("0.33")).build()
                )
                .when()
                .put(basePath + "consumption-rate/1060")
                .then()
                .statusCode(204);
        RestAssured.when()
                .get(basePath + "consumption-rate/1060")
                .then()
                .body("amount", is(0.33F));
    }

    @Test
    public void test_updateRate_mismatch_id() {
        RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(ConsumptionRateDto.builder().id(1025L).material(MaterialDto.builder().id(60L).build())
                        .title("New updated rate").amount(new BigDecimal("0.33")).build()
                )
                .when()
                .put(basePath + "consumption-rate/1060")
                .then()
                .statusCode(400)
                .body("message", is("ConsumptionRate id not math with request body"));
    }

    @Test
    public void test_updateRate_unknown_rate_id() {
        RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(ConsumptionRateDto.builder().id(125125L).material(MaterialDto.builder().id(60L).build())
                        .title("New updated rate").amount(new BigDecimal("0.33")).build()
                )
                .when()
                .put(basePath + "consumption-rate/125125")
                .then()
                .statusCode(400)
                .body("message", is("Cannot find consumption rate with id 125125"));
    }

    @Test
    public void test_updateRate_unknown_material_id() {
        RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(ConsumptionRateDto.builder().id(1060L).material(MaterialDto.builder().id(1231233L).build())
                        .title("New updated rate").amount(new BigDecimal("0.33")).build()
                )
                .when()
                .put(basePath + "consumption-rate/1060")
                .then()
                .statusCode(400)
                .body("message", is("No such material"));
    }

    @Test
    public void test_updateRate_invalid_body() {
        List errors = RestAssured
                .with()
                .contentType(MediaType.APPLICATION_JSON)
                .body(ConsumptionRateDto.builder().id(1060L)
                        .title("New updated rate").amount(new BigDecimal("0.33")).build()
                )
                .when()
                .put(basePath + "consumption-rate/1060")
                .then()
                .statusCode(400)
                .extract().as(List.class);
        assertThat(errors).hasSize(1);
    }

//    @Test
//    public void test_deleteRate() {
//        RestAssured.when()
//                .delete(basePath + "consumption-rate/1062")
//                .then().statusCode(204);
//        RestAssured.when()
//                .get(basePath + "consumption-rate/1062")
//                .then().statusCode(404);
//    }
}