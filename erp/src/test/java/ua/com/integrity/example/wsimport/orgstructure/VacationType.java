
package ua.com.integrity.example.wsimport.orgstructure;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for vacationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="vacationType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ANNUALLY"/>
 *     &lt;enumeration value="ADDITIONAL"/>
 *     &lt;enumeration value="AT-OWN-EXPENSE"/>
 *     &lt;enumeration value="FOR-FAMILY-REASONS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "vacationType")
@XmlEnum
public enum VacationType {

    ANNUALLY("ANNUALLY"),
    ADDITIONAL("ADDITIONAL"),
    @XmlEnumValue("AT-OWN-EXPENSE")
    AT_OWN_EXPENSE("AT-OWN-EXPENSE"),
    @XmlEnumValue("FOR-FAMILY-REASONS")
    FOR_FAMILY_REASONS("FOR-FAMILY-REASONS");
    private final String value;

    VacationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VacationType fromValue(String v) {
        for (VacationType c: VacationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
