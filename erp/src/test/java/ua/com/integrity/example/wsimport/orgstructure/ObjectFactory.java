
package ua.com.integrity.example.wsimport.orgstructure;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ua.com.integrity.example.wsimport package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _NewVacation_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "new-vacation");
    private final static QName _GetPosition_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "get-position");
    private final static QName _CreateUnitResponse_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "create-unitResponse");
    private final static QName _GetPositionResponse_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "get-positionResponse");
    private final static QName _RemoveSupplyRateFromPositionResponse_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "remove-supply-rate-from-positionResponse");
    private final static QName _UpdateEmployeeResponse_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "update-employeeResponse");
    private final static QName _NewVacationResponse_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "new-vacationResponse");
    private final static QName _AddSupplyRateToPositionResponse_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "add-supply-rate-to-positionResponse");
    private final static QName _GetEmployeeResponse_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "get-employeeResponse");
    private final static QName _GetUnits_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "get-units");
    private final static QName _Vacation_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "vacation");
    private final static QName _GetAllVacations_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "get-all-vacations");
    private final static QName _GetAllEmployeesResponse_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "get-all-employeesResponse");
    private final static QName _CancelVacationResponse_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "cancel-vacationResponse");
    private final static QName _AllPositions_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "all-positions");
    private final static QName _AllPositionsResponse_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "all-positionsResponse");
    private final static QName _Employee_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "employee");
    private final static QName _GetUnitResponse_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "get-unitResponse");
    private final static QName _CreatePositionResponse_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "create-positionResponse");
    private final static QName _CreatePosition_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "create-position");
    private final static QName _GetUnit_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "get-unit");
    private final static QName _CancelVacation_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "cancel-vacation");
    private final static QName _GetAllVacationsResponse_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "get-all-vacationsResponse");
    private final static QName _GetUnitsResponse_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "get-unitsResponse");
    private final static QName _Unit_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "unit");
    private final static QName _NewEmployeeResponse_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "new-employeeResponse");
    private final static QName _RemoveSupplyRateFromPosition_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "remove-supply-rate-from-position");
    private final static QName _Position_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "position");
    private final static QName _AddSupplyRateToPosition_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "add-supply-rate-to-position");
    private final static QName _NewEmployee_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "new-employee");
    private final static QName _GetAllEmployees_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "get-all-employees");
    private final static QName _GetEmployee_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "get-employee");
    private final static QName _CreateUnit_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "create-unit");
    private final static QName _UpdateEmployee_QNAME = new QName("http://ws.api.study.camunda.bpm.integrity.com.ua/", "update-employee");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ua.com.integrity.example.wsimport
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PositionDto }
     *
     */
    public PositionDto createPositionDto() {
        return new PositionDto();
    }

    /**
     * Create an instance of {@link OrgUnitDto }
     *
     */
    public OrgUnitDto createOrgUnitDto() {
        return new OrgUnitDto();
    }

    /**
     * Create an instance of {@link GetAllEmployeesResponse }
     *
     */
    public GetAllEmployeesResponse createGetAllEmployeesResponse() {
        return new GetAllEmployeesResponse();
    }

    /**
     * Create an instance of {@link CancelVacationResponse }
     *
     */
    public CancelVacationResponse createCancelVacationResponse() {
        return new CancelVacationResponse();
    }

    /**
     * Create an instance of {@link AllPositions }
     *
     */
    public AllPositions createAllPositions() {
        return new AllPositions();
    }

    /**
     * Create an instance of {@link AllPositionsResponse }
     *
     */
    public AllPositionsResponse createAllPositionsResponse() {
        return new AllPositionsResponse();
    }

    /**
     * Create an instance of {@link CreatePosition }
     *
     */
    public CreatePosition createCreatePosition() {
        return new CreatePosition();
    }

    /**
     * Create an instance of {@link GetUnit }
     *
     */
    public GetUnit createGetUnit() {
        return new GetUnit();
    }

    /**
     * Create an instance of {@link EmployeeDto }
     *
     */
    public EmployeeDto createEmployeeDto() {
        return new EmployeeDto();
    }

    /**
     * Create an instance of {@link GetUnitResponse }
     *
     */
    public GetUnitResponse createGetUnitResponse() {
        return new GetUnitResponse();
    }

    /**
     * Create an instance of {@link CreatePositionResponse }
     *
     */
    public CreatePositionResponse createCreatePositionResponse() {
        return new CreatePositionResponse();
    }

    /**
     * Create an instance of {@link NewVacation }
     *
     */
    public NewVacation createNewVacation() {
        return new NewVacation();
    }

    /**
     * Create an instance of {@link GetPosition }
     *
     */
    public GetPosition createGetPosition() {
        return new GetPosition();
    }

    /**
     * Create an instance of {@link CreateUnitResponse }
     *
     */
    public CreateUnitResponse createCreateUnitResponse() {
        return new CreateUnitResponse();
    }

    /**
     * Create an instance of {@link AddSupplyRateToPositionResponse }
     *
     */
    public AddSupplyRateToPositionResponse createAddSupplyRateToPositionResponse() {
        return new AddSupplyRateToPositionResponse();
    }

    /**
     * Create an instance of {@link GetEmployeeResponse }
     *
     */
    public GetEmployeeResponse createGetEmployeeResponse() {
        return new GetEmployeeResponse();
    }

    /**
     * Create an instance of {@link GetUnits }
     *
     */
    public GetUnits createGetUnits() {
        return new GetUnits();
    }

    /**
     * Create an instance of {@link VacationDto }
     *
     */
    public VacationDto createVacationDto() {
        return new VacationDto();
    }

    /**
     * Create an instance of {@link GetAllVacations }
     *
     */
    public GetAllVacations createGetAllVacations() {
        return new GetAllVacations();
    }

    /**
     * Create an instance of {@link GetPositionResponse }
     *
     */
    public GetPositionResponse createGetPositionResponse() {
        return new GetPositionResponse();
    }

    /**
     * Create an instance of {@link RemoveSupplyRateFromPositionResponse }
     *
     */
    public RemoveSupplyRateFromPositionResponse createRemoveSupplyRateFromPositionResponse() {
        return new RemoveSupplyRateFromPositionResponse();
    }

    /**
     * Create an instance of {@link UpdateEmployeeResponse }
     *
     */
    public UpdateEmployeeResponse createUpdateEmployeeResponse() {
        return new UpdateEmployeeResponse();
    }

    /**
     * Create an instance of {@link NewVacationResponse }
     *
     */
    public NewVacationResponse createNewVacationResponse() {
        return new NewVacationResponse();
    }

    /**
     * Create an instance of {@link GetAllEmployees }
     *
     */
    public GetAllEmployees createGetAllEmployees() {
        return new GetAllEmployees();
    }

    /**
     * Create an instance of {@link GetEmployee }
     *
     */
    public GetEmployee createGetEmployee() {
        return new GetEmployee();
    }

    /**
     * Create an instance of {@link CreateUnit }
     *
     */
    public CreateUnit createCreateUnit() {
        return new CreateUnit();
    }

    /**
     * Create an instance of {@link UpdateEmployee }
     *
     */
    public UpdateEmployee createUpdateEmployee() {
        return new UpdateEmployee();
    }

    /**
     * Create an instance of {@link AddSupplyRateToPosition }
     *
     */
    public AddSupplyRateToPosition createAddSupplyRateToPosition() {
        return new AddSupplyRateToPosition();
    }

    /**
     * Create an instance of {@link NewEmployee }
     *
     */
    public NewEmployee createNewEmployee() {
        return new NewEmployee();
    }

    /**
     * Create an instance of {@link GetUnitsResponse }
     *
     */
    public GetUnitsResponse createGetUnitsResponse() {
        return new GetUnitsResponse();
    }

    /**
     * Create an instance of {@link CancelVacation }
     *
     */
    public CancelVacation createCancelVacation() {
        return new CancelVacation();
    }

    /**
     * Create an instance of {@link GetAllVacationsResponse }
     *
     */
    public GetAllVacationsResponse createGetAllVacationsResponse() {
        return new GetAllVacationsResponse();
    }

    /**
     * Create an instance of {@link RemoveSupplyRateFromPosition }
     *
     */
    public RemoveSupplyRateFromPosition createRemoveSupplyRateFromPosition() {
        return new RemoveSupplyRateFromPosition();
    }

    /**
     * Create an instance of {@link NewEmployeeResponse }
     *
     */
    public NewEmployeeResponse createNewEmployeeResponse() {
        return new NewEmployeeResponse();
    }

    /**
     * Create an instance of {@link LocalDate }
     *
     */
    public LocalDate createLocalDate() {
        return new LocalDate();
    }

    /**
     * Create an instance of {@link PositionDto.SupplyRates }
     *
     */
    public PositionDto.SupplyRates createPositionDtoSupplyRates() {
        return new PositionDto.SupplyRates();
    }

    /**
     * Create an instance of {@link OrgUnitDto.DescendantUnits }
     *
     */
    public OrgUnitDto.DescendantUnits createOrgUnitDtoDescendantUnits() {
        return new OrgUnitDto.DescendantUnits();
    }

    /**
     * Create an instance of {@link OrgUnitDto.Positions }
     *
     */
    public OrgUnitDto.Positions createOrgUnitDtoPositions() {
        return new OrgUnitDto.Positions();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NewVacation }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "new-vacation")
    public JAXBElement<NewVacation> createNewVacation(NewVacation value) {
        return new JAXBElement<NewVacation>(_NewVacation_QNAME, NewVacation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPosition }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "get-position")
    public JAXBElement<GetPosition> createGetPosition(GetPosition value) {
        return new JAXBElement<GetPosition>(_GetPosition_QNAME, GetPosition.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUnitResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "create-unitResponse")
    public JAXBElement<CreateUnitResponse> createCreateUnitResponse(CreateUnitResponse value) {
        return new JAXBElement<CreateUnitResponse>(_CreateUnitResponse_QNAME, CreateUnitResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPositionResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "get-positionResponse")
    public JAXBElement<GetPositionResponse> createGetPositionResponse(GetPositionResponse value) {
        return new JAXBElement<GetPositionResponse>(_GetPositionResponse_QNAME, GetPositionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveSupplyRateFromPositionResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "remove-supply-rate-from-positionResponse")
    public JAXBElement<RemoveSupplyRateFromPositionResponse> createRemoveSupplyRateFromPositionResponse(RemoveSupplyRateFromPositionResponse value) {
        return new JAXBElement<RemoveSupplyRateFromPositionResponse>(_RemoveSupplyRateFromPositionResponse_QNAME, RemoveSupplyRateFromPositionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateEmployeeResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "update-employeeResponse")
    public JAXBElement<UpdateEmployeeResponse> createUpdateEmployeeResponse(UpdateEmployeeResponse value) {
        return new JAXBElement<UpdateEmployeeResponse>(_UpdateEmployeeResponse_QNAME, UpdateEmployeeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NewVacationResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "new-vacationResponse")
    public JAXBElement<NewVacationResponse> createNewVacationResponse(NewVacationResponse value) {
        return new JAXBElement<NewVacationResponse>(_NewVacationResponse_QNAME, NewVacationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddSupplyRateToPositionResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "add-supply-rate-to-positionResponse")
    public JAXBElement<AddSupplyRateToPositionResponse> createAddSupplyRateToPositionResponse(AddSupplyRateToPositionResponse value) {
        return new JAXBElement<AddSupplyRateToPositionResponse>(_AddSupplyRateToPositionResponse_QNAME, AddSupplyRateToPositionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEmployeeResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "get-employeeResponse")
    public JAXBElement<GetEmployeeResponse> createGetEmployeeResponse(GetEmployeeResponse value) {
        return new JAXBElement<GetEmployeeResponse>(_GetEmployeeResponse_QNAME, GetEmployeeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUnits }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "get-units")
    public JAXBElement<GetUnits> createGetUnits(GetUnits value) {
        return new JAXBElement<GetUnits>(_GetUnits_QNAME, GetUnits.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VacationDto }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "vacation")
    public JAXBElement<VacationDto> createVacation(VacationDto value) {
        return new JAXBElement<VacationDto>(_Vacation_QNAME, VacationDto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllVacations }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "get-all-vacations")
    public JAXBElement<GetAllVacations> createGetAllVacations(GetAllVacations value) {
        return new JAXBElement<GetAllVacations>(_GetAllVacations_QNAME, GetAllVacations.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllEmployeesResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "get-all-employeesResponse")
    public JAXBElement<GetAllEmployeesResponse> createGetAllEmployeesResponse(GetAllEmployeesResponse value) {
        return new JAXBElement<GetAllEmployeesResponse>(_GetAllEmployeesResponse_QNAME, GetAllEmployeesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelVacationResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "cancel-vacationResponse")
    public JAXBElement<CancelVacationResponse> createCancelVacationResponse(CancelVacationResponse value) {
        return new JAXBElement<CancelVacationResponse>(_CancelVacationResponse_QNAME, CancelVacationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AllPositions }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "all-positions")
    public JAXBElement<AllPositions> createAllPositions(AllPositions value) {
        return new JAXBElement<AllPositions>(_AllPositions_QNAME, AllPositions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AllPositionsResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "all-positionsResponse")
    public JAXBElement<AllPositionsResponse> createAllPositionsResponse(AllPositionsResponse value) {
        return new JAXBElement<AllPositionsResponse>(_AllPositionsResponse_QNAME, AllPositionsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmployeeDto }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "employee")
    public JAXBElement<EmployeeDto> createEmployee(EmployeeDto value) {
        return new JAXBElement<EmployeeDto>(_Employee_QNAME, EmployeeDto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUnitResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "get-unitResponse")
    public JAXBElement<GetUnitResponse> createGetUnitResponse(GetUnitResponse value) {
        return new JAXBElement<GetUnitResponse>(_GetUnitResponse_QNAME, GetUnitResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreatePositionResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "create-positionResponse")
    public JAXBElement<CreatePositionResponse> createCreatePositionResponse(CreatePositionResponse value) {
        return new JAXBElement<CreatePositionResponse>(_CreatePositionResponse_QNAME, CreatePositionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreatePosition }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "create-position")
    public JAXBElement<CreatePosition> createCreatePosition(CreatePosition value) {
        return new JAXBElement<CreatePosition>(_CreatePosition_QNAME, CreatePosition.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUnit }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "get-unit")
    public JAXBElement<GetUnit> createGetUnit(GetUnit value) {
        return new JAXBElement<GetUnit>(_GetUnit_QNAME, GetUnit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelVacation }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "cancel-vacation")
    public JAXBElement<CancelVacation> createCancelVacation(CancelVacation value) {
        return new JAXBElement<CancelVacation>(_CancelVacation_QNAME, CancelVacation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllVacationsResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "get-all-vacationsResponse")
    public JAXBElement<GetAllVacationsResponse> createGetAllVacationsResponse(GetAllVacationsResponse value) {
        return new JAXBElement<GetAllVacationsResponse>(_GetAllVacationsResponse_QNAME, GetAllVacationsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUnitsResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "get-unitsResponse")
    public JAXBElement<GetUnitsResponse> createGetUnitsResponse(GetUnitsResponse value) {
        return new JAXBElement<GetUnitsResponse>(_GetUnitsResponse_QNAME, GetUnitsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrgUnitDto }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "unit")
    public JAXBElement<OrgUnitDto> createUnit(OrgUnitDto value) {
        return new JAXBElement<OrgUnitDto>(_Unit_QNAME, OrgUnitDto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NewEmployeeResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "new-employeeResponse")
    public JAXBElement<NewEmployeeResponse> createNewEmployeeResponse(NewEmployeeResponse value) {
        return new JAXBElement<NewEmployeeResponse>(_NewEmployeeResponse_QNAME, NewEmployeeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveSupplyRateFromPosition }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "remove-supply-rate-from-position")
    public JAXBElement<RemoveSupplyRateFromPosition> createRemoveSupplyRateFromPosition(RemoveSupplyRateFromPosition value) {
        return new JAXBElement<RemoveSupplyRateFromPosition>(_RemoveSupplyRateFromPosition_QNAME, RemoveSupplyRateFromPosition.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PositionDto }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "position")
    public JAXBElement<PositionDto> createPosition(PositionDto value) {
        return new JAXBElement<PositionDto>(_Position_QNAME, PositionDto.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddSupplyRateToPosition }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "add-supply-rate-to-position")
    public JAXBElement<AddSupplyRateToPosition> createAddSupplyRateToPosition(AddSupplyRateToPosition value) {
        return new JAXBElement<AddSupplyRateToPosition>(_AddSupplyRateToPosition_QNAME, AddSupplyRateToPosition.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NewEmployee }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "new-employee")
    public JAXBElement<NewEmployee> createNewEmployee(NewEmployee value) {
        return new JAXBElement<NewEmployee>(_NewEmployee_QNAME, NewEmployee.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllEmployees }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "get-all-employees")
    public JAXBElement<GetAllEmployees> createGetAllEmployees(GetAllEmployees value) {
        return new JAXBElement<GetAllEmployees>(_GetAllEmployees_QNAME, GetAllEmployees.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEmployee }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "get-employee")
    public JAXBElement<GetEmployee> createGetEmployee(GetEmployee value) {
        return new JAXBElement<GetEmployee>(_GetEmployee_QNAME, GetEmployee.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUnit }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "create-unit")
    public JAXBElement<CreateUnit> createCreateUnit(CreateUnit value) {
        return new JAXBElement<CreateUnit>(_CreateUnit_QNAME, CreateUnit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateEmployee }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://ws.api.study.camunda.bpm.integrity.com.ua/", name = "update-employee")
    public JAXBElement<UpdateEmployee> createUpdateEmployee(UpdateEmployee value) {
        return new JAXBElement<UpdateEmployee>(_UpdateEmployee_QNAME, UpdateEmployee.class, null, value);
    }

}
