
package ua.com.integrity.example.wsimport.orgstructure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for create-position complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="create-position">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="position-data" type="{http://ws.api.study.camunda.bpm.integrity.com.ua/}positionDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "create-position", propOrder = {
    "positionData"
})
public class CreatePosition {

    @XmlElement(name = "position-data")
    protected PositionDto positionData;

    /**
     * Gets the value of the positionData property.
     * 
     * @return
     *     possible object is
     *     {@link PositionDto }
     *     
     */
    public PositionDto getPositionData() {
        return positionData;
    }

    /**
     * Sets the value of the positionData property.
     * 
     * @param value
     *     allowed object is
     *     {@link PositionDto }
     *     
     */
    public void setPositionData(PositionDto value) {
        this.positionData = value;
    }

}
