
package ua.com.integrity.example.wsimport.orgstructure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for create-unitResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="create-unitResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="unit" type="{http://ws.api.study.camunda.bpm.integrity.com.ua/}orgUnitDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "create-unitResponse", propOrder = {
    "unit"
})
public class CreateUnitResponse {

    protected OrgUnitDto unit;

    /**
     * Gets the value of the unit property.
     *
     * @return
     *     possible object is
     *     {@link OrgUnitDto }
     *
     */
    public OrgUnitDto getUnit() {
        return unit;
    }

    /**
     * Sets the value of the unit property.
     *
     * @param value
     *     allowed object is
     *     {@link OrgUnitDto }
     *     
     */
    public void setUnit(OrgUnitDto value) {
        this.unit = value;
    }

}
