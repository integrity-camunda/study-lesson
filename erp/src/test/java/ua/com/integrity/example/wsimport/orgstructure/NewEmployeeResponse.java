
package ua.com.integrity.example.wsimport.orgstructure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for new-employeeResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="new-employeeResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="employee" type="{http://ws.api.study.camunda.bpm.integrity.com.ua/}employeeDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "new-employeeResponse", propOrder = {
    "employee"
})
public class NewEmployeeResponse {

    protected EmployeeDto employee;

    /**
     * Gets the value of the employee property.
     *
     * @return
     *     possible object is
     *     {@link EmployeeDto }
     *
     */
    public EmployeeDto getEmployee() {
        return employee;
    }

    /**
     * Sets the value of the employee property.
     *
     * @param value
     *     allowed object is
     *     {@link EmployeeDto }
     *     
     */
    public void setEmployee(EmployeeDto value) {
        this.employee = value;
    }

}
