
package ua.com.integrity.example.wsimport.orgstructure;

import javax.xml.bind.annotation.*;


/**
 * <p>Java class for vacationDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="vacationDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="employee" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="start-date" type="{http://ws.api.study.camunda.bpm.integrity.com.ua/}localDate"/>
 *         &lt;element name="duration" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="type" type="{http://ws.api.study.camunda.bpm.integrity.com.ua/}vacationType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}long" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "vacationDto", propOrder = {
    "employee",
    "startDate",
    "duration",
    "type"
})
public class VacationDto {

    protected long employee;
    @XmlElement(name = "start-date", required = true)
    protected LocalDate startDate;
    protected int duration;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected VacationType type;
    @XmlAttribute(name = "id")
    protected Long id;

    /**
     * Gets the value of the employee property.
     * 
     */
    public long getEmployee() {
        return employee;
    }

    /**
     * Sets the value of the employee property.
     * 
     */
    public void setEmployee(long value) {
        this.employee = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link LocalDate }
     *     
     */
    public LocalDate getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocalDate }
     *     
     */
    public void setStartDate(LocalDate value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the duration property.
     * 
     */
    public int getDuration() {
        return duration;
    }

    /**
     * Sets the value of the duration property.
     * 
     */
    public void setDuration(int value) {
        this.duration = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link VacationType }
     *     
     */
    public VacationType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link VacationType }
     *     
     */
    public void setType(VacationType value) {
        this.type = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setId(Long value) {
        this.id = value;
    }

}
