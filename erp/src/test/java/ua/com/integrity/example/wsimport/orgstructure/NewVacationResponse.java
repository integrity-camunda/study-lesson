
package ua.com.integrity.example.wsimport.orgstructure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for new-vacationResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="new-vacationResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="vacation" type="{http://ws.api.study.camunda.bpm.integrity.com.ua/}vacationDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "new-vacationResponse", propOrder = {
    "vacation"
})
public class NewVacationResponse {

    protected VacationDto vacation;

    /**
     * Gets the value of the vacation property.
     *
     * @return
     *     possible object is
     *     {@link VacationDto }
     *
     */
    public VacationDto getVacation() {
        return vacation;
    }

    /**
     * Sets the value of the vacation property.
     *
     * @param value
     *     allowed object is
     *     {@link VacationDto }
     *     
     */
    public void setVacation(VacationDto value) {
        this.vacation = value;
    }

}
