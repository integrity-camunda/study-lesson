
package ua.com.integrity.example.wsimport.orgstructure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for new-employee complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="new-employee">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="employee-data" type="{http://ws.api.study.camunda.bpm.integrity.com.ua/}employeeDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "new-employee", propOrder = {
    "employeeData"
})
public class NewEmployee {

    @XmlElement(name = "employee-data")
    protected EmployeeDto employeeData;

    /**
     * Gets the value of the employeeData property.
     *
     * @return
     *     possible object is
     *     {@link EmployeeDto }
     *
     */
    public EmployeeDto getEmployeeData() {
        return employeeData;
    }

    /**
     * Sets the value of the employeeData property.
     *
     * @param value
     *     allowed object is
     *     {@link EmployeeDto }
     *     
     */
    public void setEmployeeData(EmployeeDto value) {
        this.employeeData = value;
    }

}
