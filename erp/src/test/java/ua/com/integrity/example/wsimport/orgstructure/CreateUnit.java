
package ua.com.integrity.example.wsimport.orgstructure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for create-unit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="create-unit">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="unit-data" type="{http://ws.api.study.camunda.bpm.integrity.com.ua/}orgUnitDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "create-unit", propOrder = {
    "unitData"
})
public class CreateUnit {

    @XmlElement(name = "unit-data")
    protected OrgUnitDto unitData;

    /**
     * Gets the value of the unitData property.
     *
     * @return
     *     possible object is
     *     {@link OrgUnitDto }
     *
     */
    public OrgUnitDto getUnitData() {
        return unitData;
    }

    /**
     * Sets the value of the unitData property.
     *
     * @param value
     *     allowed object is
     *     {@link OrgUnitDto }
     *     
     */
    public void setUnitData(OrgUnitDto value) {
        this.unitData = value;
    }

}
