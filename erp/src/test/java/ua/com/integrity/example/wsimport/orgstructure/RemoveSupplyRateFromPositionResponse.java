
package ua.com.integrity.example.wsimport.orgstructure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for remove-supply-rate-from-positionResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="remove-supply-rate-from-positionResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="position" type="{http://ws.api.study.camunda.bpm.integrity.com.ua/}positionDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "remove-supply-rate-from-positionResponse", propOrder = {
    "position"
})
public class RemoveSupplyRateFromPositionResponse {

    protected PositionDto position;

    /**
     * Gets the value of the position property.
     *
     * @return
     *     possible object is
     *     {@link PositionDto }
     *
     */
    public PositionDto getPosition() {
        return position;
    }

    /**
     * Sets the value of the position property.
     *
     * @param value
     *     allowed object is
     *     {@link PositionDto }
     *     
     */
    public void setPosition(PositionDto value) {
        this.position = value;
    }

}
