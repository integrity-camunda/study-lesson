
package ua.com.integrity.example.wsimport.orgstructure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for get-all-vacationsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="get-all-vacationsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="vacations" type="{http://ws.api.study.camunda.bpm.integrity.com.ua/}vacationDto" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "get-all-vacationsResponse", propOrder = {
    "vacations"
})
public class GetAllVacationsResponse {

    protected List<VacationDto> vacations;

    /**
     * Gets the value of the vacations property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the vacations property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVacations().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VacationDto }
     *
     *
     */
    public List<VacationDto> getVacations() {
        if (vacations == null) {
            vacations = new ArrayList<VacationDto>();
        }
        return this.vacations;
    }

}
