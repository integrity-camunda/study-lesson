
package ua.com.integrity.example.wsimport.orgstructure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for new-vacation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="new-vacation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="vacation-data" type="{http://ws.api.study.camunda.bpm.integrity.com.ua/}vacationDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "new-vacation", propOrder = {
    "vacationData"
})
public class NewVacation {

    @XmlElement(name = "vacation-data")
    protected VacationDto vacationData;

    /**
     * Gets the value of the vacationData property.
     *
     * @return
     *     possible object is
     *     {@link VacationDto }
     *
     */
    public VacationDto getVacationData() {
        return vacationData;
    }

    /**
     * Sets the value of the vacationData property.
     *
     * @param value
     *     allowed object is
     *     {@link VacationDto }
     *     
     */
    public void setVacationData(VacationDto value) {
        this.vacationData = value;
    }

}
