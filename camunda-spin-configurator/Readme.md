Установка нового модуля на сервер WildFly

1. Собрать проект `mvn clean package`

2. После сборки в папке target появятся сформированный архив с wildfly модулем

3. Распаковать в каталоге modules сервера приложений сформированый архив camunda-spin-configurator-*-wildfly-module.zip. После распаковки на сервере приложений будет создана необходимая структура с содержимым

4. В файле `$WILDFLY_HOME/modules/org/camunda/bpm/camunda-engine-plugin-spin/main/module.xml` 
добавить зависимость на новосозданный модуль `ua.com.integrity.bpm.camunda.camunda-spin-configurator` 
дополнительно указав опцию `services="import"`


