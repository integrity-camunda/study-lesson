/**
 * 
 */
package ua.com.integrity.bpm.camunda.studyexamples.servicetask;

import javax.inject.Named;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/**
 * @author Viktor Ivanchenko, Integrity Vision LLC, Kyiv
 *
 */
@Named
public class JavaDelegateImpl implements JavaDelegate {

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		execution.setVariable("javaDelegate", "value setted in JavaDelegateImpl");

	}

}
