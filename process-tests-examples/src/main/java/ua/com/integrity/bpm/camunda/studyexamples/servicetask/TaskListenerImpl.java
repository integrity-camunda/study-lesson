package ua.com.integrity.bpm.camunda.studyexamples.servicetask;

import javax.inject.Named;

import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;

@Named
public class TaskListenerImpl implements TaskListener {

	@Override
	public void notify(DelegateTask delegateTask) {
		// set variable value
		delegateTask.getExecution().setVariable("taskListener", "value setted in TaskListenerImpl");
	}

}
