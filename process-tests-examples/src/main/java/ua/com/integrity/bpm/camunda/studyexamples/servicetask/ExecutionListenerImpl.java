/**
 * 
 */
package ua.com.integrity.bpm.camunda.studyexamples.servicetask;

import javax.inject.Named;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;

/**
 * @author Viktor Ivanchenko, Integrity Vision LLC, Kyiv
 *
 */
@Named
public class ExecutionListenerImpl implements ExecutionListener {

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		// set variable value
		execution.setVariable("executionListener", "value setted in ExecutionListenerImpl");

	}

}
