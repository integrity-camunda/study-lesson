package ua.com.integrity.bpm.camunda.studyexamples.processMsgEnd;

import org.apache.ibatis.logging.LogFactory;
import org.camunda.bpm.engine.delegate.BpmnError;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.test.mock.Mocks;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.complete;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.execute;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.job;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.runtimeService;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.task;
import static org.camunda.bpm.engine.test.assertions.bpmn.AbstractAssertions.init;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.assertThat;

public class ProcessMsgEndSchema {

    @ClassRule
    @Rule
    public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();

    private static final String PROCESS_DEFINITION_KEY = "studyexamples";

    static {
        LogFactory.useSlf4jLogging(); // MyBatis
    }

    @Before
    public void setup() {
        init(rule.getProcessEngine());
    }


    @Test
    @Deployment(resources = {"process_msg_end.bpmn","process_signal_starting_on_signal_end.bpmn"})
    public void test_end_message_event_behaviour() {
        Map<String, Object> variables = new HashMap<>();
        variables.put("variable", "message");
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("process-end-events-message-escalade-error", variables);
        assertThat(processInstance).isStarted().hasPassed("StartEvent_1kunpsc", "ExclusiveGateway_15nk5cl", "EndEvent_0e4haud");

//		//check that another process is started
//		ProcessInstance processInstanceNew = runtimeService().createProcessInstanceQuery().processDefinitionKey("process_message_startevent_started_by_end_message_event").active().singleResult();
//		assertThat(processInstanceNew).isStarted().isWaitingAt("StartEvent_0xxw4oh");
//		execute(job());
//		assertThat(processInstanceNew).hasPassed("StartEvent_0xxw4oh", "EndEvent_0i2zv3z").isEnded();

    }

    @Test
    @Deployment(resources = {"process_msg_end.bpmn","process_signal_starting_on_signal_end.bpmn"})
    public void test_escalade_end_event_behaviour() {
        Map<String, Object> variables = new HashMap<>();
        variables.put("variable", "escalade");
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("process-end-events-message-escalade-error", variables);
        assertThat(processInstance).isStarted().hasPassedInOrder("StartEvent_1kunpsc", "ExclusiveGateway_15nk5cl",
                "StartEvent_050btoq", "EndEvent_1sxpvdw", "BoundaryEvent_17klovb", "EndEvent_0hwklhz")
                .hasNotPassed("EndEvent_0qstodz").isEnded();

    }

    @Test
    @Deployment(resources = {"process_msg_end.bpmn","process_signal_starting_on_signal_end.bpmn"})
    public void test_error_end_event_behaviour_javaDelegate() {
        Map<String, Object> variables = new HashMap<>();
        variables.put("variable", "error_delegate");
        JavaDelegate javaDelegate = (execution) -> {throw new BpmnError("150");};
        Mocks.register("errorDelegate", javaDelegate);
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("process-end-events-message-escalade-error", variables);
        assertThat(processInstance).isStarted().hasPassedInOrder("StartEvent_1kunpsc", "ExclusiveGateway_15nk5cl",
                "Task_0b6aq3s", "BoundaryEvent_0311me5", "EndEvent_1ijyxza")
                .hasNotPassed("EndEvent_0555wih", "BoundaryEvent_0kr7qtv").isEnded();

    }

    @Test
    @Deployment(resources = {"process_msg_end.bpmn","process_signal_starting_on_signal_end.bpmn"})
    public void test_error_end_event_behaviour_subprocess() {
        Map<String, Object> variables = new HashMap<>();
        variables.put("variable", "error_subprocess");
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("process-end-events-message-escalade-error", variables);
        assertThat(processInstance).isStarted().hasPassedInOrder("StartEvent_1kunpsc", "ExclusiveGateway_15nk5cl",
                "StartEvent_0ym3pdf", "EndEvent_0xdhxz1", "BoundaryEvent_04l0hw9", "EndEvent_1wlpbo4")
                .hasNotPassed("EndEvent_0r87s6v").isEnded();

    }

    @Test
    @Deployment(resources = {"process_msg_end.bpmn","process_signal_starting_on_signal_end.bpmn"})
    public void test_signal_end_event_behaviour() {
        Map<String, Object> variables = new HashMap<>();
        variables.put("variable", "signal");
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("process-end-events-message-escalade-error", variables);
        assertThat(processInstance).isStarted().hasPassedInOrder("StartEvent_1kunpsc", "ExclusiveGateway_15nk5cl",
                "ExclusiveGateway_099wbvb", "StartEvent_0ko18n0", "EndEvent_0fwtmze", "BoundaryEvent_0nojl69", "EndEvent_0cgntd0")
                .hasNotPassed("BoundaryEvent_0n775tf", "EndEvent_1etlh4h").isEnded();

        //check that another process is started
        ProcessInstance processInstanceNew = runtimeService().createProcessInstanceQuery().processDefinitionKey("process_signal_startevent_started_by_end_signal_event").active().singleResult();
        assertThat(processInstanceNew).isStarted().isWaitingAt("StartEvent_0supc28");
        execute(job());
        assertThat(processInstanceNew).hasPassed("StartEvent_0supc28", "EndEvent_0v6z36r").isEnded();

    }

    @Test
    @Deployment(resources = {"process_msg_end.bpmn","process_signal_starting_on_signal_end.bpmn"})
    public void test_compensation_end_event_behaviour() {
        Map<String, Object> variables = new HashMap<>();
        variables.put("variable", "compensation");
        JavaDelegate mainLogick = (execution) -> {};
        Mocks.register("mainLogick", mainLogick);
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("process-end-events-message-escalade-error", variables);

        assertThat(processInstance).isStarted().hasPassedInOrder("StartEvent_1kunpsc", "ExclusiveGateway_15nk5cl",
                "ExclusiveGateway_099wbvb", "ExclusiveGateway_1e2p8du", "StartEvent_0tndxvb",
                "Task_0reltli", "EndEvent_16iv6b5")
                .isWaitingAt("EndEvent_10nbnbf");
        execute(job());
        assertThat(processInstance)
                .hasPassed("Task_155olvd", "Task_01ox5ll", "EndEvent_10nbnbf")
                .hasNotPassed("Task_0u9hpnp").isWaitingAt("Task_0u0r492");

    }

    @Test
    @Deployment(resources = {"process_msg_end.bpmn","process_signal_starting_on_signal_end.bpmn"})
    public void test_compensation_end_event_behaviour_all_compensate() {
        Map<String, Object> variables = new HashMap<>();
        variables.put("variable", "compensation");
        JavaDelegate mainLogick = (execution) -> {};
        Mocks.register("mainLogick", mainLogick);
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("process-end-events-message-escalade-error", variables);

        assertThat(processInstance).isStarted().hasPassed("StartEvent_1kunpsc", "ExclusiveGateway_15nk5cl",
                "ExclusiveGateway_099wbvb", "ExclusiveGateway_1e2p8du", "StartEvent_0tndxvb",
                "Task_0reltli", "EndEvent_16iv6b5")
                .isWaitingAt("EndEvent_10nbnbf");
        complete(task());
        execute(job());
        assertThat(processInstance)
                .hasPassed("Task_155olvd", "Task_01ox5ll", "EndEvent_10nbnbf", "Task_0u9hpnp")
                .isEnded();

    }

}
