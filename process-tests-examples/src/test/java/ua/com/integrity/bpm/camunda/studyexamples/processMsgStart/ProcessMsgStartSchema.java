package ua.com.integrity.bpm.camunda.studyexamples.processMsgStart;

import org.apache.ibatis.logging.LogFactory;
import org.assertj.core.api.Assertions;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.execute;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.job;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.runtimeService;
import static org.camunda.bpm.engine.test.assertions.bpmn.AbstractAssertions.init;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.assertThat;

public class ProcessMsgStartSchema {

    @ClassRule
    @Rule
    public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();

    private static final String PROCESS_DEFINITION_KEY = "studyexamples";

    static {
        LogFactory.useSlf4jLogging(); // MyBatis
    }

    @Before
    public void setup() {
        init(rule.getProcessEngine());
    }



    @Test
    @Deployment(resources = { "process_msg_start.bpmn"})
    public void test_start_events_behavior_message_start() {

        ProcessInstance processInstance = runtimeService().createMessageCorrelation("Message_1jr8gbf")
                .correlateWithResult().getProcessInstance();

        assertThat(processInstance).isStarted()
                .hasPassed("StartEvent_1sy7i8d", "ExclusiveGateway_1rz5jm8", "EndEvent_0g3693o")
                .hasNotPassed("StartEvent_02poiph", "StartEvent_19n6zlx").isEnded();

    }

    @Test
    @Deployment(resources = { "process_msg_start.bpmn"})
    public void test_start_events_behavior_condition_start() {

        ProcessInstance processInstance = runtimeService().createConditionEvaluation().setVariable("temperature", 150)
                .evaluateStartConditions().get(0);

        assertThat(processInstance).isStarted()
                .hasPassed("StartEvent_02poiph", "ExclusiveGateway_1rz5jm8", "EndEvent_0g3693o")
                .hasNotPassed("StartEvent_1sy7i8d", "StartEvent_19n6zlx").isEnded();

    }

    @Test
    @Deployment(resources = { "process_msg_start.bpmn"})
    public void test_start_events_behavior_condition_start_not_conditional() {

        List<ProcessInstance> evaluateStartConditions = runtimeService().createConditionEvaluation().setVariable("temperature", 20)
                .evaluateStartConditions();

        Assertions.assertThat(evaluateStartConditions).isEmpty();
    }

    @Test
    @Deployment(resources = { "process_msg_start.bpmn"})
    public void test_start_events_behavior_signal_start() {

        runtimeService().createSignalEvent("Signal_3jsteoj").send();
        ProcessInstance processInstance = runtimeService().createProcessInstanceQuery().processDefinitionKey("start-events-examples").active().singleResult();

        assertThat(processInstance).isStarted().isWaitingAt("StartEvent_19n6zlx");
        execute(job());
        assertThat(processInstance)
                .hasPassed("StartEvent_19n6zlx", "ExclusiveGateway_1rz5jm8", "EndEvent_0g3693o")
                .hasNotPassed("StartEvent_1sy7i8d", "StartEvent_02poiph").isEnded();

    }

}
