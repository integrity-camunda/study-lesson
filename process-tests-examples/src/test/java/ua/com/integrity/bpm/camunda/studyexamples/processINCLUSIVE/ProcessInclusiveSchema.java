package ua.com.integrity.bpm.camunda.studyexamples.processINCLUSIVE;

import org.apache.ibatis.logging.LogFactory;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.test.assertions.bpmn.AbstractAssertions;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.runtimeService;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.assertThat;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.historyService;

public class ProcessInclusiveSchema {


    @ClassRule
    @Rule
    public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();

    private static final String PROCESS_DEFINITION_KEY = "studyexamples";

    static {
        LogFactory.useSlf4jLogging(); // MyBatis
    }

    @Before
    public void setup() {
        AbstractAssertions.init(rule.getProcessEngine());
    }



    @Test
    @Deployment(resources = { "process_INCLUSIVE.bpmn",})
    public void test_INCLUSIVE_Behaviour_variable_eq_1() {
        Map<String, Object> variables = new HashMap<>();
        variables.put("exampleVariable", 1);
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("INCLUSIVE_gateway_behavior", variables);
        assertThat(processInstance).isStarted().hasPassed("StartEvent_0kewhp5", "ExclusiveGateway_08ptmoo", "EndEvent_0ftfvnr", "EndEvent_0puxh6w");

    }

    @Test
    @Deployment(resources = { "process_INCLUSIVE.bpmn"})
    public void test_INCLUSIVE_Behaviour_variable_eq_3() {
        Map<String, Object> variables = new HashMap<>();
        variables.put("exampleVariable", 3);
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("INCLUSIVE_gateway_behavior", variables);
        assertThat(processInstance).isStarted().hasPassed("StartEvent_0kewhp5", "ExclusiveGateway_08ptmoo", "EndEvent_05a7lof");

    }
}
