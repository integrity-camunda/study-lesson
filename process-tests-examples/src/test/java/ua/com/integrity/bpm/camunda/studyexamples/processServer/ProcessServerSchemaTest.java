package ua.com.integrity.bpm.camunda.studyexamples.processServer;

import org.apache.ibatis.logging.LogFactory;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.test.mock.Mocks;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import ua.com.integrity.bpm.camunda.studyexamples.servicetask.ExecutionListenerImpl;
import ua.com.integrity.bpm.camunda.studyexamples.servicetask.JavaDelegateImpl;
import ua.com.integrity.bpm.camunda.studyexamples.servicetask.TaskListenerImpl;

import java.util.HashMap;
import java.util.Map;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.*;

/**
 * Test case starting an in-memory database-backed Process Engine.
 */
public class ProcessServerSchemaTest {

  @ClassRule
  @Rule
  public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();

  private static final String PROCESS_DEFINITION_KEY = "studyexamples";

  static {
    LogFactory.useSlf4jLogging(); // MyBatis
  }

  @Before
  public void setup() {
    init(rule.getProcessEngine());
  }


	@Test
	@Deployment(resources = {"process_server.bpmn"})
	public void test_service_task_implementation_WildFly_run() {
		JavaDelegateImpl javaDelegateImpl = new JavaDelegateImpl();
		Mocks.register("javaDelegateImpl", javaDelegateImpl);
		ExecutionListenerImpl executionListenerImpl= new ExecutionListenerImpl();
		Mocks.register("executionListenerImpl", executionListenerImpl);
		TaskListenerImpl taskListenerImpl = new TaskListenerImpl();
		Mocks.register("taskListenerImpl", taskListenerImpl);
		Map<String,Object> vars = new HashMap<>();
		vars.put("key","value");
		ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("service-task-impl-example",vars);

		assertThat(processInstance).isStarted()
				.hasPassed("StartEventProcessStarted", "Task_1bt1as1")
				.isWaitingAt("Task_0wvnfe4")
				.hasVariables("executionListener", "javaDelegate", "taskListener");

		complete(task("Task_0wvnfe4",processInstance));
		assertThat(processInstance).hasPassed("Task_0wvnfe4").isEnded();

	}





}
