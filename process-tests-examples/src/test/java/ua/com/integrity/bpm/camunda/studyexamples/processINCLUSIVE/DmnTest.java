package ua.com.integrity.bpm.camunda.studyexamples.processINCLUSIVE;

import org.apache.ibatis.logging.LogFactory;
import org.assertj.core.api.Assertions;
import org.camunda.bpm.dmn.engine.DmnEngine;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.test.assertions.bpmn.AbstractAssertions;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.runtimeService;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.*;

public class DmnTest {


    @ClassRule
    @Rule
    public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();

    private static final String PROCESS_DEFINITION_KEY = "studyexamples";

    static {
        LogFactory.useSlf4jLogging(); // MyBatis
    }

    @Before
    public void setup() {
        AbstractAssertions.init(rule.getProcessEngine());
    }



    @Test
    @Deployment(resources = { "loan-approve-dmn.dmn",})
    public void test_Dmn() {
        Boolean result = decisionService().evaluateDecisionByKey("Decision_1h50cme")
                .variables(withVariables("age", 20, "amount", 500))
                .evaluate()
                .getSingleEntry();

        Assertions.assertThat(result).isFalse();
    }

    @Test
    @Deployment(resources = { "loan-approve-dmn.dmn",})
    public void test_Dmn_no_age() {
        Boolean result = decisionService().evaluateDecisionByKey("Decision_1h50cme")
                .variables(withVariables( "amount", 500))
                .evaluate()
                .getSingleEntry();

        Assertions.assertThat(result).isFalse();
    }

    @Test
    @Deployment(resources = { "loan-approve-dmn.dmn",})
    public void test_Dmn_1000() {
        Boolean result = decisionService().evaluateDecisionByKey("Decision_1h50cme")
                .variables(withVariables( "amount", 1500, "age", 20))
                .evaluate()
                .getSingleEntry();

        Assertions.assertThat(result).isNull();


    }

}
