package ua.com.integrity.bpm.camunda.studyexamples.processAND;

import org.apache.ibatis.logging.LogFactory;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.runtimeService;
import static org.camunda.bpm.engine.test.assertions.bpmn.AbstractAssertions.init;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.assertThat;

public class ProcessEndSchema {
    @ClassRule
    @Rule
    public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();

    private static final String PROCESS_DEFINITION_KEY = "studyexamples";

    static {
        LogFactory.useSlf4jLogging(); // MyBatis
    }

    @Before
    public void setup() {
        init(rule.getProcessEngine());
    }

    @Test
    @Deployment(resources = {"process_AND.bpmn",})
    public void testAND_gateway_behaviour() {
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("AND-gateway-behaviour");
        assertThat(processInstance).isStarted().hasPassed("StartEvent_0fflz7q", "ExclusiveGateway_0vst1zh", "EndEvent_1hysdyz", "EndEvent_1c570dh");

    }
}
