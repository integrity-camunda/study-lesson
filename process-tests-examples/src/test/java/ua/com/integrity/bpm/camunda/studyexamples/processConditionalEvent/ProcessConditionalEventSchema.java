package ua.com.integrity.bpm.camunda.studyexamples.processConditionalEvent;

import org.apache.ibatis.logging.LogFactory;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.history.HistoricProcessInstance;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.test.assertions.bpmn.AbstractAssertions;
import org.camunda.bpm.engine.test.mock.Mocks;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.execute;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.job;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.runtimeService;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.assertThat;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.historyService;

public class ProcessConditionalEventSchema {

    @ClassRule
    @Rule
    public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();

    private static final String PROCESS_DEFINITION_KEY = "studyexamples";

    static {
        LogFactory.useSlf4jLogging(); // MyBatis
    }

    @Before
    public void setup() {
        AbstractAssertions.init(rule.getProcessEngine());
    }

    @Test
    @Deployment(resources = { "process_conditional_event.bpmn"})
    public void test_variable_intermediate() {
        Map<String, Object> variables = new HashMap<>();
        variables.put("temperature", 50);
        JavaDelegate javaDelegate = (execution) -> execution.setVariable("temperature", 80);
        Mocks.register("temperatureUpdate", javaDelegate);
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("process_conditional_event", variables);

        assertThat(processInstance).isStarted()
                .hasPassedInOrder("StartEvent_1buowdc", "ExclusiveGateway_1tnkmns", "Task_1t6y6nc");
        execute(job());
        assertThat(processInstance).hasPassed("EndEvent_1g4hi72")
                .hasNotPassed("IntermediateThrowEvent_1qb56mf").isWaitingAt("IntermediateThrowEvent_1qb56mf");
    }

    @Test
    @Deployment(resources = {"process_conditional_event.bpmn"})
    public void test_variable_intermediate_passed() {
        Map<String, Object> variables = new HashMap<>();
        variables.put("temperature", 50);
        JavaDelegate javaDelegate = (execution) -> execution.setVariable("temperature", 2000);
        Mocks.register("temperatureUpdate", javaDelegate);
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("process_conditional_event", variables);

        assertThat(processInstance).isStarted()
                .hasPassed("StartEvent_1buowdc", "ExclusiveGateway_1tnkmns", "Task_1t6y6nc");

        execute(job());
        assertThat(processInstance).hasPassed("IntermediateThrowEvent_1qb56mf", "EndEvent_0kckpoe",
                "Task_1t6y6nc", "EndEvent_1g4hi72").isEnded();

        HistoricProcessInstance historicProcessInstance = historyService().createHistoricProcessInstanceQuery()
                .processInstanceId(processInstance.getProcessInstanceId())
                .singleResult();
        historicProcessInstance.getStartTime();
        historicProcessInstance.getEndTime();

    }
}
