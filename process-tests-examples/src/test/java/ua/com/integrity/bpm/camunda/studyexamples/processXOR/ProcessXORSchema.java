package ua.com.integrity.bpm.camunda.studyexamples.processXOR;

import org.apache.ibatis.logging.LogFactory;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.runtimeService;
import static org.camunda.bpm.engine.test.assertions.bpmn.AbstractAssertions.init;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.assertThat;

public class ProcessXORSchema {

    @ClassRule
    @Rule
    public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();

    private static final String PROCESS_DEFINITION_KEY = "studyexamples";

    static {
        LogFactory.useSlf4jLogging(); // MyBatis
    }

    @Before
    public void setup() {
        init(rule.getProcessEngine());
    }




    @Test
    @Deployment(resources = { "process_XOR.bpmn"})
    public void testXORBehaviour_variable_eq_1() {
        Map<String, Object> variables = new HashMap<>();
        variables.put("exampleVariable", 1);
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("xor-behaviour", variables);
        assertThat(processInstance).isStarted().hasPassed("StartEvent_1o29ogk", "ExclusiveGateway_14k1syx", "EndEvent_1eifv59");

    }

    @Test
    @Deployment(resources = { "process_XOR.bpmn"})
    public void testXORBehaviour_variable_eq_2() {
        Map<String, Object> variables = new HashMap<>();
        variables.put("exampleVariable", 2);
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("xor-behaviour", variables);
        assertThat(processInstance).isStarted().hasPassed("StartEvent_1o29ogk", "ExclusiveGateway_14k1syx", "EndEvent_1kr0rkb");

    }

}
