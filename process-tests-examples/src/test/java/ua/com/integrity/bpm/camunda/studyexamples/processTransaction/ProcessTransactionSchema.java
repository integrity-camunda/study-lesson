package ua.com.integrity.bpm.camunda.studyexamples.processTransaction;

import org.apache.ibatis.logging.LogFactory;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.test.mock.Mocks;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.runtimeService;
import static org.camunda.bpm.engine.test.assertions.bpmn.AbstractAssertions.init;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.assertThat;

public class ProcessTransactionSchema {

    @ClassRule
    @Rule
    public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();

    private static final String PROCESS_DEFINITION_KEY = "studyexamples";

    static {
        LogFactory.useSlf4jLogging(); // MyBatis
    }

    @Before
    public void setup() {
        init(rule.getProcessEngine());
    }


    @Test
    @Deployment(resources = {"process_transaction.bpmn"})
    public void test_compensation_in_transaction() {
        JavaDelegate javaDelegate = (execution) -> execution.setVariable("operation", "ok");
        Mocks.register("operationTask", javaDelegate);
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("process_boundary_event");
        assertThat(processInstance).isStarted()
                .hasPassed("StartEvent_0f82yz8", "StartEvent_1e0cmwr", "Task_009kn3e",
                        "EndEvent_13z5mlx", "EndEvent_13f5z4d")
                .hasNotPassed("EndEvent_0tcpblk", "Task_1rtk8gj")
                .isEnded();

    }

    @Test
    @Deployment(resources = {"process_transaction.bpmn"})
    public void test_compensation_done_in_transaction() {
        JavaDelegate javaDelegate = (execution) -> execution.setVariable("operation", "rollback");
        Mocks.register("operationTask", javaDelegate);
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("process_boundary_event");
        assertThat(processInstance).isStarted()
                .hasPassed("StartEvent_0f82yz8", "StartEvent_1e0cmwr", "Task_009kn3e",
                        "EndEvent_0tcpblk", "Task_1rtk8gj", "BoundaryEvent_09d267i", "EndEvent_19coprk")
                .hasNotPassed("EndEvent_13f5z4d")
                .isEnded();

    }

}
