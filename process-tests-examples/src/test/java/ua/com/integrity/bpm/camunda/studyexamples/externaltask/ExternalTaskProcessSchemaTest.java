package ua.com.integrity.bpm.camunda.studyexamples.externaltask;

import org.apache.ibatis.logging.LogFactory;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.runtimeService;
import static org.camunda.bpm.engine.test.assertions.bpmn.AbstractAssertions.init;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.*;

public class ExternalTaskProcessSchemaTest {
    @ClassRule
    @Rule
    public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();

    private static final String PROCESS_DEFINITION_KEY = "process_with_external_task";

    static {
        LogFactory.useSlf4jLogging(); // MyBatis
    }

    @Before
    public void setup() {
        init(rule.getProcessEngine());
    }

    @Test
    @Deployment(resources = {"process-with-external-task.bpmn"})
    public void testAND_gateway_behaviour() {
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey(PROCESS_DEFINITION_KEY);
        assertThat(processInstance).isStarted().isWaitingAt("Activity_0mfczeu")
                .externalTask().hasTopicName("topic-test");

        externalTaskService().fetchAndLock(1, "testWorkerId")
                .topic("topic-test",1000L)
                .execute();

        externalTaskService().complete(externalTask().getId(), "testWorkerId");

        assertThat(processInstance).hasPassed("Activity_0mfczeu").isWaitingAt("Activity_0mfczeu");

        execute(job());

        assertThat(processInstance)
                .isEnded();
    }
}
