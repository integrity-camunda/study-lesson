package ua.com.integrity.bpm.camunda.studyexamples.processServerBusinssRule;

import org.apache.ibatis.logging.LogFactory;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.junit.*;

import java.util.HashMap;
import java.util.Map;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.*;

public class ProcessServerBusinessRuleSchemaTest {

    @ClassRule
    @Rule
    public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();

    private static final String PROCESS_DEFINITION_KEY = "studyexamples";

    static {
        LogFactory.useSlf4jLogging(); // MyBatis
    }

    @Before
    public void setup() {
        init(rule.getProcessEngine());
    }



    @Test
    @Deployment(resources = {"process_server_business_rule.bpmn","call_activiti_here.bpmn", "diagram_check_payment.dmn"})
    public void testBusinessRoleProcessAmountMoreThan_1000() {
        Map<String, Object> variables = new HashMap<>();
        variables.put("amount", 2000);
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("business-rule-task-subproc-call-activity-example", variables);

        assertThat(processInstance).isStarted()
                .hasPassed("StartEvent_0mu4u6z", "ScriptTask_18x13ua", "ExclusiveGateway_11rg7pc", "StartEvent_15ozk1q")
                .isWaitingAt("Task_1ng16qg").hasNotPassed("Task_0ao7va1");

        complete(task());

        assertThat(processInstance).hasPassed("Task_1ng16qg", "EndEvent_1km84b9").isWaitingAt("IntermediateThrowEvent_1jiq5ft");
        execute(job());
        assertThat(processInstance).isEnded();

  }

    @Test
    @Deployment(resources = {"process_server_business_rule.bpmn","call_activiti_here.bpmn", "diagram_check_payment.dmn"})
    public void testBusinessRoleProcessAmountLessThan_1000() {
        Map<String, Object> variables = new HashMap<>();
        variables.put("amount", 100);
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("business-rule-task-subproc-call-activity-example", variables);

        assertThat(processInstance).isStarted()
                .hasPassed("StartEvent_0mu4u6z", "ScriptTask_18x13ua", "ExclusiveGateway_11rg7pc")
                .isWaitingAt("Task_0ao7va1").hasNotPassed("StartEvent_15ozk1q");
        //call activity check
        ProcessInstance callActivityProcess = runtimeService().createProcessInstanceQuery().processDefinitionKey("call_activity_make_payment").active().singleResult();
        assertThat(callActivityProcess).isStarted().isWaitingAt("IntermediateThrowEvent_19c8ueg");
        execute(job());
        assertThat(callActivityProcess).hasPassedInOrder("IntermediateThrowEvent_19c8ueg", "Task_1fuwwnc")
                .isWaitingAt("EndEvent_04xy083").hasVariables("operationResult");
        execute(job());
        assertThat(callActivityProcess).isEnded();
        //end call activity check
        assertThat(processInstance).hasPassed("Task_0ao7va1").isWaitingAt("IntermediateThrowEvent_1jiq5ft");
        execute(job());
        assertThat(processInstance).isEnded();

   }

}
