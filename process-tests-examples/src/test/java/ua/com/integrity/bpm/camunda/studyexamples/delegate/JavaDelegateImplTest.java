package ua.com.integrity.bpm.camunda.studyexamples.delegate;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.junit.Test;
import org.mockito.Mockito;
import ua.com.integrity.bpm.camunda.studyexamples.servicetask.JavaDelegateImpl;

import static org.mockito.Mockito.verify;

public class JavaDelegateImplTest {


    private DelegateExecution execution = Mockito.mock(DelegateExecution.class);
    private JavaDelegateImpl javaDelegate = new JavaDelegateImpl();

    @Test
    public void executionTest() throws Exception {

        javaDelegate.execute(execution);

        verify(execution).setVariable("javaDelegate", "value setted in JavaDelegateImpl");

    }

}
