package ua.com.integrity.bpm.camunda.studyexamples.processEVENT;

import org.apache.ibatis.logging.LogFactory;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.test.assertions.bpmn.AbstractAssertions;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.execute;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.jobQuery;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.runtimeService;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.assertThat;

public class ProcessEventSchema {

    @ClassRule
    @Rule
    public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();

    private static final String PROCESS_DEFINITION_KEY = "studyexamples";

    static {
        LogFactory.useSlf4jLogging(); // MyBatis
    }

    @Before
    public void setup() {
        AbstractAssertions.init(rule.getProcessEngine());
    }



    @Test
    @Deployment(resources = {"process_EVENT.bpmn"})
    public void test_EVENT_BASED_gateway_behavior_message_first() {
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("event_based_gateway_behavior");
        assertThat(processInstance).isStarted().hasPassed("StartEvent_0d0ai2b")
                .isWaitingAt("ExclusiveGateway_15qi8o8");

        runtimeService().createMessageCorrelation("Message_2idasdi")
                .processInstanceId(processInstance.getProcessInstanceId())
                .correlate();

        assertThat(processInstance).hasPassed("ExclusiveGateway_15qi8o8", "IntermediateCatchEvent_1dicp5k", "EndEvent_05sm92v").isEnded();

    }

    @Test
    @Deployment(resources = {"process_EVENT.bpmn"})
    public void test_EVENT_BASED_gateway_behavior_timer_first() {
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("event_based_gateway_behavior");
        assertThat(processInstance).isStarted().hasPassed("StartEvent_0d0ai2b")
                .isWaitingAt("ExclusiveGateway_15qi8o8");

        execute(jobQuery().timers().singleResult());

        assertThat(processInstance).hasPassed("ExclusiveGateway_15qi8o8", "IntermediateCatchEvent_0ibwq4t", "EndEvent_0yaz7ch").isEnded();

    }

}
