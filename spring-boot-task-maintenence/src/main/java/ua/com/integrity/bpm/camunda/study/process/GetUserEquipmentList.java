package ua.com.integrity.bpm.camunda.study.process;

import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;
import ua.com.integrity.bpm.camunda.study.ProcessVariables;
import ua.com.integrity.bpm.camunda.study.dto.EmployeeDto;
import ua.com.integrity.bpm.camunda.study.dto.equipment.EquipmentDto;
import ua.com.integrity.bpm.camunda.study.dto.equipment.EquipmentTypeDto;
import ua.com.integrity.bpm.camunda.study.service.EmployeeService;
import ua.com.integrity.bpm.camunda.study.service.EquipmentService;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class GetUserEquipmentList implements JavaDelegate {

  private final EmployeeService employeeService;
  private final EquipmentService equipmentService;

  public void execute(DelegateExecution execution) throws Exception {
    String login = (String) execution.getVariable(ProcessVariables.INITIATOR);
    EmployeeDto employee = employeeService.getByLogin(login);

    List<EquipmentDto> allEquipment = equipmentService.findAll(false);
    List<EquipmentDto> employeesEquipment = allEquipment.stream()
            .filter(equipment -> employee.getId().equals(equipment.getUser()))
            .collect(Collectors.toList());
    
    List<EquipmentTypeDto> allEquipmentTypes = equipmentService.getAllTypes();

    Set<Long> employeesEquipmentTypeSet = employeesEquipment.stream().map(EquipmentDto::getType).collect(Collectors.toSet());
    String userEquipmentList = allEquipmentTypes.stream()
            .filter(type -> employeesEquipmentTypeSet.contains(type.getId()))
            .map(type -> new EquipmentTypeNumber(type.getTitle(), findEquipmentByType(employeesEquipment, type.getId())).toString())
            .collect(Collectors.joining(";"));
    execution.setVariable(ProcessVariables.userEquipmentList, userEquipmentList );

  }

  private static Set<String> findEquipmentByType(List<EquipmentDto> employeesEquipment, Long typeId) {
    return employeesEquipment.stream()
            .filter(eq -> eq.getType().equals(typeId))
            .map(EquipmentDto::getSerialNumber)
            .collect(Collectors.toSet());
  }

  @RequiredArgsConstructor
  public static class EquipmentTypeNumber {
    private final String title;
    private final Set<String> serialNumbers;

    @Override
    public String toString() {
      return title + ":" + serialNumbers.toString();
    }
  }

}
