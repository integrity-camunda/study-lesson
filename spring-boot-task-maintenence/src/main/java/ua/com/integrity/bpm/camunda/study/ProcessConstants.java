package ua.com.integrity.bpm.camunda.study;

public class ProcessConstants {

  public static final String PROCESS_DEFINITION_KEY = "spring-boot-task-maintenence"; // BPMN Process ID
  public static final String START_EVENT = "start_event"; // BPMN Process ID
  public static final String SERVICE_TASK_RECIEVE_USER_EQUIPMENT_LIST = "service_task_recieve_user_equipment_list"; // BPMN Process ID

  public static final String HUMAN_TASK_SELECT_EQUIPMENT_FOR_MAINTENENCE = "human_task_select_equipment_for_maintenence"; // BPMN Process ID

  public static final String SERVICE_TASK_GET_MAINTENENCE_LIST = "service_task_get_maintenence_list"; // BPMN Process ID
  public static final String HUMAN_TASK_SELECT_MAINTENANCE_TYPE = "human_task_select_maintenance_type"; // BPMN Process ID
  public static final String SERVICE_TASK_GET_CONSUMPTION_RATES = "service_task_get_consumption_rates"; // BPMN Process ID
  public static final String GATEWAY_0GRE3HT = "Gateway_0gre3ht"; // BPMN Process ID
  public static final String SERVICE_TASK_DEFINE_BATCH_OF_MATERIAL_TO_WITHDRAW = "service_task_define_batch_of_material_to_withdraw"; // BPMN Process ID
  public static final String ERROR_EVENT_UNSUFFICIENT_AMOUNT_OF_MATERIALS = "error_event_unsufficient_amount_of_materials";
  public static final String HUMAN_TASK_PROVIDE_NEW_MATERIALS = "human_task_provide_new_materials";
  public static final String HUMAN_TASK_PROVIDE_MAINTENENCE = "human_task_provide_maintenence";
  public static final String SERVICE_TASK_WITHDRAW_MATERIALS_FROM_BATCH = "service_task_withdraw_materials_from_batch";







}
