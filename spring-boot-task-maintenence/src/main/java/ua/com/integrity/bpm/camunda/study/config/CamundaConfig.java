package ua.com.integrity.bpm.camunda.study.config;

import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableProcessApplication("spring-boot-task-maintenence")
public class CamundaConfig {
}
