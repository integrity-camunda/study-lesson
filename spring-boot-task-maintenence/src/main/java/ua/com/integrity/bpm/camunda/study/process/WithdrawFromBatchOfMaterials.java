package ua.com.integrity.bpm.camunda.study.process;

import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;
import ua.com.integrity.bpm.camunda.study.ProcessVariables;
import ua.com.integrity.bpm.camunda.study.dto.api.BatchWriteOffRequestDto;
import ua.com.integrity.bpm.camunda.study.service.MaterialService;

import java.math.BigDecimal;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class WithdrawFromBatchOfMaterials implements JavaDelegate {

  private final MaterialService materialService;

  public void execute(DelegateExecution execution) throws Exception {
    Map<Long, BigDecimal> batchesForWithdraw = (Map<Long, BigDecimal>) execution.getVariable(ProcessVariables.batchesForWithdraw);
    for (Map.Entry<Long, BigDecimal> entry : batchesForWithdraw.entrySet()) {
      BatchWriteOffRequestDto writeOffRequestDto = new BatchWriteOffRequestDto();
      writeOffRequestDto.setBatchId(entry.getKey());
      writeOffRequestDto.setAmount(entry.getValue().toPlainString());
      materialService.writeOffFromBatch(entry.getKey(), writeOffRequestDto);
    }

  }

}
