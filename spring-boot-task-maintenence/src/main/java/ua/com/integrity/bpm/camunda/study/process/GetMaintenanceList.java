package ua.com.integrity.bpm.camunda.study.process;

import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;
import ua.com.integrity.bpm.camunda.study.ProcessVariables;
import ua.com.integrity.bpm.camunda.study.dto.equipment.EquipmentDto;
import ua.com.integrity.bpm.camunda.study.dto.equipment.EquipmentTypeDto;
import ua.com.integrity.bpm.camunda.study.dto.equipment.MaintenanceDto;
import ua.com.integrity.bpm.camunda.study.service.EquipmentService;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class GetMaintenanceList implements JavaDelegate {

  private final EquipmentService equipmentService;

  public void execute(DelegateExecution execution) throws Exception {
    String serialNumber = (String) execution.getVariable(ProcessVariables.equipmentForMaintenence);
    EquipmentDto equipmentDto = equipmentService.getEquipment(serialNumber);
    EquipmentTypeDto equipmentTypeDto = equipmentService.getEquipmentType(equipmentDto.getType());
    execution.setVariable(ProcessVariables.equipmentType, equipmentTypeDto.getTitle());
    Set<String> availableMaintenanceSet = new HashSet<>();
    for (Long maintenanceId : equipmentTypeDto.getMaintenanceSet()) {
      MaintenanceDto maintenanceDto = equipmentService.getMaintenance(maintenanceId);
      availableMaintenanceSet.add(maintenanceDto.getTitle() + ": " + maintenanceId);
    }
    execution.setVariable(ProcessVariables.availableMaintenanceSet, availableMaintenanceSet.stream().collect(Collectors.joining(";")));
  }

}
