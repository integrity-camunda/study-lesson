package ua.com.integrity.bpm.camunda.study.process;

import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.delegate.BpmnError;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;
import ua.com.integrity.bpm.camunda.study.ProcessVariables;
import ua.com.integrity.bpm.camunda.study.dto.materail.BatchOfMaterialDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.ConsumptionRateDto;
import ua.com.integrity.bpm.camunda.study.service.MaterialService;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class DefineBatchOfMaterialsToWithdraw implements JavaDelegate {

  private final MaterialService materialService;

  public void execute(DelegateExecution execution) throws Exception {
    Set<ConsumptionRateDto> consumptionRates = (Set<ConsumptionRateDto>) execution.getVariable(ProcessVariables.consumptionRates);
    Map<Long, BigDecimal> batchesForWithdraw = new HashMap<>();
    Map<String, BigDecimal> materialsForRenew = new HashMap<>();
    for (ConsumptionRateDto rate : consumptionRates) {
      List<BatchOfMaterialDto> batches = materialService.getBatches(rate.getMaterial().getId());
      Optional<BatchOfMaterialDto> batchToUseOpt = batches.stream()
              .filter(batch -> batch.getRemainingAmount().compareTo(rate.getAmount()) >= 0)
              .findAny();
      if (batchToUseOpt.isPresent()) {
        BatchOfMaterialDto batchToUse = batchToUseOpt.get();
        batchesForWithdraw.put(batchToUse.getId(), rate.getAmount());
      } else {
        materialsForRenew.put(rate.getMaterial().getTitle(), rate.getAmount());
      }
    }

    if (!materialsForRenew.isEmpty()) {
      String listOfMaterialsToRenew = materialsForRenew.entrySet().stream()
              .map(entry -> entry.getKey() + ": " + entry.getValue().toPlainString())
              .collect(Collectors.joining(";"));
      execution.setVariable(ProcessVariables.listOfMaterialsToRenew, listOfMaterialsToRenew);
      throw new BpmnError("NOT_ENOUPH_MATERIALS");
    }
    execution.setVariable(ProcessVariables.batchesForWithdraw, batchesForWithdraw);
  }

}
