package ua.com.integrity.bpm.camunda.study.process;

import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;
import ua.com.integrity.bpm.camunda.study.ProcessVariables;
import ua.com.integrity.bpm.camunda.study.dto.equipment.MaintenanceDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.ConsumptionRateDto;
import ua.com.integrity.bpm.camunda.study.service.EquipmentService;
import ua.com.integrity.bpm.camunda.study.service.MaterialService;

import java.util.HashSet;
import java.util.Set;

@Component
@RequiredArgsConstructor
public class GetSupplyRates implements JavaDelegate {

  private final EquipmentService equipmentService;
  private final MaterialService materialService;

  public void execute(DelegateExecution execution) throws Exception {
    Long maintenenceType = (Long) execution.getVariable(ProcessVariables.selectedMaintenanceType);
    MaintenanceDto maintenanceDto = equipmentService.getMaintenance(maintenenceType);
    Set<ConsumptionRateDto> consumptionRates = new HashSet<>();
    for (Long rateId : maintenanceDto.getConsumptionRates()) {
      ConsumptionRateDto consumptionRate = materialService.getConsumptionRate(rateId);
      consumptionRates.add(consumptionRate);
    }
    execution.setVariable(ProcessVariables.consumptionRates, consumptionRates);
  }

}
