package ua.com.integrity.bpm.camunda.study.config;

import feign.okhttp.OkHttpClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ua.com.integrity.bpm.camunda.study.service.EmployeeService;
import ua.com.integrity.bpm.camunda.study.service.EquipmentService;
import ua.com.integrity.bpm.camunda.study.service.MaterialService;

@Configuration
@EnableFeignClients(clients = {
        EmployeeService.class, EquipmentService.class, MaterialService.class
})
public class FeignConfig {

    @Bean
    public OkHttpClient client() {
        return new OkHttpClient();
    }
}
