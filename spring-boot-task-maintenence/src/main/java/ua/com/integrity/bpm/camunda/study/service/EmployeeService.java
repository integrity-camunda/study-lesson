package ua.com.integrity.bpm.camunda.study.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ua.com.integrity.bpm.camunda.study.dto.EmployeeDto;

@FeignClient(name = "employeeServiceClient", url = "https://wisconsin.integrity.com.ua/erp/api/rest/org-structure/employee/")
public interface EmployeeService {
    @GetMapping(path = "login/{login}")
    EmployeeDto getByLogin(@PathVariable("login") String login);
}
