package ua.com.integrity.bpm.camunda.study.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import ua.com.integrity.bpm.camunda.study.dto.api.BatchWriteOffRequestDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.BatchOfMaterialDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.ConsumptionRateDto;

import java.util.List;

@FeignClient(name = "materialServiceClient", url = "https://wisconsin.integrity.com.ua/erp/api/rest/material/")
public interface MaterialService {

    @GetMapping(path = "batch")
    List<BatchOfMaterialDto> getBatches(@RequestParam(name = "material") Long id);


    @PatchMapping(path = "batch/{id}")
    void writeOffFromBatch(@PathVariable("id") Long id, @RequestBody BatchWriteOffRequestDto writeOffRequestDto);

    @GetMapping(path = "consumption-rate/{rateId}")
    ConsumptionRateDto getConsumptionRate(@PathVariable("rateId") Long rateId);
}
