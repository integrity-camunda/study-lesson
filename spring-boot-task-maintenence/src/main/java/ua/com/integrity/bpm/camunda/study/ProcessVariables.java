package ua.com.integrity.bpm.camunda.study;

public class ProcessVariables {

    public static final String INITIATOR = "initiator";
    public static final String userEquipmentList = "userEquipmentList";
    public static final String equipmentForMaintenence = "equipmentForMaintenence";
    public static final String selectedMaintenanceType = "selectedMaintenanceType";
    public static final String equipmentType = "equipmentType";
    public static String availableMaintenanceSet = "availableMaintenanceSet";
    public static String consumptionRates = "consumptionRates";
    public static String listOfMaterialsToRenew = "listOfMaterialsToRenew";
    public static String batchesForWithdraw = "batchesForWithdraw";
}
