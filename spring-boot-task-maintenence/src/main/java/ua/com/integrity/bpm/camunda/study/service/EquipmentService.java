package ua.com.integrity.bpm.camunda.study.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import ua.com.integrity.bpm.camunda.study.dto.equipment.EquipmentDto;
import ua.com.integrity.bpm.camunda.study.dto.equipment.EquipmentTypeDto;
import ua.com.integrity.bpm.camunda.study.dto.equipment.MaintenanceDto;

import java.util.List;

@FeignClient(name = "equipmentServiceClient", url = "https://wisconsin.integrity.com.ua/erp/api/rest")
public interface EquipmentService {

    @GetMapping(path = "equipment")
    List<EquipmentDto> findAll(@RequestParam(name = "withDecommissioned") boolean withDecommisioned);

    @GetMapping(path = "equipment/type")
    List<EquipmentTypeDto> getAllTypes();

    @GetMapping(path = "equipment/{serialNumber}")
    EquipmentDto getEquipment(@PathVariable("serialNumber") String serialNumber);

    @GetMapping(path = "equipment/type/{type}")
    EquipmentTypeDto getEquipmentType(@PathVariable("type") Long type);

    @GetMapping(path = "equipment/maintenance/{maintenanceId}")
    MaintenanceDto getMaintenance(@PathVariable("maintenanceId") Long maintenanceId);


}
