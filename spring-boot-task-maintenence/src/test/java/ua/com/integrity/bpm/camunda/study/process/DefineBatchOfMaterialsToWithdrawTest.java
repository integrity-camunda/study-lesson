package ua.com.integrity.bpm.camunda.study.process;

import org.camunda.bpm.engine.delegate.BpmnError;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoSettings;
import ua.com.integrity.bpm.camunda.study.ProcessVariables;
import ua.com.integrity.bpm.camunda.study.dto.materail.BatchOfMaterialDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.ConsumptionRateDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.MaterialDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.MeasureUnit;
import ua.com.integrity.bpm.camunda.study.service.MaterialService;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@MockitoSettings
class DefineBatchOfMaterialsToWithdrawTest {

    @Mock
    DelegateExecution delegateExecution;

    @Mock
    MaterialService materialService;

    @InjectMocks
    DefineBatchOfMaterialsToWithdraw defineBatchOfMaterialsToWithdraw;

    @Captor
    ArgumentCaptor<String> forRenewArgumentCaptor;

    @Captor
    ArgumentCaptor<Map<Long, BigDecimal>> forWithrawArgumentCaptor;

    private static final Set<ConsumptionRateDto> CONSUMPTIONRATES = Stream.of(
            ConsumptionRateDto.builder().id(1000L).title("testRate").material(MaterialDto.builder().id(20L).title("Spirt").measureUnit(MeasureUnit.LITER).build()).amount(new BigDecimal("0.1")).build(),
            ConsumptionRateDto.builder().id(2000L).title("testRate2000").material(MaterialDto.builder().id(30L).title("testMat").measureUnit(MeasureUnit.PCS).build()).amount(BigDecimal.valueOf(2)).build()
    ).collect(Collectors.toSet());

    @BeforeEach
    void setUp() {
        when(delegateExecution.getVariable(ProcessVariables.consumptionRates)).thenReturn(CONSUMPTIONRATES);
    }

    @Test
    void execute_all_ok() throws Exception {
        when(materialService.getBatches(20L)).thenReturn(
                Arrays.asList(
                        BatchOfMaterialDto.builder().id(14L).material(MaterialDto.builder().id(20L).title("Spirt").measureUnit(MeasureUnit.LITER).build()).remainingAmount(new BigDecimal("0.05")).build(),
                        BatchOfMaterialDto.builder().id(15L).material(MaterialDto.builder().id(20L).title("Spirt").measureUnit(MeasureUnit.LITER).build()).remainingAmount(new BigDecimal("0.3")).build()
                )
        );
        when(materialService.getBatches(30L)).thenReturn(
                Arrays.asList(
                        BatchOfMaterialDto.builder().id(16L).material(MaterialDto.builder().id(30L).title("testMat").measureUnit(MeasureUnit.PCS).build()).remainingAmount(BigDecimal.valueOf(1)).build(),
                        BatchOfMaterialDto.builder().id(17L).material(MaterialDto.builder().id(30L).title("testMat").measureUnit(MeasureUnit.PCS).build()).remainingAmount(BigDecimal.valueOf(2)).build()
                )
        );

        defineBatchOfMaterialsToWithdraw.execute(delegateExecution);

        verify(delegateExecution).setVariable(Mockito.eq(ProcessVariables.batchesForWithdraw), forWithrawArgumentCaptor.capture());

        assertThat(forWithrawArgumentCaptor.getValue())
                .containsEntry(15L, new BigDecimal("0.1"))
                .containsEntry(17L, new BigDecimal("2"));
    }

    @Test
    void execute_throw_BpmnError() {
        when(materialService.getBatches(20L)).thenReturn(
                Arrays.asList(
                        BatchOfMaterialDto.builder().id(14L).material(MaterialDto.builder().id(20L).title("Spirt").measureUnit(MeasureUnit.LITER).build()).remainingAmount(new BigDecimal("0.05")).build()
                )
        );
        when(materialService.getBatches(30L)).thenReturn(
                Arrays.asList(
                        BatchOfMaterialDto.builder().id(16L).material(MaterialDto.builder().id(30L).title("testMat").measureUnit(MeasureUnit.PCS).build()).remainingAmount(BigDecimal.valueOf(1)).build(),
                        BatchOfMaterialDto.builder().id(17L).material(MaterialDto.builder().id(30L).title("testMat").measureUnit(MeasureUnit.PCS).build()).remainingAmount(BigDecimal.valueOf(2)).build()
                )
        );

        assertThatThrownBy(() -> defineBatchOfMaterialsToWithdraw.execute(delegateExecution))
                .isInstanceOf(BpmnError.class);

        verify(delegateExecution).setVariable(Mockito.eq(ProcessVariables.listOfMaterialsToRenew), forRenewArgumentCaptor.capture());

        assertThat(forRenewArgumentCaptor.getValue())
                .contains("Spirt: 0.1");
    }
}