package ua.com.integrity.bpm.camunda.study.process;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoSettings;
import ua.com.integrity.bpm.camunda.study.ProcessVariables;
import ua.com.integrity.bpm.camunda.study.dto.EmployeeDto;
import ua.com.integrity.bpm.camunda.study.dto.equipment.EquipmentDto;
import ua.com.integrity.bpm.camunda.study.dto.equipment.EquipmentTypeDto;
import ua.com.integrity.bpm.camunda.study.service.EmployeeService;
import ua.com.integrity.bpm.camunda.study.service.EquipmentService;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@MockitoSettings
class GetUserEquipmentListTest {

    @Mock
    DelegateExecution delegateExecution;

    @Mock
    EmployeeService employeeService;

    @Mock
    EquipmentService equipmentService;

    @Captor
    ArgumentCaptor<String> resultArgumentCaptor;

    @InjectMocks
    GetUserEquipmentList getUserEquipmentList;

    @BeforeEach
    void setUp() {
        when(delegateExecution.getVariable(ProcessVariables.INITIATOR)).thenReturn("testUser");
        when(employeeService.getByLogin("testUser")).thenReturn(EmployeeDto.builder()
                .id(1L)
                .build());
    }

    @Test
    void test_that_execute_correctly_update_process_variables() throws Exception {
        when(equipmentService.findAll(false)).thenReturn(Arrays.asList(new EquipmentDto[] {
                EquipmentDto.builder().serialNumber("sn1").type(10L).user(1L).build(),
                EquipmentDto.builder().serialNumber("sn2").type(10L).user(2L).build(),
                EquipmentDto.builder().serialNumber("sn3").type(3L).user(1L).build(),
                EquipmentDto.builder().serialNumber("sn4").type(4L).user(1L).build(),
                EquipmentDto.builder().serialNumber("sn5").type(4L).user(2L).build()
        }));
        when(equipmentService.getAllTypes()).thenReturn(Arrays.asList(new EquipmentTypeDto[]{
                EquipmentTypeDto.builder().id(10L).title("printer").build(),
                EquipmentTypeDto.builder().id(3L).title("notebook").build(),
                EquipmentTypeDto.builder().id(4L).title("mouse").build(),
                EquipmentTypeDto.builder().id(5L).title("phone").build()
        }));

        getUserEquipmentList.execute(delegateExecution);

        verify(delegateExecution).setVariable(eq(ProcessVariables.userEquipmentList), resultArgumentCaptor.capture());

        assertThat(resultArgumentCaptor.getValue())
                .contains("printer:[sn1]","notebook:[sn3]","mouse:[sn4]");
    }
}