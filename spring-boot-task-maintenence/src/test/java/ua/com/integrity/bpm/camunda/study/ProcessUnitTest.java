package ua.com.integrity.bpm.camunda.study;

import org.apache.ibatis.logging.LogFactory;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import ua.com.integrity.bpm.camunda.study.dto.materail.ConsumptionRateDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.MaterialDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.MeasureUnit;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.assertThat;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.complete;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.init;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.processEngine;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.runtimeService;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.task;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.withVariables;
import static ua.com.integrity.bpm.camunda.study.ProcessConstants.*;


/**
 * Test case starting an in-memory database-backed Process Engine.
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class ProcessUnitTest {

  @Autowired
  private ProcessEngine processEngine;

  static {
    LogFactory.useSlf4jLogging(); // MyBatis
  }

  @BeforeEach
  void setup() {
    init(processEngine);
  }

  @Test
  @Deployment(resources = "process.bpmn")
  public void testHappyPath() {
    // Drive the process by API and assert correct behavior by camunda-bpm-assert

    ProcessInstance processInstance = processEngine().getRuntimeService()
            .startProcessInstanceByKey(PROCESS_DEFINITION_KEY, withVariables(ProcessVariables.INITIATOR, "vkadr" ));

    assertThat(processInstance)
            .isStarted()
            .hasPassed(SERVICE_TASK_RECIEVE_USER_EQUIPMENT_LIST)
            .isWaitingAt(HUMAN_TASK_SELECT_EQUIPMENT_FOR_MAINTENENCE)
            .task().isAssignedTo("vkadr");

    String userEquipmentList = (String) fetchProcessVariable(processInstance.getId(), ProcessVariables.userEquipmentList);
    assertThat(userEquipmentList).contains("Ноутбук Lenovo:[3002SN5]", "IP-телефон Grandstream:[3010SN6]");

    complete(task(), withVariables(ProcessVariables.equipmentForMaintenence, "3002SN5"));

    assertThat(processInstance)
            .hasPassed(SERVICE_TASK_GET_MAINTENENCE_LIST)
            .isWaitingAt(HUMAN_TASK_SELECT_MAINTENANCE_TYPE)
            .task().isAssignedTo("aadmin");

    String equipmentTypeForMaintenence = (String) fetchProcessVariable(processInstance.getId(), ProcessVariables.equipmentType);
    assertThat(equipmentTypeForMaintenence).isEqualTo("Ноутбук Lenovo");
    String availableMaintenanceSet = (String) fetchProcessVariable(processInstance.getId(), ProcessVariables.availableMaintenanceSet);
    assertThat(availableMaintenanceSet).contains("Полугодовое обслуживание ПК: 2006", "Текущий ремонт ПК: 2007");

    complete(task(), withVariables(ProcessVariables.selectedMaintenanceType, 2007L ));

    assertThat(processInstance)
            .hasPassedInOrder(
                    SERVICE_TASK_GET_CONSUMPTION_RATES,
                    SERVICE_TASK_DEFINE_BATCH_OF_MATERIAL_TO_WITHDRAW
            ).hasNotPassed(ERROR_EVENT_UNSUFFICIENT_AMOUNT_OF_MATERIALS)
            .isWaitingAt(HUMAN_TASK_PROVIDE_MAINTENENCE)
            .task().isAssignedTo("aadmin");

    Map<Long, BigDecimal> batchesForWithdraw = (Map<Long, BigDecimal>) fetchProcessVariable(processInstance.getId(), ProcessVariables.batchesForWithdraw);
    assertThat(batchesForWithdraw)
            .containsEntry(601L, new BigDecimal("0.5"))
            .containsAnyOf(entry(611L, new BigDecimal("1.3")), entry(612L, new BigDecimal("1.3")));

    complete(task());

    assertThat(processInstance)
            .hasPassed(SERVICE_TASK_WITHDRAW_MATERIALS_FROM_BATCH)
            .isEnded();

  }

  private Object fetchProcessVariable(String processId, String varName) {
    return runtimeService().createVariableInstanceQuery()
            .processInstanceIdIn(processId)
            .variableName(varName)
            .singleResult().getValue();
  }


  private static final Set<ConsumptionRateDto> CONSUMPTIONRATES = Stream.of(
          ConsumptionRateDto.builder().id(1061l).title("Норма расхода спирта №2").material(MaterialDto.builder().id(60L).title("Спирт технический").measureUnit(MeasureUnit.LITER).build()).amount(new BigDecimal("0.5")).build(),
          ConsumptionRateDto.builder().id(1065l).title("Расход кабеля при ТО-1").material(MaterialDto.builder().id(61L).title("Кабель SFTP cat.4").measureUnit(MeasureUnit.PCS).build()).amount(new BigDecimal("1.5")).build()
  ).collect(Collectors.toSet());

  @Test
  @Deployment(resources = "process.bpmn")
  public void test_that_process_flow_to_human_task_for_materials_renew_when_insufficient_amount_of_mateirals_found() {

    ProcessInstance processInstance = runtimeService().createProcessInstanceByKey(PROCESS_DEFINITION_KEY)
            .startBeforeActivity(SERVICE_TASK_DEFINE_BATCH_OF_MATERIAL_TO_WITHDRAW)
            .setVariables(withVariables(ProcessVariables.consumptionRates, CONSUMPTIONRATES))
            .execute();

    assertThat(processInstance)
            .hasPassedInOrder(
                    SERVICE_TASK_DEFINE_BATCH_OF_MATERIAL_TO_WITHDRAW,
                    ERROR_EVENT_UNSUFFICIENT_AMOUNT_OF_MATERIALS
            ).isWaitingAt(HUMAN_TASK_PROVIDE_NEW_MATERIALS)
            .task().isAssignedTo("istarshyna");


    String listOfMaterialsToRenew = (String) fetchProcessVariable(processInstance.getId(), ProcessVariables.listOfMaterialsToRenew);
    assertThat(listOfMaterialsToRenew).isEqualTo("Спирт технический: 0.5");

  }

}
