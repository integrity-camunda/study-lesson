package ua.com.integrity.bpm.camunda.study.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import ua.com.integrity.bpm.camunda.study.config.FeignConfig;
import ua.com.integrity.bpm.camunda.study.dto.EmployeeDto;
import ua.com.integrity.bpm.camunda.study.dto.api.BatchWriteOffRequestDto;
import ua.com.integrity.bpm.camunda.study.dto.equipment.EquipmentDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.BatchOfMaterialDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.ConsumptionRateDto;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

@SpringBootTest(properties = {
        "camunda.bpm.enabled=false",
        "camunda.bpm.webapp.enabled=false"
}, classes = FeignConfig.class)
@EnableAutoConfiguration
class FeignClientsTest {


    @Autowired
    EmployeeService employeeService;

    @Test
    void getByLogin() {
        EmployeeDto employeeDto = employeeService.getByLogin("oglavniy");
        assertThat(employeeDto)
                .hasFieldOrPropertyWithValue("id", 111L)
                .hasFieldOrPropertyWithValue("firstName", "Олег")
                .hasFieldOrPropertyWithValue("lastName", "Главный")
                .hasFieldOrPropertyWithValue("position", 11L);
    }

    @Autowired
    MaterialService materialService;

    @Test
    void getBatches() {
        List<BatchOfMaterialDto> batches = materialService.getBatches(60L);
        assertThat(batches).hasSize(1)
                .extracting(BatchOfMaterialDto::getId, BatchOfMaterialDto::getRemainingAmount)
                .contains(tuple(601L, new BigDecimal("0.25")));
    }

    @Test
    void getConsumptionRate() {
        ConsumptionRateDto rate = materialService.getConsumptionRate(1060l);
        assertThat(rate)
                .hasFieldOrPropertyWithValue("id", 1060L)
                .hasFieldOrPropertyWithValue("title", "Норма расхода спирта №1")
                .hasFieldOrPropertyWithValue("amount", new BigDecimal("0.25"));
    }

    @Autowired
    EquipmentService equipmentService;

    @Test
    void findAll() {
        List<EquipmentDto> equipmentDtos = equipmentService.findAll(false);
        assertThat(equipmentDtos)
                .hasSize(45);
    }

    @Test
    void test_withdraw() {
        BatchWriteOffRequestDto requestDto = new BatchWriteOffRequestDto();
        requestDto.setBatchId(611L);
        requestDto.setAmount("0.1");
        materialService.writeOffFromBatch(611L, requestDto);

    }


}