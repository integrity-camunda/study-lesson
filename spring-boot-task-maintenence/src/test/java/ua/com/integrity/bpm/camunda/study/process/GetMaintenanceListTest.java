package ua.com.integrity.bpm.camunda.study.process;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoSettings;
import ua.com.integrity.bpm.camunda.study.ProcessVariables;
import ua.com.integrity.bpm.camunda.study.dto.equipment.EquipmentDto;
import ua.com.integrity.bpm.camunda.study.dto.equipment.EquipmentTypeDto;
import ua.com.integrity.bpm.camunda.study.dto.equipment.MaintenanceDto;
import ua.com.integrity.bpm.camunda.study.service.EquipmentService;

import java.util.Collections;

import static org.mockito.Mockito.*;

@MockitoSettings
class GetMaintenanceListTest {

    @Mock
    DelegateExecution delegateExecution;

    @Mock
    EquipmentService equipmentService;

    @InjectMocks
    GetMaintenanceList getMaintenanceList;

    @BeforeEach
    void setUp() {
        when(delegateExecution.getVariable(ProcessVariables.equipmentForMaintenence)).thenReturn("sn1");
    }

    @Test
    void test_that_execute_correctly_update_process_variable() throws Exception {
        when(equipmentService.getEquipment("sn1"))
                .thenReturn(EquipmentDto.builder().serialNumber("sn1").type(10L).user(1L).build());
        when(equipmentService.getEquipmentType(10L))
                .thenReturn(EquipmentTypeDto.builder().id(10L).title("testType").maintenanceSet(Collections.singleton(100L)).build());
        when(equipmentService.getMaintenance(100L))
                .thenReturn(MaintenanceDto.builder().id(100L).title("testMaintenance").build());

        getMaintenanceList.execute(delegateExecution);

        verify(delegateExecution).setVariable(eq(ProcessVariables.equipmentType), eq("testType"));
        verify(delegateExecution).setVariable(eq(ProcessVariables.availableMaintenanceSet), eq("testMaintenance: 100"));
    }
}