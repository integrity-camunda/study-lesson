package ua.com.integrity.bpm.camunda.study.process;

import org.assertj.core.api.Assertions;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoSettings;
import ua.com.integrity.bpm.camunda.study.ProcessVariables;
import ua.com.integrity.bpm.camunda.study.dto.equipment.MaintenanceDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.ConsumptionRateDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.MaterialDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.MeasureUnit;
import ua.com.integrity.bpm.camunda.study.service.EmployeeService;
import ua.com.integrity.bpm.camunda.study.service.EquipmentService;
import ua.com.integrity.bpm.camunda.study.service.MaterialService;

import java.math.BigDecimal;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@MockitoSettings
class GetSupplyRatesTest {

    @Mock
    DelegateExecution delegateExecution;

    @Mock
    EquipmentService equipmentService;
    @Mock
    MaterialService materialService;

    @Captor
    ArgumentCaptor<Set<ConsumptionRateDto>> resultArgumentCaptor;

    @InjectMocks
    GetSupplyRates getSupplyRates;

    @BeforeEach
    void setUp() {
        when(delegateExecution.getVariable(ProcessVariables.selectedMaintenanceType)).thenReturn(100L);
    }

    private Set<Long> consumptionRates = Stream.of(1000L, 2000L).collect(Collectors.toSet());

    @Test
    void execute() throws Exception {
        when(equipmentService.getMaintenance(100L))
                .thenReturn(MaintenanceDto.builder().id(100L).consumptionRates(consumptionRates).title("testMaintenance").build());
        when(materialService.getConsumptionRate(1000L))
                .thenReturn(ConsumptionRateDto.builder().id(1000L).title("testRate").material(MaterialDto.builder().id(20L).title("Spirt").measureUnit(MeasureUnit.LITER).build()).amount(new BigDecimal("0.1")).build());
        when(materialService.getConsumptionRate(2000L))
                .thenReturn(ConsumptionRateDto.builder().id(2000L).title("testRate2000").material(MaterialDto.builder().id(20L).title("testMat").measureUnit(MeasureUnit.PCS).build()).amount(BigDecimal.valueOf(1)).build());


        getSupplyRates.execute(delegateExecution);

        verify(delegateExecution).setVariable(eq(ProcessVariables.consumptionRates), resultArgumentCaptor.capture());

        assertThat(resultArgumentCaptor.getValue())
                .extracting(ConsumptionRateDto::getId, ConsumptionRateDto::getAmount, rate -> rate.getMaterial().getTitle())
                .contains(
                        tuple(1000L, new BigDecimal("0.1"), "Spirt"),
                        tuple(2000L, BigDecimal.valueOf(1), "testMat")
                );
    }
}