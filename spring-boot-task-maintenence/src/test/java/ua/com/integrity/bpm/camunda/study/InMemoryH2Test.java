package ua.com.integrity.bpm.camunda.study;

import org.apache.ibatis.logging.LogFactory;
import org.assertj.core.api.Assertions;
import org.camunda.bpm.engine.delegate.BpmnError;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.test.mock.Mocks;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ua.com.integrity.bpm.camunda.study.dto.EmployeeDto;
import ua.com.integrity.bpm.camunda.study.dto.equipment.EquipmentDto;
import ua.com.integrity.bpm.camunda.study.dto.equipment.EquipmentTypeDto;
import ua.com.integrity.bpm.camunda.study.dto.equipment.MaintenanceDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.BatchOfMaterialDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.ConsumptionRateDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.MaterialDto;
import ua.com.integrity.bpm.camunda.study.dto.materail.MeasureUnit;
import ua.com.integrity.bpm.camunda.study.process.*;
import ua.com.integrity.bpm.camunda.study.service.EmployeeService;
import ua.com.integrity.bpm.camunda.study.service.EquipmentService;
import ua.com.integrity.bpm.camunda.study.service.MaterialService;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.*;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.*;
import static org.mockito.Mockito.when;
import static ua.com.integrity.bpm.camunda.study.ProcessConstants.*;

/**
 * Test case starting an in-memory database-backed Process Engine.
 */
@RunWith(MockitoJUnitRunner.class)
public class InMemoryH2Test {

  static {
    LogFactory.useSlf4jLogging(); // MyBatis
  }

  @ClassRule
  @Rule
  public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();


  @Mock
  EmployeeService employeeService;

  @Mock
  EquipmentService equipmentService;

  @Mock
  MaterialService materialService;

  @Before
  public void setup() {
    init(rule.getProcessEngine());
    mocksSetup();
    GetUserEquipmentList getUserEquipmentList = new GetUserEquipmentList(employeeService, equipmentService);
    Mocks.register("getUserEquipmentList", getUserEquipmentList);

    GetMaintenanceList getMaintenanceList = new GetMaintenanceList(equipmentService);
    Mocks.register("getMaintenanceList", getMaintenanceList);

    GetSupplyRates getSupplyRates = new GetSupplyRates(equipmentService, materialService);
    Mocks.register("getSupplyRates", getSupplyRates);

    DefineBatchOfMaterialsToWithdraw defineBatchOfMaterialsToWithdraw = new DefineBatchOfMaterialsToWithdraw(materialService);
    Mocks.register("defineBatchOfMaterialsToWithdraw", defineBatchOfMaterialsToWithdraw);

    WithdrawFromBatchOfMaterials withdrawFromBatchOfMaterials = new WithdrawFromBatchOfMaterials(materialService);
    Mocks.register("withdrawFromBatchOfMaterials", withdrawFromBatchOfMaterials);

    AssignTaskToAdmin assignTaskToAdmin = new AssignTaskToAdmin();
    Mocks.register("assignTaskToAdmin", assignTaskToAdmin);

  }

  @After
  public void reset() {
    Mocks.reset();
  }


  @Test
  @Deployment(resources = "process.bpmn")
  public void testParsingAndDeployment() {
  }

  @Test
  @Deployment(resources = "process.bpmn")
  public void testHappyPath() {
    // Drive the process by API and assert correct behavior by camunda-bpm-assert

    ProcessInstance processInstance = processEngine().getRuntimeService()
        .startProcessInstanceByKey(PROCESS_DEFINITION_KEY, withVariables(ProcessVariables.INITIATOR, "exampleUser" ));

    assertThat(processInstance)
            .isStarted()
            .hasPassed(SERVICE_TASK_RECIEVE_USER_EQUIPMENT_LIST)
            .isWaitingAt(HUMAN_TASK_SELECT_EQUIPMENT_FOR_MAINTENENCE)
            .task().isAssignedTo("exampleUser");

    String userEquipmentList = (String) fetchProcessVariable(processInstance.getId(), ProcessVariables.userEquipmentList);
    assertThat(userEquipmentList).contains("printer:[sn1]");

    complete(task(), withVariables(ProcessVariables.equipmentForMaintenence, "sn1"));

    assertThat(processInstance)
            .hasPassed(SERVICE_TASK_GET_MAINTENENCE_LIST)
            .isWaitingAt(HUMAN_TASK_SELECT_MAINTENANCE_TYPE)
            .task().isAssignedTo("aadmin");

    String equipmentTypeForMaintenence = (String) fetchProcessVariable(processInstance.getId(), ProcessVariables.equipmentType);
    assertThat(equipmentTypeForMaintenence).isEqualTo("testType");
    String availableMaintenanceSet = (String) fetchProcessVariable(processInstance.getId(), ProcessVariables.availableMaintenanceSet);
    assertThat(availableMaintenanceSet).isEqualTo("testMaintenance: 100");

    complete(task(), withVariables(ProcessVariables.selectedMaintenanceType, 100L ));

    assertThat(processInstance)
            .hasPassedInOrder(
                    SERVICE_TASK_GET_CONSUMPTION_RATES,
                    SERVICE_TASK_DEFINE_BATCH_OF_MATERIAL_TO_WITHDRAW
            ).hasNotPassed(ERROR_EVENT_UNSUFFICIENT_AMOUNT_OF_MATERIALS)
            .isWaitingAt(HUMAN_TASK_PROVIDE_MAINTENENCE)
            .task().isAssignedTo("aadmin");

    Map<Long, BigDecimal> batchesForWithdraw = (Map<Long, BigDecimal>) fetchProcessVariable(processInstance.getId(), ProcessVariables.batchesForWithdraw);
    assertThat(batchesForWithdraw)
            .containsEntry(15L, new BigDecimal("0.1"))
            .containsEntry(17L, new BigDecimal("2"));

    complete(task());

    assertThat(processInstance)
            .hasPassed(SERVICE_TASK_WITHDRAW_MATERIALS_FROM_BATCH)
            .isEnded();

  }

  private Object fetchProcessVariable(String processId, String varName) {
    return runtimeService().createVariableInstanceQuery()
            .processInstanceIdIn(processId)
            .variableName(varName)
            .singleResult().getValue();
  }


  private static final Set<ConsumptionRateDto> CONSUMPTIONRATES = Stream.of(
          ConsumptionRateDto.builder().id(1000L).title("testRate").material(MaterialDto.builder().id(20L).title("Spirt").measureUnit(MeasureUnit.LITER).build()).amount(new BigDecimal("0.1")).build(),
          ConsumptionRateDto.builder().id(2000L).title("testRate2000").material(MaterialDto.builder().id(30L).title("testMat").measureUnit(MeasureUnit.PCS).build()).amount(BigDecimal.valueOf(2)).build()
  ).collect(Collectors.toSet());

  @Test
  @Deployment(resources = "process.bpmn")
  public void test_that_process_flow_to_human_task_for_materials_renew_when_insufficient_amount_of_mateirals_found() {
    when(materialService.getBatches(20L)).thenReturn(
            Arrays.asList(
                    BatchOfMaterialDto.builder().id(14L).material(MaterialDto.builder().id(20L).title("Spirt").measureUnit(MeasureUnit.LITER).build()).remainingAmount(new BigDecimal("0.05")).build()
            )
    );

    ProcessInstance processInstance = runtimeService().createProcessInstanceByKey(PROCESS_DEFINITION_KEY)
            .startBeforeActivity(SERVICE_TASK_DEFINE_BATCH_OF_MATERIAL_TO_WITHDRAW)
            .setVariables(withVariables(ProcessVariables.consumptionRates, CONSUMPTIONRATES))
            .execute();

    assertThat(processInstance)
            .hasPassedInOrder(
                    SERVICE_TASK_DEFINE_BATCH_OF_MATERIAL_TO_WITHDRAW,
                    ERROR_EVENT_UNSUFFICIENT_AMOUNT_OF_MATERIALS
            ).isWaitingAt(HUMAN_TASK_PROVIDE_NEW_MATERIALS)
            .task().isAssignedTo("istarshyna");


    when(materialService.getBatches(20L)).thenReturn(
            Arrays.asList(
                    BatchOfMaterialDto.builder().id(14L).material(MaterialDto.builder().id(20L).title("Spirt").measureUnit(MeasureUnit.LITER).build()).remainingAmount(new BigDecimal("0.05")).build(),
                    BatchOfMaterialDto.builder().id(15L).material(MaterialDto.builder().id(20L).title("Spirt").measureUnit(MeasureUnit.LITER).build()).remainingAmount(new BigDecimal("0.3")).build()
            )
    );
    String listOfMaterialsToRenew = (String) fetchProcessVariable(processInstance.getId(), ProcessVariables.listOfMaterialsToRenew);
    assertThat(listOfMaterialsToRenew).isEqualTo("Spirt: 0.1");

    complete(task());

    assertThat(processInstance)
            .hasPassedInOrder(
                    GATEWAY_0GRE3HT,
                    SERVICE_TASK_DEFINE_BATCH_OF_MATERIAL_TO_WITHDRAW
            ).isWaitingAt(HUMAN_TASK_PROVIDE_MAINTENENCE);
  }




//=====================================AUX METHODS


  private void mocksSetup() {
    when(employeeService.getByLogin("exampleUser")).thenReturn(EmployeeDto.builder()
            .id(1L)
            .build());
    when(equipmentService.findAll(false)).thenReturn(Arrays.asList(new EquipmentDto[] {
            EquipmentDto.builder().serialNumber("sn1").type(10L).user(1L).build(),
            EquipmentDto.builder().serialNumber("sn2").type(10L).user(2L).build(),
            EquipmentDto.builder().serialNumber("sn3").type(3L).user(1L).build(),
            EquipmentDto.builder().serialNumber("sn4").type(4L).user(1L).build(),
            EquipmentDto.builder().serialNumber("sn5").type(4L).user(2L).build()
    }));
    when(equipmentService.getAllTypes()).thenReturn(Arrays.asList(new EquipmentTypeDto[]{
            EquipmentTypeDto.builder().id(10L).title("printer").build(),
            EquipmentTypeDto.builder().id(3L).title("notebook").build(),
            EquipmentTypeDto.builder().id(4L).title("mouse").build(),
            EquipmentTypeDto.builder().id(5L).title("phone").build()
    }));

    when(equipmentService.getEquipment("sn1"))
            .thenReturn(EquipmentDto.builder().serialNumber("sn1").type(10L).user(1L).build());
    when(equipmentService.getEquipmentType(10L))
            .thenReturn(EquipmentTypeDto.builder().id(10L).title("testType").maintenanceSet(Collections.singleton(100L)).build());
    when(equipmentService.getMaintenance(100L))
            .thenReturn(MaintenanceDto.builder().id(100L).consumptionRates(consumptionRates).title("testMaintenance").build());
    when(materialService.getConsumptionRate(1000L))
            .thenReturn(ConsumptionRateDto.builder().id(1000L).title("testRate").material(MaterialDto.builder().id(20L).title("Spirt").measureUnit(MeasureUnit.LITER).build()).amount(new BigDecimal("0.1")).build());
    when(materialService.getConsumptionRate(2000L))
            .thenReturn(ConsumptionRateDto.builder().id(2000L).title("testRate2000").material(MaterialDto.builder().id(30L).title("testMat").measureUnit(MeasureUnit.PCS).build()).amount(BigDecimal.valueOf(2)).build());

    when(materialService.getBatches(20L)).thenReturn(
            Arrays.asList(
                    BatchOfMaterialDto.builder().id(14L).material(MaterialDto.builder().id(20L).title("Spirt").measureUnit(MeasureUnit.LITER).build()).remainingAmount(new BigDecimal("0.05")).build(),
                    BatchOfMaterialDto.builder().id(15L).material(MaterialDto.builder().id(20L).title("Spirt").measureUnit(MeasureUnit.LITER).build()).remainingAmount(new BigDecimal("0.3")).build()
            )
    );
    when(materialService.getBatches(30L)).thenReturn(
            Arrays.asList(
                    BatchOfMaterialDto.builder().id(16L).material(MaterialDto.builder().id(30L).title("testMat").measureUnit(MeasureUnit.PCS).build()).remainingAmount(BigDecimal.valueOf(1)).build(),
                    BatchOfMaterialDto.builder().id(17L).material(MaterialDto.builder().id(30L).title("testMat").measureUnit(MeasureUnit.PCS).build()).remainingAmount(BigDecimal.valueOf(2)).build()
            )
    );
  }

  private Set<Long> consumptionRates = Stream.of(1000L, 2000L).collect(Collectors.toSet());


}
