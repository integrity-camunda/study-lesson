package ua.com.integrity.camunda.study;

import org.apache.ibatis.logging.LogFactory;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.test.mock.Mocks;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import ua.com.integrity.camunda.study.localdatetest.LocalDateRead;
import ua.com.integrity.camunda.study.localdatetest.LocalDateSet;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.*;

/**
 * Test case starting an in-memory database-backed Process Engine.
 */
public class ProcessUnitTest {

  static {
    LogFactory.useSlf4jLogging(); // MyBatis
  }

  @ClassRule
  @Rule
  public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();

  @Before
  public void setup() {
    init(rule.getProcessEngine());
    Mocks.register("localDateRead", new LocalDateRead());
    Mocks.register("localDateSet", new LocalDateSet());
  }

  @Test
  @Deployment(resources = "process.bpmn")
  public void testHappyPath() {
    // Drive the process by API and assert correct behavior by camunda-bpm-assert

    ProcessInstance processInstance = processEngine().getRuntimeService()
        .startProcessInstanceByKey("localdate-test-process");

    assertThat(processInstance)
            .isStarted()
            .hasPassed("localDateSet").isWaitingAt("localDateSet");

    execute(job());

    assertThat(processInstance)
            .hasPassed("localDateRead").isEnded();
  }

}
