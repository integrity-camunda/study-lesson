package ua.com.integrity.camunda.study;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.cdi.annotation.ProcessEngineName;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.ScopeType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.io.File;
import java.util.concurrent.TimeUnit;

import static org.camunda.bpm.engine.test.assertions.bpmn.AbstractAssertions.processEngine;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.assertThat;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.init;
import static org.jboss.shrinkwrap.resolver.api.maven.coordinate.MavenDependencies.createDependency;

@RunWith(Arquillian.class)
public class ProcessIT {

    @Deployment
    public static WebArchive getEarArchive() {

        File[] libs = Maven.configureResolver()
                .workOffline(false)
                .loadPomFromFile("pom.xml")
                .importDependencies(ScopeType.COMPILE)
                .addDependencies(
                        createDependency("org.camunda.bpm.assert:camunda-bpm-assert:jar:8.0.0", ScopeType.TEST, false),
                        createDependency("org.assertj:assertj-core:jar:3.19.0", ScopeType.TEST, false),
                        createDependency("org.camunda.commons:camunda-commons-logging:jar:1.10.0", ScopeType.TEST, false),
                        createDependency("org.jboss.shrinkwrap.resolver:shrinkwrap-resolver-impl-maven:jar:2.2.6", ScopeType.TEST, false)
                ).resolve().withTransitivity().asFile();

        return ShrinkWrap.create(WebArchive.class, "localdate-test-process.war")
                .addAsLibraries(libs)
                .addPackage("ua.com.integrity.camunda.study")
                .addPackage("ua.com.integrity.camunda.study.localdatetest")
                .addAsResource("process.bpmn")
                .addAsResource("META-INF/processes.xml", "META-INF/processes.xml")
                .addAsResource("META-INF/beans.xml", "META-INF/beans.xml");
    }

    @Inject
    @ProcessEngineName("default")
    ProcessEngine processEngine;

    @Before
    public void setupProcessEngine(){
        init(processEngine);
    }

    @Test
    public void testHappyPath() throws InterruptedException {

        ProcessInstance processInstance = processEngine().getRuntimeService()
                .startProcessInstanceByKey("localdate-test-process");
        TimeUnit.SECONDS.sleep(1);
        assertThat(processInstance)
                .isStarted()
                .hasPassed("localDateSet", "localDateRead")
                .isEnded();
    }
}
