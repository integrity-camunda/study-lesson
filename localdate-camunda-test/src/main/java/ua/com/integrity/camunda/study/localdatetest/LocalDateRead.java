package ua.com.integrity.camunda.study.localdatetest;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import javax.inject.Named;
import java.time.LocalDate;

@Named
public class LocalDateRead implements JavaDelegate {
    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        LocalDate localDate = (LocalDate) delegateExecution.getVariable("localDate");
    }
}
