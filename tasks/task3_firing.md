# Процесс увольнения сотрудника

**Цель**: уволить сотрудника с подписанием обходного листа, сдачей выданной сотруднику техники и расчетом.

### Описание процесса

1. HR-менеджер инициирует увольнение сотрудника заполняя обходной лист с такими данными:

    - данные по сотруднику

    - дата увольнения

2. Обходной лист подписывается непосредственным руководителем сотрудника, либо если непосредственный руководитель - вакант, то следующим руководителем по вертикали.

3. Обходной лист должен попасть на согласованию администратору где тот должен проставить галочки о том какое оборудование из выданного сотруднику он принял назад.

4. В ERP проводится открепление от сотрудника техники которая была за ним закреплена и которая была принята администратором.

5. Обходной лист должен попасть на согласование бухгалтеру. 

    - если системным администратором не было принято назад какое-то оборудование из того что закреплено за сотрудником, то на форме должны отображатся сведения о недостающем оборудовании и его стоимости. Кроме того, если была выявлена недостача и балансовая стоимость недостающего оборудования выше 0 - то бухгалтер должен будет сделать отметку на форме о том что недостача перекрыта средствами сотрудника.

6. После бухгалтера обходной лист попадает на утверждение директору.

7. После утверждения обходного листа директором в автоматическом режиме вносятся такие изменения в ERP:
    - обновляется запись сотрудника
    - обновляется запись должности занимаемой сотрудником
    - техника с нулевой балансовой стоимостью выявленная как недостача помечается как списанная
 