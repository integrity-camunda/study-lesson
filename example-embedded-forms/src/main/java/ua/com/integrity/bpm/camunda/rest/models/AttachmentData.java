package ua.com.integrity.bpm.camunda.rest.models;

import lombok.Data;
import org.jboss.resteasy.annotations.providers.multipart.PartType;

import javax.ws.rs.FormParam;
import javax.ws.rs.core.MediaType;
import java.io.InputStream;

@Data
public class AttachmentData {

    @FormParam("fileId")
    @PartType(MediaType.TEXT_PLAIN)
    private String fileId;

    @FormParam("content")
    @PartType(MediaType.APPLICATION_OCTET_STREAM)
    private InputStream content;

}
