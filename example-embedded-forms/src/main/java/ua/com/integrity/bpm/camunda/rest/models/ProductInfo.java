package ua.com.integrity.bpm.camunda.rest.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProductInfo {
    private String code;
    private String title;
    private String interestRate;
    private int creditTerm;
}
