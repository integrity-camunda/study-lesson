package ua.com.integrity.bpm.camunda.process;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import ua.com.integrity.bpm.camunda.constants.ProcessVariables;
import ua.com.integrity.bpm.camunda.process.models.CreditRequest;

import javax.inject.Named;
import java.util.Date;

@Named
public class FindCustomerInEDR implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        String code = (String) execution.getVariable(ProcessVariables.CLIENT_IDENTIFICATION_CODE);
        CreditRequest request = CreditRequest.builder()
                .identificationCode(code)
                .requestDate(new Date())
                .build();
        execution.setVariable(ProcessVariables.LOAN_REQUEST_DATA, request);
        execution.setVariable(ProcessVariables.CLIENT_DATA_FETCHED_FROM_EDR, false);
    }
}
