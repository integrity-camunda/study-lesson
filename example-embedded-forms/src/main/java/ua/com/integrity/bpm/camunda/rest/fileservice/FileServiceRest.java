package ua.com.integrity.bpm.camunda.rest.fileservice;

import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import ua.com.integrity.bpm.camunda.rest.models.AttachmentData;
import ua.com.integrity.bpm.camunda.rest.models.FileUploadResponse;
import ua.com.integrity.bpm.camunda.service.FileService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Path("attachment")
@Produces(MediaType.APPLICATION_JSON)
public class FileServiceRest {

    @Inject
    private FileService fileService;

    @GET
    @Path("{fileId}")
    @Produces("application/pdf")
    public Response readFileContent(@PathParam("fileId") String fileId) {
        try {
            InputStream fileContent = fileService.getFileContent(fileId);
            return Response.ok(new StreamingOutput() {
                @Override
                public void write(OutputStream output) throws IOException, WebApplicationException {
                    IOUtils.copy(fileContent,output);
                    fileContent.close();
                }
            }).build();
        } catch (FileNotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFile(@MultipartForm AttachmentData attachmentData) {
        String fileId = fileService.createFile(attachmentData.getContent());
        String fileLink = String.format("/ukrsibbank-demo-credit/api/attachment/%s", fileId);
        FileUploadResponse body = new FileUploadResponse(fileId,fileLink);
        return Response.status(Response.Status.CREATED).entity(body).build();
    }

    @PUT
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response updateFileContent(@MultipartForm AttachmentData attachmentData) {
        try {
            fileService.updateFileContent(attachmentData.getFileId(), attachmentData.getContent());
        } catch (FileNotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.noContent().build();
    }

    @DELETE
    @Path("{fileId}")
    public Response deleteFileContent(@PathParam("fileId") String fileId) {
        try {
            fileService.deleteFile(fileId);
        } catch (FileNotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.noContent().build();
    }
}
