package ua.com.integrity.bpm.camunda.process;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.TaskListener;
import org.camunda.bpm.engine.identity.User;
import ua.com.integrity.bpm.camunda.constants.ProcessVariables;
import ua.com.integrity.bpm.camunda.process.models.StepResultData;

import javax.inject.Named;
import java.util.Date;

@Named
public class ConfirmRequestTaskCompleteListener implements TaskListener {
    @Override
    public void notify(DelegateTask delegateTask) {
        String assigneeId = delegateTask.getAssignee();
        User assignee = delegateTask.getProcessEngine().getIdentityService().createUserQuery()
                .userId(assigneeId)
                .singleResult();
        DelegateExecution execution = delegateTask.getExecution();
        String decision = (String) execution.getVariable(ProcessVariables.CONFIRMATION_RESULT);
        StepResultData stepResultData = StepResultData.builder()
                .resultDate(new Date())
                .firstName(assignee.getFirstName())
                .lastName(assignee.getLastName())
                .decisionResult(decision)
                .build();
        execution.setVariable(ProcessVariables.CONFIRMATION_STEP_DATA, stepResultData);
    }
}
