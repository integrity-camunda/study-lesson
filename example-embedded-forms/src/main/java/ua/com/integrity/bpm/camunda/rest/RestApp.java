package ua.com.integrity.bpm.camunda.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/api")
public class RestApp extends Application {
}
