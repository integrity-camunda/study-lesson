package ua.com.integrity.bpm.camunda.rest.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FileUploadResponse {
    private String fileId;
    private String fileLink;
}
