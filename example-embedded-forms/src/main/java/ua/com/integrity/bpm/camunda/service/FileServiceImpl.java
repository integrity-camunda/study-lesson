package ua.com.integrity.bpm.camunda.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;
import java.io.*;
import java.util.UUID;

@Singleton
public class FileServiceImpl implements FileService {

    private final static String DIRECTORY_TITLE = "ukrsib-demo-process";
    private File directory;

    @PostConstruct
    private void setup() {
        String jbossServerDataDir = System.getProperty("jboss.server.data.dir");
        directory = new File(jbossServerDataDir,DIRECTORY_TITLE);
        try {
            FileUtils.forceMkdir(directory);
        } catch (IOException e) {
            throw new RuntimeException("Cannot create new directory for process files " + directory.getAbsolutePath(), e);
        }
    }

    private static String generateFileId() {
        return UUID.randomUUID().toString();
    }

    @Override
    public String createFile(InputStream content) {
        String fileId = generateFileId();
        File newFile = new File(directory, fileId);
        try(OutputStream outputStream = new FileOutputStream(newFile)) {
            IOUtils.copy(content, outputStream);
            content.close();
            return fileId;
        } catch (IOException e) {
            throw new RuntimeException("Cannot create new file with createFile operation", e);
        }
    }

    @Override
    public InputStream getFileContent(String fileId) throws FileNotFoundException {
        File fileToRead = new File(directory, fileId);
        return new FileInputStream(fileToRead);
    }

    @Override
    public void updateFileContent(String fileId, InputStream content) throws FileNotFoundException {
        File fileForUpdate = new File(directory, fileId);
        try(OutputStream outputStream = new FileOutputStream(fileForUpdate)) {
            IOUtils.copy(content, outputStream);
            content.close();
        } catch (FileNotFoundException e) {
            throw e;
        } catch (IOException e) {
            throw new RuntimeException("Cannot update file with id " + fileId + " in updateFileContent operation.", e);
        }
    }

    @Override
    public void deleteFile(String fileId) throws FileNotFoundException {
        File fileForRemove = new File(directory, fileId);
        try {
            FileUtils.forceDelete(fileForRemove);
        } catch (FileNotFoundException e) {
            throw e;
        } catch (IOException e) {
            throw new RuntimeException("Cannot remove file with id " + fileId, e);
        }
    }
}
