package ua.com.integrity.bpm.camunda.process.models;

import lombok.Data;

import java.io.Serializable;

@Data
public class Attachment implements Serializable {
    private String title;
    private String id;
    private String link;
}
