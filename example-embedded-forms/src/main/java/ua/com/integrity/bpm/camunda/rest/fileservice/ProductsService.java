package ua.com.integrity.bpm.camunda.rest.fileservice;

import ua.com.integrity.bpm.camunda.rest.models.ProductInfo;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Path("product")
@Produces(MediaType.APPLICATION_JSON)
public class ProductsService {

    private static final List<ProductInfo> ALL_PRODUCTS = Stream.of(
            new ProductInfo("creditLine", "Кредитная линия", "36.8", 12),
            new ProductInfo("autoCredit", "Автокредит", "28.2", 36),
            new ProductInfo("equipmentCredit", "Покупка оборудования", "31.6", 36)
    ).collect(Collectors.toList());

    @GET
    public List<ProductInfo> getAvailableProducts() {
        return ALL_PRODUCTS;
    }

}
