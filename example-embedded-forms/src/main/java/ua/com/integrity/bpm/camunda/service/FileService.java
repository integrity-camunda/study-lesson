package ua.com.integrity.bpm.camunda.service;

import java.io.FileNotFoundException;
import java.io.InputStream;

public interface FileService {

    String createFile(InputStream content);
    InputStream getFileContent(String fileId) throws FileNotFoundException;
    void updateFileContent(String fileId, InputStream content) throws FileNotFoundException;
    void deleteFile(String fileId) throws FileNotFoundException;

}
