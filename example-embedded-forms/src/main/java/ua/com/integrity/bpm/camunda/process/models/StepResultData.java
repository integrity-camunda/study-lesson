package ua.com.integrity.bpm.camunda.process.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@Builder
public class StepResultData implements Serializable {
    private String firstName;
    private String lastName;
    private String decisionResult;
    private Date resultDate;
}
