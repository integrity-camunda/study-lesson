package ua.com.integrity.bpm.camunda.constants;

public class ProcessVariables {

    public static final String CLIENT_IDENTIFICATION_CODE = "clientIdentificationCode";
    public static final String CLIENT_DATA_FETCHED_FROM_EDR = "clientDataFetchedFromEDR";
    public static final String PROCESS_INITIATOR = "processInitiator";
    public static final String LOAN_REQUEST_DATA = "loanRequestData";

    public static final String CREATE_REQUEST_STEP_DATA = "createRequestStepData";
    public static final String CREATE_REQUEST_STEP_RESULT  = "createRequestStepResult";

    public static final String VERIFICATION_STEP_DATA = "verificationStepData";
    public static final String VERIFICATION_RESULT  = "verificationResult";

    public static final String CONFIRMATION_RESULT = "confirmationResult";
    public static final String CONFIRMATION_STEP_DATA = "confirmationStepData";
}
