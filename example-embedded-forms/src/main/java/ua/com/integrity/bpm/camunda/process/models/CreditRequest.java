package ua.com.integrity.bpm.camunda.process.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@Builder
public class CreditRequest implements Serializable {

    private final Date requestDate;
    private String identificationCode;
    private String subjectTitle;
    private String creditAmount;
    private String creditCurrency;
    private String productTitle;

    private String subjectLifeTime;
    private boolean subjectIsActive;
    private boolean hasNoLitigationsWithBank;

    private boolean subjectInfoIsCorrect;

    private List<Attachment> attachments;
}
