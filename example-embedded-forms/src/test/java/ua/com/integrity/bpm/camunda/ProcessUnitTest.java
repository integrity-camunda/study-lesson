package ua.com.integrity.bpm.camunda;

import lombok.Data;
import org.apache.ibatis.logging.LogFactory;
import org.camunda.bpm.engine.identity.Group;
import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.test.mock.Mocks;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.camunda.bpm.engine.test.Deployment;
import org.junit.*;
import ua.com.integrity.bpm.camunda.constants.ProcessVariables;
import ua.com.integrity.bpm.camunda.process.ConfirmRequestTaskCompleteListener;
import ua.com.integrity.bpm.camunda.process.FindCustomerInEDR;
import ua.com.integrity.bpm.camunda.process.LoanRequestTaskCompleteListener;
import ua.com.integrity.bpm.camunda.process.VerifyRequestTaskCompleteListener;
import ua.com.integrity.bpm.camunda.process.models.StepResultData;

import static org.assertj.core.api.Assertions.*;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.*;

/**
 * Test case starting an in-memory database-backed Process Engine.
 */
public class ProcessUnitTest {

  private static final String TEST_PROCESS_INITIATOR = "ogolovko";
  private static final TestUser[] TEST_INITIATORS = new TestUser[]{
          new TestUser("ogolovko", "Oleg", "Golovko")
  };

  private static final String ANALITICS_GROUP = "analitics";
  private static final TestUser[] TEST_ANALITICS = new TestUser[] {
      new TestUser("oivanov", "Oleg", "Ivanov"),
      new TestUser("dpetrov", "Dmytro", "Petrov"),
      new TestUser("vsidorov", "Volodymyr", "Sidorov")
  };
  private static final String VERIFICATORS_GROUP = "verificators";
  private static final TestUser[] TEST_VERIFICATORS = new TestUser[] {
          new TestUser("vivanchenko", "Viktor", "Ivanchenko")
  };
  @ClassRule
  @Rule
  public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();

  private static final String PROCESS_DEFINITION_KEY = "ukrsibbank-demo-credit";

  static {
    LogFactory.useSlf4jLogging(); // MyBatis
  }

  @BeforeClass
  public static void setupProcessEngine() {
    init(rule.getProcessEngine());
    for (TestUser newUser : TEST_INITIATORS) {
      User user = identityService().newUser(newUser.getId());
      user.setFirstName(newUser.getFirstName());
      user.setLastName(newUser.getLastName());
      identityService().saveUser(user);
    }
    Group analiticsGroup = identityService().newGroup(ANALITICS_GROUP);
    identityService().saveGroup(analiticsGroup);
    for (TestUser newUser : TEST_ANALITICS) {
      User user = identityService().newUser(newUser.getId());
      user.setFirstName(newUser.getFirstName());
      user.setLastName(newUser.getLastName());
      identityService().saveUser(user);
      identityService().createMembership(user.getId(), analiticsGroup.getId());
    }
    Group verificatorsGroup = identityService().newGroup(VERIFICATORS_GROUP);
    identityService().saveGroup(verificatorsGroup);
    for (TestUser newUser : TEST_VERIFICATORS) {
      User user = identityService().newUser(newUser.getId());
      user.setFirstName(newUser.getFirstName());
      user.setLastName(newUser.getLastName());
      identityService().saveUser(user);
      identityService().createMembership(user.getId(), verificatorsGroup.getId());
    }
  }

  @Before
  public void setupMocks() {
    FindCustomerInEDR findCustomerInEDR = new FindCustomerInEDR();
    Mocks.register("findCustomerInEDR", findCustomerInEDR);

    LoanRequestTaskCompleteListener loanRequestTaskCompleteListener = new LoanRequestTaskCompleteListener();
    Mocks.register("loanRequestTaskCompleteListener", loanRequestTaskCompleteListener);

    VerifyRequestTaskCompleteListener verifyRequestTaskCompleteListener = new VerifyRequestTaskCompleteListener();
    Mocks.register("verifyRequestTaskCompleteListener", verifyRequestTaskCompleteListener);

    ConfirmRequestTaskCompleteListener confirmRequestTaskCompleteListener = new ConfirmRequestTaskCompleteListener();
    Mocks.register("confirmRequestTaskCompleteListener", confirmRequestTaskCompleteListener);
  }

  /**
   * Just tests if the process definition is deployable.
   */
  @Test
  @Deployment(resources = "process.bpmn")
  public void testParsingAndDeployment() {
    // nothing is done here, as we just want to check for exceptions during deployment
  }

  @Test
  @Deployment(resources = "process.bpmn")
  public void testHappyPath() {
    ProcessInstance processInstance = runtimeService()
            .startProcessInstanceByKey(PROCESS_DEFINITION_KEY, Variables.putValue(ProcessVariables.PROCESS_INITIATOR, TEST_PROCESS_INITIATOR));

    assertThat(processInstance)
            .hasPassedInOrder(
                    "start_process",
                    "search_customer_in_EDR"
            ).isWaitingAt("human_task_create_loan_request")
            .task().isAssignedTo(TEST_PROCESS_INITIATOR);

    complete(task(), Variables.putValue(ProcessVariables.CREATE_REQUEST_STEP_RESULT, "OK"));

    assertThat(processInstance)
            .hasPassed(
                    "human_task_create_loan_request"
            ).isWaitingAt("human_task_verify_loan_request")
            .task().hasCandidateGroup(ANALITICS_GROUP);

    StepResultData requestStepResultData = (StepResultData) runtimeService().createVariableInstanceQuery()
            .processInstanceIdIn(processInstance.getProcessInstanceId())
            .variableName(ProcessVariables.CREATE_REQUEST_STEP_DATA)
            .singleResult().getValue();

    assertThat(requestStepResultData.getFirstName()).isEqualTo(TEST_INITIATORS[0].firstName);
    assertThat(requestStepResultData.getLastName()).isEqualTo(TEST_INITIATORS[0].lastName);
    assertThat(requestStepResultData.getDecisionResult()).isEqualTo("OK");
    assertThat(requestStepResultData.getResultDate()).isNotNull();

    taskService().claim(task().getId(), TEST_ANALITICS[0].getId());
    complete(task(), Variables.putValue(ProcessVariables.VERIFICATION_RESULT, "VERIFIED"));

    assertThat(processInstance)
            .hasPassed(
                    "human_task_verify_loan_request"
            ).isWaitingAt("human_task_confirm_loan_request")
            .task().hasCandidateGroup(VERIFICATORS_GROUP);

    StepResultData verificationStepResultData = (StepResultData) runtimeService().createVariableInstanceQuery()
            .processInstanceIdIn(processInstance.getProcessInstanceId())
            .variableName(ProcessVariables.VERIFICATION_STEP_DATA)
            .singleResult().getValue();

    assertThat(verificationStepResultData.getFirstName()).isEqualTo(TEST_ANALITICS[0].firstName);
    assertThat(verificationStepResultData.getLastName()).isEqualTo(TEST_ANALITICS[0].lastName);
    assertThat(verificationStepResultData.getDecisionResult()).isEqualTo("VERIFIED");
    assertThat(verificationStepResultData.getResultDate()).isNotNull();

    taskService().claim(task().getId(), TEST_VERIFICATORS[0].getId());
    complete(task(), Variables.putValue(ProcessVariables.CONFIRMATION_RESULT, "CONFIRMED"));

    StepResultData confirmationStepResultData = (StepResultData) runtimeService().createVariableInstanceQuery()
            .processInstanceIdIn(processInstance.getProcessInstanceId())
            .variableName(ProcessVariables.CONFIRMATION_STEP_DATA)
            .singleResult().getValue();

    assertThat(confirmationStepResultData.getFirstName()).isEqualTo(TEST_VERIFICATORS[0].firstName);
    assertThat(confirmationStepResultData.getLastName()).isEqualTo(TEST_VERIFICATORS[0].lastName);
    assertThat(confirmationStepResultData.getDecisionResult()).isEqualTo("CONFIRMED");
    assertThat(confirmationStepResultData.getResultDate()).isNotNull();

    assertThat(processInstance)
            .hasPassed(
                    "human_task_verify_loan_request"
            ).isWaitingAt("human_task_confirm_loan_results_view")
            .task().isAssignedTo(TEST_PROCESS_INITIATOR);

    complete(task());

    assertThat(processInstance).isEnded();

  }

  @Test
  @Deployment(resources = "process.bpmn")
  public void testPathWithRemakeStepAfterVerification() {
    ProcessInstance processInstance = runtimeService()
            .startProcessInstanceByKey(PROCESS_DEFINITION_KEY, Variables.putValue(ProcessVariables.PROCESS_INITIATOR, TEST_PROCESS_INITIATOR));

    assertThat(processInstance)
            .hasPassedInOrder(
                    "start_process",
                    "search_customer_in_EDR"
            ).isWaitingAt("human_task_create_loan_request")
            .task().isAssignedTo(TEST_PROCESS_INITIATOR);

    complete(task());

    assertThat(processInstance)
            .hasPassed(
                    "human_task_create_loan_request"
            ).isWaitingAt("human_task_verify_loan_request")
            .task().hasCandidateGroup(ANALITICS_GROUP);

    taskService().claim(task().getId(), TEST_ANALITICS[0].getId());
    complete(task(), Variables.putValue(ProcessVariables.VERIFICATION_RESULT, "REMAKE"));

    assertThat(processInstance)
            .hasPassed(
                    "human_task_verify_loan_request"
            ).isWaitingAt("human_task_create_loan_request")
            .task().isAssignedTo(TEST_PROCESS_INITIATOR);

  }

  @Data
  public static class TestUser {
    private final String id;
    private final String firstName;
    private final String lastName;
  }

}
